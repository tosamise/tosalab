/**
 * Description: Get message for validation of JQuery and Javascript
 * Function: Getmessage()
 * @author: HuyDo
 * @params: $type, $key
 * @return: message string
 * @version: 1.0
 * Note: WHEN EDIT OR UPDATE THE MESSAGE INFO IN FUNCTION, 
 * PLEASE ALSO EDIT OR UPDATE FOR /config/massage.php FILE.
 */
function Getmessage($type, $key)
{
    $msg = "";
    if( $type == "success" )
    {
        switch( $key ) {
            case "add-success":
                $msg = "The data has been added";
                break;
            default:
                $msg = "";
        }
    }
    else if( $type == "error" )
    {
        switch( $key ) {
            case "add-fail":
                $msg = "Error occurred, the data has not been add !";
                break;
            case "file-type":
                $msg = "The file type was not accepted !";
                break;
            case "file-size":
                $msg = "The file size was too big !";
                break;
            case "not-empty":
                $msg = "This field cannot empty";
                break;
            case "user-exist":
                $msg = "This username has been existed";
                break;
            case "email-exist":
                $msg = "This email has been existed";
                break;
            case "number-only":
                $msg = "Please insert the number digits";
                break;
            case "text-only":
                $msg = "Please insert text only";
                break;
            case "phone-invalid":
                $msg = "Please insert valid phone number";
                break;
            case "email-invalid":
                $msg = "Please insert valid email address";
                break;
            case "pass-wrong":
                $msg = "Your password is wrong";
                break;
            case "pass-unmatch":
                $msg = "Your passwords are not matched";
                break;
            case "min-6":
                $msg = "Required minimum 6 characters";
                break;
            case "select-role":
                $msg = "Please choose the role";
                break;
            default:
                $msg = "";
        }
    }
    else
    {
        $msg = "";
    }
}