Dropzone.autoDiscover = false;
Dropzone.options.myAwesomeDropzone = {
    accept: function(file, done) {
        done();
    },
    init: function() {
        this.on("addedfile", function() {
        if (this.files[10]!=null){
            swal("Error", "You can only upload 10 files once", "error");
            this.removeFile(this.files[0]);
            }
        });
    }
};

var errors = "";
var myDropzone = new Dropzone("#my-awesome-dropzone", {
    url: "/add-product-images",                        
    autoProcessQueue: false,
    acceptedFiles: "image/*",
    addRemoveLinks: true,
    parallelUploads: 10,
    maxFilesize: 2, // MB
    removedfile: function(file) {
        var _ref;
        return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
    },
    error: function(file, errorMessage) {
        errors = true;
    },
    queuecomplete: function() {
        if( (errors != null) && errors) swal("Error", "Fail to upload", "error");
        else swal("Good job!", "Your photos have been uploaded", "success");
        Dropzone.forElement("#my-awesome-dropzone").removeAllFiles(true);
        //location.reload();
    }
});
/*
myDropzone.on("addedfile", function(origFile) {
    var MAX_WIDTH = 800;
    var MAX_HEIGHT = 1200;

    var reader = new FileReader();

    // Convert file to img
    reader.addEventListener("load", function(event) {

        var origImg = new Image();
        origImg.src = event.target.result;

        origImg.addEventListener("load", function(event) {

            var width = event.target.width;
            var height = event.target.height;


            // Don't resize if it's small enough
            if (width <= MAX_WIDTH && height <= MAX_HEIGHT) {
                myDropzone.enqueueFile(origFile);
                return;
            }

            // Calc new dims otherwise
            if (width > height) {
                if (width > MAX_WIDTH) {
                    height *= MAX_WIDTH / width;
                    width = MAX_WIDTH;
                }
            }
            else {
                if (height > MAX_HEIGHT) {
                    width *= MAX_HEIGHT / height;
                    height = MAX_HEIGHT;
                }
            }

            // Resize
            var canvas = document.createElement('canvas');
            canvas.width = width;
            canvas.height = height;

            var ctx = canvas.getContext("2d");
            ctx.drawImage(origImg, 0, 0, width, height);

            var resizedFile = base64ToFile(canvas.toDataURL(), origFile);


            // Replace original with resized
            var origFileIndex = myDropzone.files.indexOf(origFile);
            myDropzone.files[origFileIndex] = resizedFile;


            // Enqueue added file manually making it available for
            // further processing by dropzone
            myDropzone.enqueueFile(resizedFile);
        });
    });

    reader.readAsDataURL(origFile);
});

function base64ToFile(dataURI, origFile) {
    var byteString, mimestring;

    if (dataURI.split(',')[0].indexOf('base64') !== -1) {
        byteString = atob(dataURI.split(',')[1]);
    }
    else {
        byteString = decodeURI(dataURI.split(',')[1]);
    }

    mimestring = dataURI.split(',')[0].split(':')[1].split(';')[0];

    var content = new Array();
    for (var i = 0; i < byteString.length; i++) {
        content[i] = byteString.charCodeAt(i);
    }

    var newFile = new File(
        [new Uint8Array(content)], origFile.name, {
            type: mimestring
        }
    );


    // Copy props set by the dropzone in the original file

    var origProps = [
        "upload", "status", "previewElement", "previewTemplate", "accepted"
    ];

    $.each(origProps, function(i, p) {
        newFile[p] = origFile[p];
    });

    return newFile;
}
*/