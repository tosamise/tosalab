/**
** Upload javascript handler
** file: /webroot/js/upload.js
**
** Author: HuyDo
** Date: Aug 2016
** Ver: 1.0
**
**/

$(document).ready(function()
{

	/**
	 * Description: Define dropzone for upload images function
	 * Function: none
	 * @author: HuyDo
	 * @pluginSource: http://www.dropzonejs.com
	 * @version: 1.0
	 * @applyLocation: src/Template/MPhotos/add.ctp (/upload-image)
	 */
 
	Dropzone.autoDiscover = false;
	Dropzone.options.myAwesomeDropzone = {
        accept: function(file, done) {
            done();
        },
        init: function() {
            this.on("addedfile", function() {
            if (this.files[10]!=null){
                swal("Error", "You can only upload 10 files once", "error");
                this.removeFile(this.files[0]);
                }
            });
        }
    };
    var errors = "";
	var myDropzone = new Dropzone("#my-awesome-dropzone", {
	    url: "/upload-image",                        
	    autoProcessQueue: false,
	    acceptedFiles: "image/*",
	    addRemoveLinks: true,
	    parallelUploads: 10,
        removedfile: function(file) {
        // var name = file.name;        
        // $.ajax({
        //     type: 'POST',
        //     url: 'delete.php',
        //     data: "id="+name,
        //     dataType: 'html'
        // });
            var _ref;
            return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
        },
        error: function(file, errorMessage) {
            errors = true;
        },
        queuecomplete: function() {
            if( (errors != null) && errors) swal("Error", "Fail to upload", "error");
            else swal("Good job!", "Your photos have been uploaded", "success");
            Dropzone.forElement("#my-awesome-dropzone").removeAllFiles(true);
        }
	});
	
	$("#cancel-image").click(function(){           
	     $.magnificPopup.close();
	     Dropzone.forElement("#my-awesome-dropzone").removeAllFiles(true);
	});
	
	$('#upload-image').click(function(){
	     swal({
            title: "Are you sure?",
            text: "Your photos will be uploaded to server",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes,do it!",
            closeOnConfirm: true
        }, function() {
            myDropzone.processQueue();
        });
	});
	
}); //End ready

(function() {

    // if ($("input#imageUpload").length) {
    //     $("input#imageUpload").on('change', function() {

    //         //Get count of selected files
    //         var countFiles = $(this)[0].files.length;

    //         var imgPath = $(this)[0].value;
    //         var extension = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
    //         var previewHolder = $("ul.imagePreview");
    //         previewHolder.empty();

    //         if (extension == "gif" || extension == "png" || extension == "jpg" || extension == "jpeg") {
    //             if (typeof(FileReader) != "undefined") {

    //                 //loop for each file selected for uploaded.
    //                 for (var i = 0; i < countFiles; i++) {

    //                     var reader = new FileReader();
    //                     reader.onload = function(e) {

    //                         $("<li><img src='" + e.target.result +"'></li>").appendTo(previewHolder);
    //                     }

    //                     previewHolder.show();
    //                     reader.readAsDataURL($(this)[0].files[i]);
    //                 }
    //             } else {
    //                  alert("This browser does not support FileReader.");
    //             }
    //         } else {
    //             alert("Please select only images");
    //         }
    //     });
    // } //Image upload preview

    // if($("ul.imagePreview").length) {
    //     $("ul.imagePreview").on("click", "li", function(event) {
    //         if($(this).hasClass("selected")) {
    //             $(this).removeClass("selected");
    //             $(this).find("div").remove();
    //         } else {
    //             $(this).addClass("selected");
    //             $(this).append("<div><label><span>Image Alt</span><input type='text'></label><label><span>Image Caption</span><input type='text'></label></div>");
    //         }
    //     });

    //     $("ul.imagePreview").on("click", "div", function(event) {
    //         event.stopPropagation();
    //     });
    // }//add form when clicked on an image

    if ($("a.uploadMediaImageForm").length) {
        $("a.uploadMediaImageForm").magnificPopup({
            type: 'inline',
            preloader: false,
            closeOnBgClick: false,
            enableEscapeKey: false,
            closeBtnInside: false,
            showCloseBtn: false,
            focus: '#name',

            removalDelay: 500, //delay removal by X to allow out-animation

            // When elemened is focused, some mobile browsers in some cases zoom in
            // It looks not nice, so we disable it:
            callbacks: {
                beforeOpen: function() {

                    if ($(window).width() < 700) {
                        this.st.focus = false;
                    } else {
                        this.st.focus = '#name';
                    }

                    this.st.mainClass = this.st.el.attr('data-effect');
                },

                beforeClose: function() {
                    ///$("ul.imagePreview").empty();
                    var countFiles = "";
                    var imgPath = "";
                    var extension = "";
                    var previewHolder = $("ul.imagePreview");
                    previewHolder.empty();
                    $("#my-awesome-dropzone")[0].reset();
                }
            },

            midClick: true // allow opening popup on middle mouse click. Always set
        });
    }
})(); //popup Forms and Uploads
