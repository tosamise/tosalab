/**
** Main javascript
** file: /webroot/js/main.js
**
** Author: HuyDo
** Date: June 2016
** Ver: 1.0
**
**/

$(document).ready(function()
{
    //Declare input mark plugin
    $("[data-mask]").inputmask();
    
    //Call datepicker for input
    var year = (new Date).getFullYear();
    //alert(year-18);
    $( "#dob" ).datepicker({
		dateFormat: "yy-mm-dd",
		changeMonth: true,
		changeYear: true,
		yearRange: (new Date().getFullYear()-80) + ':' + (new Date().getFullYear()-18)
	});
    
	/*** Message timeout for flash() of CakePHP ***/
	setTimeout(function(){
	$("div.auto_out").fadeOut("slow", function () {
		$(this).remove()
		;})
	;}, 
	4000);
	
	//Requences run function in update user page
	if( $("#select_city").val() && $("#select_city").val() != "0" )
	{
		$.when( getdistrict() ).then( getward() );
	}
	
	//Check and set currency in edit camera
	if( $("#hidden_bought_price").val() != "" )
	{
		var currency = $("#hidden_bought_price").val();
		var format = formatPrice( currency );
		
		$("#bought_price").val( format );
	}
	
	/**
	 * Description: Create gallery with lightbox
	 * Function: magnificPopup
	 * @author: HuyDo
	 * @pluginSource: http://dimsemenov.com/plugins/magnific-popup/
	 * @version: 1.0
	 * @applyLocation: src/Template/MPhotos/index.ctp
	 */
	$('.popup-gallery').magnificPopup({
		delegate: 'a',
		type: 'image',
		tLoading: 'Loading image #%curr%...',
		mainClass: 'mfp-img-mobile',
		gallery: {
			enabled: true,
			navigateByImgClick: true,
			preload: [0,1] // Will preload 0 - before current, and 1 after the current image
		},
		image: {
			tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
			titleSrc: function(item) {
				return item.el.attr('title') + '<small>by '+ item.el.attr('author') +'</small>';
			}
		}
	});
	
}); //End ready


/**
 * The functional functions
 *
 * Author TommyDo
 * 
 */

//Clear all input data
$("#clearAllbtn").on('click',function(event){
    $(this).closest('form').find("input").val("");
});

//SlideToggle the user panel when minimize toolbar
$(".sidebar-toggle").on('click',function(event){
    $(".user-panel").slideToggle();
});

/**
 * MUsers/update - update-info page
**/

//Get & Set districts data when select city
$("#select_city").on('change',function(event){
	return getdistrict();
}); //end select districts

function getdistrict()
{
	//Get city id
    var city_id = $("#select_city").val();
    
    var obj = new Array();
    
	$('#divLoading').addClass('show');
    $.ajax({
		type: "POST",
		url: "/get-district",
		data: {city_id:city_id},
		success: function(data){
			
			if(data != "")
			{
				//console.log(data);
				//Parse data got from server as JSON
				obj = jQuery.parseJSON(data);
				items = obj["value"];

				var strHtml="";
				//Fetch data to HTML
				for(var i = 0; i < items.length; i++) 
				{
					strHtml+="<option value='"+items[i].id+"'>"+items[i].district+"</option>";
				} // end for loop
				
				// Write all HTML string to page.
				$("#select_dist").html(strHtml);
				
				//Call function to set database data to selectbox
				setSelValDist();
				getward();
			}//end if
		},
		complete: function(){
        	$('#divLoading').removeClass('show');
    	},
    	error: function(){
        	$('#divLoading').removeClass('show');
    	}
   });//end ajax
   return false;
}//end getdistrict()

//Get & Set wards data when select district
$("#select_dist").on('change',function(event){
    return getward();
}); //end select districts

function getward()
{
	//Get ward id
    var district_id = $("#select_dist").val();
    
    var obj = new Array();
    
    $('#divLoading').addClass('show');
    $.ajax({
		type: "POST",
		url: "/get-ward",
		data: {district_id:district_id},
		success: function(data){
			
			if(data != "")
			{
				//Parse data got from server as JSON
				obj = jQuery.parseJSON(data);
				items = obj["value"];

				var strHtml="";	
				//Fetch data to HTML
				for(var i = 0; i < items.length; i++) 
				{
					strHtml+="<option value='"+items[i].id+"'>"+items[i].ward+"</option>";
				} // end for loop
				
				//* Write all HTML string to page.
				$("#select_ward").html(strHtml);
				//Call function to set database data to selectbox
				setSelValWard();
			}//end if
		},
		complete: function(){
        	$('#divLoading').removeClass('show');
    	},
    	error: function(){
        	$('#divLoading').removeClass('show');
    	}
   });//end ajax
   return false;
}//end getward()

//Set selected value district
function setSelValDist() {
	if( $("#dist_tmp").val() != "")
	{
		$tmp = $("#dist_tmp").val();
		$("#select_dist").val($tmp);
	}
}

//Set selected value ward
function setSelValWard() {
	if( $("#ward_tmp").val() != "")
	{
		$tmp = $("#ward_tmp").val();
		$("#select_ward").val($tmp);
	}
}


/**
 * MCameras/add - new-camera page
**/

//Get & Set money format from "bought_price"
$("#bought_price").on('change',function(event){
    
    var currency = $(this).val();
    var format = formatPrice( currency );
    
    $("#hidden_bought_price").val( currency );
    $("#bought_price").val( format);
    
}); //end bought_price


//Function format price when input
function formatPrice(nStr){
	nStr += '';
	nStr = nStr.replace(',','');
    var x = nStr.split('.');
    var x1 = x[0];
    var x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
    }
    return x1 + x2;
}


/**
 * MLabs/add or edit
**/

//Get & Set money format from "bought_price"
$("#lab_website").on('change',function(event){
    
    var fulllink = $(this).val();
    var position = fulllink.indexOf("/")+2;
	var link = fulllink.substr(position,fulllink.length)
	
    $("#lab_website").val( link );
    
}); //end bought_price