<?php
    
    $config['msg'] = [
        'success' => [
                'add-success' => 'The data has been added',
                'update-success' => 'The data has been updated',
                'reactive-user' => 'The user has been reactived',
                'banned' => 'User was banned',
                'welcome' => 'Welcome back !',
                'deactivated' => 'You was deactivated',
                'cam-sold' => 'Your camera was marked as sold',
                'deactivate-film' => 'Your film was deactivated',
                'active-film' => 'Your film was actived'
            ],
        'error' => [
                'add-fail' => 'Error occurred, the data has not been add !',
                'update-fail' => 'Error occurred, the data has not been updated !',
                'file-type' => 'The file type was not accepted !',
                'file-size' => 'The file size was too big !',
                'not-empty' => 'This field cannot empty',
                'user-exist' => 'This username has been existed',
                'email-exist' => 'This email has been existed',
                'number-only' => 'Please insert the number digits',
                'text-only' => 'Please insert text only',
                'phone-invalid' => 'Please insert valid phone number',
                'email-invalid' => 'Please insert valid email address',
                'pass-wrong' => 'Your password is wrong',
                'pass-unmatch' => 'Your passwords are not matched',
                'min-6' => 'Required minimum 6 characters',
                'select-role' => 'Please choose the role',
                'unmatch-user' => 'You can update your own info only',
                'invalid-user' => 'Invalid username / password or your account was banned',
                'invalid-request' => 'Your request is not right, please try again',
                'banned-fail' => 'Fail to ban this user',
                'reactive-fail' => 'Fail to reactive this user',
                'deactivated-fail' => 'Fail to deactivated you user',
                'unique' => 'This record was existed',
                'required' => 'This field is required'
            ],
        'warning' => [
                'logged-in' => 'You have already logged in'
            ]    
    ]
 
?>