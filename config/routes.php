<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

use Cake\Core\Plugin;
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;

/**
 * The default class to use for all routes
 *
 * The following route classes are supplied with CakePHP and are appropriate
 * to set as the default:
 *
 * - Route
 * - InflectedRoute
 * - DashedRoute
 *
 * If no call is made to `Router::defaultRouteClass()`, the class used is
 * `Route` (`Cake\Routing\Route\Route`)
 *
 * Note that `Route` does not do any inflections on URLs which will result in
 * inconsistently cased URLs when used with `:plugin`, `:controller` and
 * `:action` markers.
 *
 */
Router::defaultRouteClass('DashedRoute');

Router::scope('/', function (RouteBuilder $routes) {
    /**
     * Here, we are connecting '/' (base path) to a controller called 'Pages',
     * its action called 'display', and we pass a param to select the view file
     * to use (in this case, src/Template/Pages/home.ctp)...
     */
    //$routes->connect('/', ['controller' => 'Pages', 'action' => 'display', 'home']);
    $routes->connect('/', ['controller' => 'Pages', 'action' => 'dashboard']);
    
    /*
    * My custom routes config 
    * By TommyDo
    * June 2016
    */
    //Login & logout
    $routes->connect('/login', ['controller' => 'MUsers', 'action' => 'login']);
    $routes->connect('/logout', ['controller' => 'MUsers', 'action' => 'logout']);
    //Register - Add users page
    $routes->connect('/register', ['controller' => 'MUsers', 'action' => 'add']);
    //Edit-user - Admin edit users
    $routes->connect('/edit-user/*', ['controller' => 'MUsers', 'action' => 'edit']);
    //Edit-password - Admin edit users password
    $routes->connect('/edit-password/*', ['controller' => 'MUsers', 'action' => 'editpass']);
    //Update-info - Users update their own infomation
    $routes->connect('/update-info/*', ['controller' => 'MUsers', 'action' => 'update']);
    //change-password - Users update their own password
    $routes->connect('/change-password', ['controller' => 'MUsers', 'action' => 'updatepass']);
    //user-list - User list
    $routes->connect('/user-list', ['controller' => 'MUsers', 'action' => 'index']);
    //View user detail
    $routes->connect('/view-user/*', ['controller' => 'MUsers', 'action' => 'view']);
    //ban-user - Admin ban the user
    $routes->connect('/ban-user', ['controller' => 'MUsers', 'action' => 'ban']);
    //active-user - Admin ban the user
    $routes->connect('/active-user', ['controller' => 'MUsers', 'action' => 'active']);
    //deactivate-user - Users deactivate themself
    $routes->connect('/deactivate-user/*', ['controller' => 'MUsers', 'action' => 'deactivate']);
    //activate-user - Users re-activate themself
    $routes->connect('/activate-user/*', ['controller' => 'MUsers', 'action' => 'activate']);

    //All cameras records
    $routes->connect('/camera', ['controller' => 'MCameras', 'action' => 'index']);
    //Create new cameras record
    $routes->connect('/new-camera', ['controller' => 'MCameras', 'action' => 'add']);
    //Edit cameras record
    $routes->connect('/edit-camera/*', ['controller' => 'MCameras', 'action' => 'edit']);
    //View cameras record
    $routes->connect('/view-camera/*', ['controller' => 'MCameras', 'action' => 'view']);
    //Sold cameras
    $routes->connect('/sold-camera/*', ['controller' => 'MCameras', 'action' => 'sold']);

    //All labs records
    $routes->connect('/lab', ['controller' => 'MLabs', 'action' => 'index']);
    //Create new labs record
    $routes->connect('/new-lab', ['controller' => 'MLabs', 'action' => 'add']);
    //Edit labs record
    $routes->connect('/edit-lab/*', ['controller' => 'MLabs', 'action' => 'edit']);
    //View labs record
    $routes->connect('/view-lab/*', ['controller' => 'MLabs', 'action' => 'view']);

    //All films records
    $routes->connect('/film', ['controller' => 'MFilms', 'action' => 'index']);
    //Create new film record
    $routes->connect('/new-film', ['controller' => 'MFilms', 'action' => 'add']);
    //Edit film record
    $routes->connect('/edit-film/*', ['controller' => 'MFilms', 'action' => 'edit']);
    //View film record
    $routes->connect('/view-film/*', ['controller' => 'MFilms', 'action' => 'view']);
    //Deactivate film record
    $routes->connect('/deactivate-film/*', ['controller' => 'MFilms', 'action' => 'deactivate']);
    //Activate film record
    $routes->connect('/activate-film/*', ['controller' => 'MFilms', 'action' => 'activate']);
    
    //The photo gallery
    $routes->connect('/gallery', ['controller' => 'MPhotos', 'action' => 'index']);
    $routes->connect('/upload-image', ['controller' => 'MPhotos', 'action' => 'add']);

    /**
     * Route the AJAX request
     */
    $routes->connect('/get-district/*', ['controller' => 'MUsers', 'action' => 'getdistrict']);
    $routes->connect('/get-ward/*', ['controller' => 'MUsers', 'action' => 'getward']);
    $routes->connect('/ajax-film/*', ['controller' => 'MFilms', 'action' => 'ajax']);
    
    /**
     * *******
     * TOSA L'amour page
     * *******
     * 
     */
    //The categories
    $routes->connect('/categories/*', ['controller' => 'MCategories', 'action' => 'index']);
    $routes->connect('/add-category/*', ['controller' => 'MCategories', 'action' => 'add']);
    //The suppliers
    $routes->connect('/suppliers/*', ['controller' => 'MSuppliers', 'action' => 'index']);
    $routes->connect('/add-supplier/*', ['controller' => 'MSuppliers', 'action' => 'add']);
    $routes->connect('/edit-supplier/*', ['controller' => 'MSuppliers', 'action' => 'edit']);
    //The products
    $routes->connect('/add-product/*', ['controller' => 'MProducts', 'action' => 'add']);
    $routes->connect('/products/*', ['controller' => 'MProducts', 'action' => 'index']);
    $routes->connect('/product-images/*', ['controller' => 'MProimgs', 'action' => 'index']);
    $routes->connect('/add-product-images', ['controller' => 'MProimgs', 'action' => 'add']);
    $routes->connect('/edit-product', ['controller' => 'MProducts', 'action' => 'edit']);
     
    /**
     * Route the AJAX request
     */
    $routes->connect('/ajax-cate/*', ['controller' => 'MCategories', 'action' => 'ajax']);
    $routes->connect('/ajax-sup/*', ['controller' => 'MSuppliers', 'action' => 'ajax']);
    $routes->connect('/ajax-pro/*', ['controller' => 'MProducts', 'action' => 'ajax']);
    $routes->connect('/ajax-proimg/*', ['controller' => 'MProimgs', 'action' => 'ajax']);

    /**
     * ...and connect the rest of 'Pages' controller's URLs.
     */
    $routes->connect('/pages/*', ['controller' => 'Pages', 'action' => 'display']);

    /**
     * Connect catchall routes for all controllers.
     *
     * Using the argument `DashedRoute`, the `fallbacks` method is a shortcut for
     *    `$routes->connect('/:controller', ['action' => 'index'], ['routeClass' => 'DashedRoute']);`
     *    `$routes->connect('/:controller/:action/*', [], ['routeClass' => 'DashedRoute']);`
     *
     * Any route class can be used with this method, such as:
     * - DashedRoute
     * - InflectedRoute
     * - Route
     * - Or your own route class
     *
     * You can remove these routes once you've connected the
     * routes you want in your application.
     */
    $routes->fallbacks('DashedRoute');
});

/**
 * Load all plugin routes.  See the Plugin documentation on
 * how to customize the loading of plugin routes.
 */
Plugin::routes();
