<?php
namespace App\Shell;

use Cake\Console\Shell;
use Ifsnop\Mysqldump as IMysqldump;
use Cake\Core\Configure;

class BackupDbShell extends Shell
{

    public function main()
    {
    	//Read the config
    	$datasource = Configure::read('Datasources.default');
    	$host = $datasource['host'];
    	$dbname = $datasource['database'];
    	$user = $datasource['username'];
    	$pass = $datasource['password'];
    	$link = 'tmp/backup/';
    	$filename = ''.date("Ymd_his").'.sql';

    	//Run the backup
        try 
        {
        	$dump = new IMysqldump\Mysqldump("mysql:host=localhost;dbname=tosalab", 'root', '');
		    $dump->start(''.$link.$filename.'');
		} catch (\Exception $e) {
		    echo 'mysqldump-php error: ' . $e->getMessage();
		}

    }
}
?>