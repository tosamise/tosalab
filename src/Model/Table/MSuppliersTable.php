<?php  
// src/Model/Table/MSuppliersTable.php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\RulesChecker;
use Cake\ORM\Rule\IsUnique;
use Cake\Core\Configure;

//Loading the messages in config/message.php
$GLOBALS = ['msg' => Configure::read('msg')];

class MSuppliersTable extends Table
{
    public function initialize(array $config)
    {
        //parent::initialize();
        //Define used table name in database
        $this->table('tsl_suppliers');
        
    }//end initialize
    
    // public function validationDefault(Validator $validator)
    // {
    //     $validator = new Validator();

    //     $validator
    //         //Apply for add, edit, update actions
    //         ->notEmpty('cate_code', $GLOBALS['msg']['error']['required'])
    //         ->notEmpty('cate_name', $GLOBALS['msg']['error']['required'])

    //     ;//Important comma
        
    //     return $validator;
    // }
    
}//end class
?> 