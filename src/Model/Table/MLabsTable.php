<?php  
// src/Model/Table/MLabsTable.php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\RulesChecker;
use Cake\ORM\Rule\IsUnique;
use Cake\Core\Configure;

//Loading the messages in config/message.php
$GLOBALS = ['msg' => Configure::read('msg')];

class MLabsTable extends Table
{
    public function initialize(array $config)
    {
        //Load Xety/Cake3Upload plugin
        $this->addBehavior('Xety/Cake3Upload.Upload', [
        'fields' => [
            'lab_image' => [
                'path' => 'upload/labs/:md5',
                'overwrite' => true,
                'defaultFile' => 'default-lab.jpg',
                'prefix' => '../'
                    ]
                ]
            ]
        );
    
    }//end initialize
   
    public function validationDefault(Validator $validator)
    {
        $validator = new Validator();

        $validator
            //Apply for add, edit, update actions
            ->notEmpty('lab_name', $GLOBALS['msg']['error']['not-empty'])
            ->notEmpty('lab_address', $GLOBALS['msg']['error']['not-empty'])
            
            ->allowEmpty('lab_website')
            ->allowEmpty('map_link')
            ->allowEmpty('lab_image_file')
            
            ->allowEmpty('lab_phone')
            ->add('lab_phone', 'validPhoneType',[
                'rule' => array('custom', '/^[0]{1}[19]{1}[0-9]{8}$/i'),
                'message' => $GLOBALS['msg']['error']['phone-invalid']
            ])
            
        ;//Important comma
        
        return $validator;
    }
    
}//end class
?> 