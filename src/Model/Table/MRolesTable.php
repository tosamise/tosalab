<?php  
// src/Model/Table/MRolesTable.php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\RulesChecker;
use Cake\ORM\Rule\IsUnique;

class MRolesTable extends Table
{
    
    public function initialize(array $config)
    {
        //Create the relationship with m_users table
        $this->hasMany('MUsers', [
            'className' => ' MUsers',
            'foreignKey' => 'role',
        ]);
    
    }//end initialize
    
}//end class
?> 