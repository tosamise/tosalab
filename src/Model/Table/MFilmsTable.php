<?php  
// src/Model/Table/MFilmsTable.php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\RulesChecker;
use Cake\ORM\Rule\IsUnique;
use Cake\Core\Configure;

//Loading the messages in config/message.php
$GLOBALS = ['msg' => Configure::read('msg')];

class MFilmsTable extends Table
{
    public function initialize(array $config)
    {
        //Load Xety/Cake3Upload plugin
        $this->addBehavior('Xety/Cake3Upload.Upload', [
        'fields' => [
            'film_image' => [
                'path' => 'upload/films/:md5',
                'overwrite' => true,
                'defaultFile' => 'default-films.jpg',
                'prefix' => '../'
                    ]
                ]
            ]
        );
    
    }//end initialize
   
    public function validationDefault(Validator $validator)
    {
        $validator = new Validator();

        $validator
            //Apply for add, edit, update actions
            ->notEmpty('model', $GLOBALS['msg']['error']['not-empty'])
            ->notEmpty('film_name', $GLOBALS['msg']['error']['not-empty'])
            ->notEmpty('film_type', $GLOBALS['msg']['error']['not-empty'])
            
            ->allowEmpty('film_image_file')
        
            ->add('film_name', [
                'unique' => ['rule' => 'validateUnique', 
                            'provider' => 'table',
                            'message' => $GLOBALS['msg']['error']['unique']
                        ]
            ])
            
        ;//Important comma
        
        return $validator;
    }
    
}//end class
?> 