<?php  
// src/Model/Table/MTagsTable.php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\RulesChecker;
use Cake\ORM\Rule\IsUnique;

class MTagsTable extends Table
{
    public function initialize(array $config)
    {
        //parent::initialize();
        //Define used table name in database
        $this->table('tsl_tags');
        
    }//end initialize
    
}//end class
?> 