<?php  
// src/Model/Table/MusersTable.php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\RulesChecker;
use Cake\ORM\Rule\IsUnique;
use Cake\Core\Configure;

//Loading the messages in config/message.php
$GLOBALS = ['msg' => Configure::read('msg')];

class MUsersTable extends Table
{
    
    public function initialize(array $config)
    {
        //Create the relationship with m_cameras table
        $this->hasMany('MCameras');
        //Create the relationship with m_role table
        $this->hasOne('MRoles', [
            'className' => 'MRoles',
            'conditions' => ['MRoles.primary' => '1'],
            'dependent' => true
        ]);
    
        //Load Xety/Cake3Upload plugin
        $this->addBehavior('Xety/Cake3Upload.Upload', [
        'fields' => [
            'avatar' => [
                'path' => 'upload/avatars/:md5',
                'overwrite' => true,
                'defaultFile' => 'default-avatar.png',
                'prefix' => '../'
                    ]
                ]
            ]
        );
    
    }//end initialize
    
    public function validationDefault(Validator $validator)
    {
        $validator = new Validator();
        
        $validator
            //Apply for add, edit, update actions
            ->notEmpty('username', $GLOBALS['msg']['error']['not-empty'] )
            ->add('username', [
                'unique' => ['rule' => 'validateUnique', 
                            'provider' => 'table',
                            'message' => $GLOBALS['msg']['error']['user-exist']
                        ]
            ])
            
            ->notEmpty('nice_name', $GLOBALS['msg']['error']['not-empty'] )
            ->notEmpty('password', $GLOBALS['msg']['error']['not-empty'] )
            ->add('password', [
                'length' => [
                    'rule' => ['minLength', 6],
                    'message' => $GLOBALS['msg']['error']['min-6']
                ]
            ])
            
            ->allowEmpty('phone')
            ->add('phone', 'validPhone',[
                'rule' => array('custom', '/^[0]{1}[19]{1}[0-9]{8}$/i'),
                'message' => $GLOBALS['msg']['error']['phone-invalid']
            ])

            ->notEmpty('email', 'A email is required')
            ->add('email', 'validEmail', [
                'rule' => 'email',
                'message' => $GLOBALS['msg']['error']['email-invalid']
            ])
            ->add('email', [
                'unique' => ['rule' => 'validateUnique', 
                            'provider' => 'table',
                            'message' => $GLOBALS['msg']['error']['email-exist']
                        ]
            ])
            ->add('role','custom',[
                'rule'=>  function($value, $context){
                    $role = $context['data']['role'];
                    if ( !empty($role) && $role != 0 ) {
                        return true;
                    }
                    return false;
                },
                'message' => $GLOBALS['msg']['error']['select-role']
            ])
        ;//Important comma
        
        return $validator;
    }
    
    public function validationPassword(Validator $validator )
    {
        $validator = $this->validationDefault($validator);
        
        $validator
            ->add('old_pass','custom',[
                'rule'=>  function($value, $context){
                    $user = $this->get($context['data']['id']);
                    if ($user) {
                        if ((new DefaultPasswordHasher)->check($value, $user->password)) {
                            return true;
                        }
                    }
                    return false;
                },
                'message' => $GLOBALS['msg']['error']['pass-wrong']
            ])
            ->notEmpty('old_pass');
 
        $validator
            ->add('new_pass', [
                'length' => [
                    'rule' => ['minLength', 6],
                    'message' => $GLOBALS['msg']['error']['min-6']
                ]
            ])
            ->add('new_pass',[
                'match'=>[
                    'rule'=> ['compareWith','confirm_pass'],
                    'message' => $GLOBALS['msg']['error']['pass-unmatch']
                ]
            ])
            ->notEmpty('new_pass');
        $validator
            ->add('confirm_pass', [
                'length' => [
                    'rule' => ['minLength', 6],
                    'message' => $GLOBALS['msg']['error']['min-6']
                ]
            ])
            ->add('confirm_pass',[
                'match'=>[
                    'rule'=> ['compareWith','new_pass'],
                    'message' => $GLOBALS['msg']['error']['pass-unmatch']
                ]
            ])
            ->notEmpty('confirm_pass');
 
        return $validator;

    }//end validationPassword
    
    //Check the email when register
    public function buildRules(RulesChecker $rules)
    {
        $rules->addCreate(new IsUnique(['email']), 'uniqueEmail');
        
        return $rules;
    }
    
}//end class
?> 