<?php  
// src/Model/Table/MCamerasTable.php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\RulesChecker;
use Cake\ORM\Rule\IsUnique;

class MCamerasTable extends Table
{
    public function initialize(array $config)
    {
        //Create the relationship with m_users table
        $this->belongsTo('MUsers');
        
        //Load Xety/Cake3Upload plugin
        $this->addBehavior('Xety/Cake3Upload.Upload', [
        'fields' => [
            'cam_image' => [
                'path' => 'upload/cameras/:md5',
                'overwrite' => true,
                'defaultFile' => 'default-camera.jpg',
                'prefix' => '../'
                    ]
                ]
            ]
        );
    
    }//end initialize
    
    public function validationDefault(Validator $validator)
    {
        $validator = new Validator();

        $validator
            //Apply for add, edit, update actions
            ->notEmpty('cam_name', 'The camera name is required')
            ->notEmpty('cam_series', 'The serials is required')
            ->notEmpty('belong_to', 'Please select the owner')
            
            ->allowEmpty('lens')
            ->allowEmpty('bought_price')
            ->add('bought_price', 'validCurrency',[
                'rule' => 'numeric',
                'message' => 'Please insert the number'
            ])
            
        ;//Important comma
        
        return $validator;
    }
    
}//end class
?> 