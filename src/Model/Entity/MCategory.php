<?php
// src/Model/Entity/MCategory.php
namespace App\Model\Entity;

use Cake\ORM\Entity;

class MCategory extends Entity
{
    
    public function initialize(array $config)
    {
        parent::initialize();
        //Define used table name in database
        
    }//end initialize

    // Make all fields mass assignable except for primary key field "id".
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];

}//end class
?>