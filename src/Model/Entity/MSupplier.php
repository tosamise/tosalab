<?php
// src/Model/Entity/MSupplier.php
namespace App\Model\Entity;

use Cake\ORM\Entity;

class MSupplier extends Entity
{
    public function initialize(array $config)
    {
        parent::initialize();
        //Define used table name in database
        $this->table('m_suppliers');
        
    }//end initialize

    // Make all fields mass assignable except for primary key field "id".
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];

}//end class
?>