<?php
// src/Model/Entity/MProimg.php
namespace App\Model\Entity;

use Cake\ORM\Entity;

class MProimg extends Entity
{
    
    public function initialize(array $config)
    {
        parent::initialize();
        //Define used table name in database
        
    }//end initialize

    // Make all fields mass assignable except for primary key field "id".
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];

}//end class
?>