<?php
// src/Model/Entity/MTag.php
namespace App\Model\Entity;

use Cake\ORM\Entity;

class MTag extends Entity
{
    public function initialize(array $config)
    {
        parent::initialize();
        //Define used table name in database
        $this->table('m_tags');
        
    }//end initialize

    // Make all fields mass assignable except for primary key field "id".
    protected $_accessible = [
        '*' => true,
    ];

}//end class
?>