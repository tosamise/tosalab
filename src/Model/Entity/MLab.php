<?php
// src/Model/Entity/MLab.php
namespace App\Model\Entity;

use Cake\ORM\Entity;

class MLab extends Entity
{
    public function initialize(array $config)
    {
        parent::initialize();
        //Define used table name in database
        $this->table('m_labs');
        
    }//end initialize

    // Make all fields mass assignable except for primary key field "id".
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];

}//end class
?>