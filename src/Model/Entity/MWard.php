<?php
// src/Model/Entity/MWard.php
namespace App\Model\Entity;

use Cake\ORM\Entity;

class MWard extends Entity
{
    public function initialize(array $config)
    {
        parent::initialize();
        //Define used table name in database
        $this->table('m_wards');
        
    }//end initialize

    // Make all fields mass assignable except for primary key field "id".
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];

}//end class
?>