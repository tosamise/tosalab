<?php
// src/Model/Entity/MCamera.php
namespace App\Model\Entity;

use Cake\ORM\Entity;

class MCamera extends Entity
{
    public function initialize(array $config)
    {
        parent::initialize();
        //Define used table name in database
        $this->table('m_cameras');
        
    }//end initialize

    // Make all fields mass assignable except for primary key field "id".
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];

}//end class
?>