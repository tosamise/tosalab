<?php
/**
*
* Description: MCategories controller to handle all action in m_categories table
* Class: MCategoriesController
* @author: HuyDo
* Path: /src/Controller/MCategoriesController.php
*
**/
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;

//Loading the messages in config/message.php
$GLOBALS = ['msg' => Configure::read('msg')];

class MCategoriesController extends AppController
{
    
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Paginator');
        
    }
    
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        
        $this->Auth->allow(['']);
    }
    
    //Check the permission to access to the User controller
    public function isAuthorized($user)
    {

        if( in_array( $this->request->action , ['index','view'] ) ) 
        {
            return true;
        }
            
        return parent::isAuthorized($user);
    }
    
    public $components=array('RequestHandler');
    
    /**
	 * Description: Categories list
	 * Function: index
	 * @author: HuyDo
	 * @params: $search = null
	 * @return: object
	 * @version: 1.0
	 */
    public function index($search = null)
    {
        //Set page Title
        $this->set('title', 'Categories');
        
        if($this->request->is('post'))
        {
            //List all users search condition
            $this->paginate = [
                'limit' => 5,
                'order' => [
                    'MCategories.id' => 'desc'
                ]
            ];
            
            $search = $this->request->data['search'];
            
            $query = $this->MCategories->query();
            $query->where(['OR'=>['cate_code LIKE' => '%'.$search.'%',
                                'cate_name LIKE' => '%'.$search.'%',
                                ]
            ]);
            $categories = $this->paginate($query);
        }
        else
        {
            //List all users without condition
            $this->paginate = [
                'limit' => 5,
                'order' => [
                    'MCategories.id' => 'desc'
                ]
            ];
            $categories = $this->paginate($this->MCategories);
        }
        //Return the taken results
        $this->set(compact('categories'));
    }
    
    /**
	 * Description: Add new category
	 * Function: add
	 * @author: HuyDo
	 * @params: none
	 * @return: object
	 * @version: 1.0
	 */
    public function add()
    {
        if ($this->request->is('post')) {
            $cates = $this->MCategories->newEntity();
            $cates = $this->MCategories->patchEntity($cates, $this->request->data);
  
            if ($this->MCategories->save($cates)) {
                $this->Flash->success(__( $GLOBALS['msg']['success']['add-success'] ));
                return $this->redirect('/categories');
            }
            $this->Flash->error(__( $GLOBALS['msg']['error']['add-fail'] ));
        }
        $this->Flash->error(__( $GLOBALS['msg']['error']['invalid-request'] ));
    }//end add
    
    /**
	 * Description: Ajax function, solve the request from ajax
	 * Function: ajax
	 * @author: HuyDo
	 * @params: none
	 * @return: boolean
	 * @version: 1.0
	 */
    public function ajax()
    {
        $data = array();
        
        if( isset( $this->request->data['edit'] ) ) 
        {
            $data = $this->__edit( $this->request->data['edit'] );
        }
        else if( isset ( $this->request->data['del'] ) )
        {
            $data = $this->__del( $this->request->data['del'] );
        }
        else if( isset ( $this->request->data['check_code'] ) )
        {
            $data = $this->__checkCode( $this->request->data['check_code'] );
        }
        
        echo json_encode( ['data' => $data] );
        exit();
    }
    
    /**
    * Description: Edit the category info
    * Function: __edit()
    * @author: HuyDo
    * @params: data
    * @return: array data
    */
    private function __edit($data)
    {
        if($data != "")
        {
            $query = $this->MCategories->query();
            $query->update()
                ->set([
                    'cate_code' => $data['cate_code'],
                    'cate_name' => $data['cate_name'],
                    'modified' => $data['modified']
                ])
                ->where(['id' => $data['id']])
                ->execute();
                
            if( $query != false){
                return true;
            }else{
                return false;
            }
        }
    }
    
    /**
    * Description: Logical delete the category record
    * Function: __del()
    * @author: HuyDo
    * @params: $data
    * @return: array data
    */
    private function __del($data)
    {
        if($data != "")
        {
            $query = $this->MCategories->query();
            $query->update()
                ->set([
                    'is_delete' => 1,
                ])
                ->where(['id' => $data['id']])
                ->execute();
                
            if( $query != false){
                return true;
            }else{
                return false;
            }
        }
    }
    
    /**
    * Description: Check the category code is existed
    * Function: __checkCode()
    * @author: HuyDo
    * @params: $data
    * @return: boolean
    */
    private function __checkCode($data)
    {
        $query = $this->MCategories->find()
                       ->where(['cate_code' => $data])
                       ->first();
        ;
        if( $query != null ){
            return false;
        }else{
            return true;
        }
    }

}//end class

?>