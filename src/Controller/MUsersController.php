<?php
/**
*
* Description: MUsers controller to handle all actions in m_users table
* Class: MUsersController
* @author: HuyDo
* Path: src/Controller/MUsersController.php
*
**/
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;

//Loading the messages in config/message.php
$GLOBALS = ['msg' => Configure::read('msg')];

class MUsersController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Paginator');
        $this->loadComponent('RequestHandler');
    }
    
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        // Allow users to register and logout.
        // You should not add the "login" action to allow list. Doing so would
        // cause problems with normal functioning of AuthComponent.
        $this->Auth->allow(['logout','login','add']);
        
        // Change layout for Ajax requests
        if ($this->request->is('ajax')) 
        {
            $this->viewBuilder()->layout("ajax");
        }
    }
    
    //Check the permission to access to the User controller
    public function isAuthorized($user)
    {
        if($user['role'] != 1)
        {
            /* Stuff what normal user can do */
            if( in_array( $this->request->action , ['logout','update','updatepass','index','view','getdistrict','getward','deactivate','activate'] ) ) 
            {
                return true;
            }
        }    
            
        return parent::isAuthorized($user);
    }

    /**
	 * Description: Main page - User list
	 * Function: index
	 * @author: HuyDo
	 * @params: $search
	 * @return: object
	 * @version: 1.0
	 */
    public function index($search = null)
    {
        //Set page Title
        $this->set('title', 'Users list');
        
        // $this->set('roles', $this->getrole() );
   
        $MUsers = TableRegistry::get('MUsers');
        
        if($this->request->is('post'))
        {
            //List all users search condition
            $this->paginate = [
                'limit' => 8,
                // 'where' => ['is_active' => 0],
                'order' => [
                    'MUsers.id' => 'desc'
                ]
            ];
            
            $search = $this->request->data['search'];
            $query = $MUsers->query();
            $query->where(['OR'=>['username LIKE' => '%'.$search.'%',
                                'nice_name LIKE' => '%'.$search.'%',
                                'role LIKE' => '%'.$search.'%'
                                ],
                            // 'AND' => ['is_active' => 0]
            ]);
            $users = $this->paginate($query);
        }
        else
        {
            //List all users without search condition
            $query = $MUsers->query();
            // $query->where(['is_active' => 0]);                    
            $this->paginate = [
                'limit' => 8,
                'order' => [
                    'MUsers.id' => 'desc'
                ]
            ];
            $users = $this->paginate($query);
        }
        //Return the taken results
        $this->set(compact('users', $users));
    }

    /**
	 * Description: ADMIN role - Add or register new user
	 * Function: add
	 * @author: HuyDo
	 * @params: none
	 * @return: boolean
	 * @version: 1.0
	 */
    public function add()
    {
        //Set page Title
        $this->set('title', 'Register Users');
        
        //Get all user list
        $this->set('newusers', $this->newestUsers() );
        
        //Get & Set roles data
        //$this->set('roles' , $this->getrole() );
        
        $user = $this->MUsers->newEntity();
        if ($this->request->is('post')) {
            $user = $this->MUsers->patchEntity($user, $this->request->data);
            if ($this->MUsers->save($user)) {
                $this->Flash->success(__( $GLOBALS['msg']['success']['add-success'] ));
                return $this->redirect('register');
            }
            $this->Flash->error(__( $GLOBALS['msg']['error']['add-fail'] ));
        }
        $this->set('user', $user);
    }
    
    public function login()
    {
        //Define layout = null, not use default design from Cake
        $this->viewBuilder()->layout("");
        
        //Check user has already logon.
        if ($this->Auth->user())
        {
            $this->Flash->error(__( $GLOBALS['msg']['warning']['logged-in'] ));
            return $this->redirect($this->Auth->redirectUrl());
        }
        else
        {
            //Check the _POST method
            if ($this->request->is('post')) {
                $user = $this->Auth->identify();
                if ($user)
                {
                    //Set user login
                    $this->Auth->setUser($user);
                    
                    if($this->Auth->user())
                    {
                        //Set last_login time
                        $MUsers = TableRegistry::get('MUsers');
                        $query = $MUsers->query();
                        $query->update()
                            ->set(['last_login' => date('Y-m-d H:i:s'),
                                'login_ip' => $this->ip()
                            ])
                            ->where(['id' => $this->Auth->user('id')])
                            ->execute();
                        
                        //Check user active or not, if not set user active when login again
                        if( $this->Auth->user('is_active') == 1 )
                        {
                           $query2 = $MUsers->query();
                           $query2->update()
                            ->set(['is_active' => 0 ])
                            ->where(['id' => $this->Auth->user('id')])
                            ->execute();
                        }
                        
                        if($query)
                        {
                            $this->Flash->success(__( $GLOBALS['msg']['success']['welcome'] ));
                            return $this->redirect($this->Auth->redirectUrl());
                        }
                        else
                        {
                            $this->Auth->logout();
                            return $this->redirect('/login');
                        }
                    }
                }
                $this->Flash->error(__( $GLOBALS['msg']['error']['invalid-user'] ) );
            }
        }
    }
    
    /**
	 * Description: For all users can log out
	 * Function: logout
	 * @author: HuyDo
	 * @params: none
	 * @return: boolean
	 * @version: 1.0
	 */
    public function logout()
    {
        $this->request->session()->destroy();
        return $this->redirect($this->Auth->logout());
    }
    
    /**
	 * Description: ADMIN role - Admin edit users info.
	 * Function: edit
	 * @author: HuyDo
	 * @params: $id
	 * @return: boolean
	 * @version: 1.0
	 */
    public function edit($id = null)
    {
        //Set page Title
        $this->set('title', 'Edit User');
        
        //Check correct flow of process
        if( $id === "" || $id === null )
        {
            $this->Flash->error(__( $GLOBALS['msg']['error']['invalid-request'] ));
            return $this->redirect('/user-list');
        }
        
        //Get chosen user to edit 
        $user = $this->MUsers->get($id);
        
        if ($this->request->is(['post', 'put'])) 
        {
            $this->MUsers->patchEntity($user, $this->request->data);
            
            //Get upload file info
            $file = $this->request->data['avatar_file'];
            // Check avatar will upload or not.
            if( $file['name'] != "" )
            {
                
                if( in_array( $file['type'] , array('image/png','image/jpg','image/jpeg','image/gif') ) )
                {
                    if( $file['size'] >= 100000 )
                    {
                        $this->Flash->error(__( $GLOBALS['msg']['error']['file-size'] ));
                        return $this->redirect('/edit-user/'.$user['id']);
                    }
                }
                else
                {
                    $this->Flash->error(__( $GLOBALS['msg']['error']['file-type'] ));
                    return $this->redirect('/edit-user/'.$user['id']);
                }
            }
            
            if ($this->MUsers->save($user)) 
            {
                $this->Flash->success(__( $GLOBALS['msg']['success']['update-success'] ));
                return $this->redirect('/edit-user/'.$user['id']);
            }
            $this->Flash->error(__( $GLOBALS['msg']['error']['update-fail'] ));
        }
    
        $this->set('user', $user);
    }//end edit
    
    /**
	 * Description: ADMIN role - Admin edit users password
	 * Function: editpass
	 * @author: HuyDo
	 * @params: $id
	 * @return: boolean
	 * @version: 1.0
	 */
    public function editpass($id = null)
    {
        //Set page Title
        $this->set('title', 'Edit user password');
        
        //Check correct flow of process
        if( $id === "" || $id === null )
        {
            $this->Flash->error(__( $GLOBALS['msg']['error']['invalid-request'] ));
            return $this->redirect('/user-list');
        }
        
        //Get chosen user to edit 
        $user = $this->MUsers->get($id);
        
        
        if ($this->request->is(['post', 'put'])) 
        {
            $user = $this->MUsers->patchEntity($user, [
                    'password'      => $this->request->data['new_pass'],
                    'new_pass'     => $this->request->data['new_pass'],
                    'confirm_pass'     => $this->request->data['confirm_pass']
                ],
                ['validate' => 'password']
            );
        
            if ($this->MUsers->save($user)) 
            {
                $this->Flash->success(__( $GLOBALS['msg']['success']['update-success'] ));
                return $this->redirect('/edit-user/'.$user['id']);
            }
            $this->Flash->error(__( $GLOBALS['msg']['error']['pass-wrong'] ));
        }
        
        $this->set('user', $user);
    }//end editpass
    
    
    /**
	 * Description: USER role - Users update their info
	 * Function: update
	 * @author: HuyDo
	 * @params: $id
	 * @return: boolean
	 * @version: 1.0
	 */
    public function update($id = null)
    {
        //Set page Title
        $this->set('title', 'Update infomation');
        
        //Get & Set roles data
        //$this->set('roles' , $this->getrole() );
        //Get & Set cities data
        $this->set('cities' , $this->getcity() );

        //Check correct flow of process
        if( $id === "" || $id === null )
        {
            $this->Flash->error(__( $GLOBALS['msg']['error']['invalid-request'] ));
            return $this->redirect('/user-list');
        }//Check correct user update their own info
        elseif( $id != $this->Auth->user('id') )
        {
            $this->Flash->error( $GLOBALS['msg']['error']['unmatch-user'] );
            return $this->redirect('/update-info/'.$this->Auth->user('id'));
        }
        
        //Get chosen user to edit 
        $user = $this->MUsers->get($id);
        
        if ($this->request->is(['post', 'put'])) 
        {
            $this->MUsers->patchEntity($user, $this->request->data);
            
            //Get upload file info
            $file = $this->request->data['avatar_file'];
            // Check avatar will upload or not.
            if( $file['name'] != "" )
            {
                $file = $this->request->data['avatar_file'];
                //debug($file);
                if( in_array( $file['type'] , array('image/png','image/jpg','image/jpeg','image/gif') ) )
                {
                    if( $file['size'] >= 1000000 )
                    {
                        $this->Flash->error(__( $GLOBALS['msg']['error']['file-size'] ));
                        return $this->redirect('/update-info/'.$user['id']);
                    }
                }
                else
                {
                    $this->Flash->error(__( $GLOBALS['msg']['error']['file-type'] ));
                    return $this->redirect('/update-info/'.$user['id']);
                }
            }
            
            // debug($user);
            // exit;
            if ($this->MUsers->save($user)) 
            {
                $this->Flash->success(__( $GLOBALS['msg']['success']['update-success'] ));
                return $this->redirect('/update-info/'.$user['id']);
            }
            $this->Flash->error(__( $GLOBALS['msg']['error']['update-fail'] ));
        }
    
        $this->set('user', $user);
    }//end update
    
    /**
	 * Description: USER role - Users change their password
	 * Function: updatepass
	 * @author: HuyDo
	 * @params: none
	 * @return: boolean
	 * @version: 1.0
	 */
    public function updatepass()
    {
        //Set page Title
        $this->set('title', 'Change password');
        
        //Get chosen user to edit 
        $user = $this->MUsers->get($this->Auth->user('id'));
        
        if ($this->request->is(['post', 'put'])) 
        {
            $user = $this->MUsers->patchEntity($user, [
                    'old_pass'  => $this->request->data['old_pass'],
                    'password'      => $this->request->data['new_pass'],
                    'new_pass'     => $this->request->data['new_pass'],
                    'confirm_pass'     => $this->request->data['confirm_pass']
                ],
                ['validate' => 'password']
            );
        
            if ($this->MUsers->save($user)) 
            {
                $this->Flash->success(__( $GLOBALS['msg']['success']['update-success'] ));
                return $this->redirect('/update-info/'.$user['id']);
            }
            $this->Flash->error(__( $GLOBALS['msg']['error']['pass-wrong'] ));
        }
        
        $this->set('user', $user);
    }//end updatepass
    
    /**
	 * Description: View other user info - For user
	 * Function: view
	 * @author: HuyDo
	 * @params: $id = null
	 * @return: object
	 * @version: 1.0
	 */
    public function view($id = null)
    {
        //Set page Title
        $this->set('title', 'View user infomation');
        //Get chosen lab to edit 
        $user = $this->MUsers->get($id);
        $this->set('user', $user);
    }//end view
    
    
    /*
    *
    * -------------------------
    * The functional functions
    * -------------------------
    *
    */
    
    /**
	 * Description: Get the list of newest users
	 * Function: newestUsers
	 * @author: HuyDo
	 * @params: none
	 * @return: object
	 * @version: 1.0
	 */
    public function newestUsers()
    {
        return $this->MUsers->find('all', [ 'limit'=>'8', 'order'=>'id DESC' ])->all();
    }
    
    /**
	 * Description: Get the list of city
	 * Function: getcity
	 * @author: HuyDo
	 * @params: none
	 * @return: object
	 * @version: 1.0
	 */
    public function getcity()
    {
        //Get model MCities
        $MCities = TableRegistry::get('MCities');
        $query = $MCities->find('list', [
            'keyField' => 'id',
            'valueField' => 'city',
            // 'order' => 'city'
        ]);
        $data_init = ['0'=>'Select city'];
        $data = $data_init + $query->toArray();
        return $data;
    }//getcity
    
    /**
	 * Description: Get the list of district
	 * Function: getdistrict
	 * @author: HuyDo
	 * @params: none
	 * @return: object
	 * @version: 1.0
	 */
    public function getdistrict()
    {
        if($this->request->is('ajax'))
        {
            $city_id = $this->request->data['city_id'];
            if($city_id != "")
            {
                //Get model MDistricts
                $MDistricts = TableRegistry::get('MDistricts');
                $query = $MDistricts->find('all', [
                    'fields' => ['id','district'],
                    'conditions' => ['city' => $city_id]
                ]);
                $data = $query->toArray();
                
                $this->set('value', $query);
            }
        }
    }
    
    /**
	 * Description: Get the list of ward
	 * Function: getward
	 * @author: HuyDo
	 * @params: none
	 * @return: object
	 * @version: 1.0
	 */
    public function getward()
    {
        if($this->request->is('ajax'))
        {
            $district_id = $this->request->data['district_id'];
            if($district_id != "")
            {
                //Get model MDistricts
                $MWards = TableRegistry::get('MWards');
                $query = $MWards->find('all', [
                    'fields' => ['id','ward'],
                    'conditions' => ['district' => $district_id]
                ]);
                $data = $query->toArray();
                
                $this->set('value', $query);
            }
        }
    }

    /**
	 * Description: ADMIN role - Admin block the users
	 * Function: ban
	 * @author: HuyDo
	 * @params: none
	 * @return: boolean
	 * @version: 1.0
	 */
    public function ban()
    {
        if ($this->request->is(['post', 'put']))
        {
            $id = $this->request->data['id'];
            $MUsers = TableRegistry::get('MUsers');
            $query = $MUsers->query();
            $query->update()
                ->set(['is_delete' => '1'])
                ->where(['id' => $id ] )
                ->execute();
            if( $query != false)
            {
                $this->Flash->success(__( $GLOBALS['msg']['success']['banned'] ));
                return $this->redirect('/edit-user/'.$id);
            }
            else
            {
                $this->Flash->error(__( $GLOBALS['msg']['error']['banned-fail'] ));
                return $this->redirect('/edit-user/'.$id);
            }
        }
        $this->Flash->error(__( $GLOBALS['msg']['error']['invalid-request'] ));
        return $this->redirect('/user-list');
    }
    
    /**
	 * Description: ADMIN role - Admin re-active the users
	 * Function: active
	 * @author: HuyDo
	 * @params: none
	 * @return: boolean
	 * @version: 1.0
	 */
    public function active()
    {
        if ($this->request->is(['post', 'put']))
        {
            $id = $this->request->data['id'];
            $MUsers = TableRegistry::get('MUsers');
            $query = $MUsers->query();
            $query->update()
                ->set(['is_delete' => '0'])
                ->where(['id' => $id ] )
                ->execute();
            if( $query != false)
            {
                $this->Flash->success(__( $GLOBALS['msg']['success']['reactive-user'] ));
                return $this->redirect('/edit-user/'.$id);
            }
            else
            {
                $this->Flash->error(__( $GLOBALS['msg']['error']['reactive-fail'] ));
                return $this->redirect('/edit-user/'.$id);
            }
        }
        $this->Flash->error(__( $GLOBALS['msg']['error']['invalid-request'] ));
        return $this->redirect('/user-list');
    }
    
    /**
	 * Description: USER role - Users block themselfs
	 * Function: deactivate
	 * @author: HuyDo
	 * @params: $id
	 * @return: boolean
	 * @version: 1.0
	 */
    public function deactivate($id = null)
    {
        //Check correct flow of process
        if( $id === "" || $id === null )
        {
            $this->Flash->error(__( $GLOBALS['msg']['error']['invalid-request'] ));
            return $this->redirect('/user-list');
        }//Check correct user update their own info
        elseif( $id != $this->Auth->user('id') )
        {
            $this->Flash->error( $GLOBALS['msg']['error']['unmatch-user'] );
            return $this->redirect('/deactivate-user/'.$this->Auth->user('id'));
        }
        else
        {
            //Get chosen user to edit 
            $user = $this->MUsers->get($id);
        }
        
        if ($this->request->is(['post', 'put']))
        {
            $MUsers = TableRegistry::get('MUsers');
            $query = $MUsers->query();
            $query->update()
                ->set(['is_active' => '1', 'modified' => $this->request->data['modified']])
                ->where(['id' => $this->request->data['id']])
                ->execute();
            if( $query != false)
            {
                $this->Flash->success(__( $GLOBALS['msg']['success']['deactivated'] ));
                return $this->redirect('/logout');
            }
            else
            {
                $this->Flash->error(__( $GLOBALS['msg']['error']['deactivated-fail'] ));
                return $this->redirect('/update-info/'.$id);
            }
        }
        // $this->Flash->error(__('Sorry your request is not right'));
        // return $this->redirect('/user-list');
        $this->set('user',$user);
    }
    
    /**
	 * Description: USER role - Users re-activate themselfs
	 * Function: activate
	 * @author: HuyDo
	 * @params: none
	 * @return: boolean
	 * @version: 1.0
	 */
    public function activate()
    {
        if ($this->request->is(['post', 'put']))
        {
            $id = $this->request->data['id'];
            $MUsers = TableRegistry::get('MUsers');
            if( $query != false)
            {
                $this->Flash->success(__( $GLOBALS['msg']['success']['welcome'] ));
                return $this->redirect('/edit-user/'.$id);
            }
            else
            {
                $this->Flash->error(__( $GLOBALS['msg']['error']['reactive-fail'] ));
                return $this->redirect('/edit-user/'.$id);
            }
        }
        $this->Flash->error(__( $GLOBALS['msg']['error']['invalid-request'] ));
        return $this->redirect('/user-list');
    }
    //** User role **//
    
    /**
	 * Description: Get location IP
	 * Function: _ip
	 * @author: HuyDo
	 * @params: none
	 * @return: variable
	 * @version: 1.0
	 */
    public function ip()
    {
        if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            return ( preg_match( "/^([d]{1,3}).([d]{1,3}).([d]{1,3}).([d]{1,3})$/", $_SERVER['HTTP_X_FORWARDED_FOR'] ) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'] );
        } else {
            return null; //something else
        }
    }

}//end class

?>