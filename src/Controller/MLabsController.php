<?php
/**
*
* Description: MLabs controller to handle all actions in m_labs table
* Class: MLabsController
* @author: HuyDo
* Path: /src/Controller/MLabsController.php
*
**/
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;

//Loading the messages in config/message.php
$GLOBALS = ['msg' => Configure::read('msg')];

class MLabsController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Paginator');
    }
    
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
    }
    
    //Check the permission to access to the User controller
    public function isAuthorized($user)
    {
        /* Stuff what normal user can do */
        if( in_array( $this->request->action , ['index','view'] ) ) 
        {
            return true;
        }
            
        return parent::isAuthorized($user);
    }
    
    public $components=array('RequestHandler');
    
    /**
	 * Description: Main page - Lab list
	 * Function: index
	 * @author: HuyDo
	 * @params: $search = null
	 * @return: object
	 * @version: 1.0
	 */
    public function index($search = null)
    {
        //Set page Title
        $this->set('title', 'Labs list');
        
        if($this->request->is('post'))
        {
            //List all users search condition
            $this->paginate = [
                'limit' => 8,
                'order' => [
                    'MLabs.id' => 'desc'
                ]
            ];
            
            $search = $this->request->data['search'];
            $MLabs = TableRegistry::get('MLabs');
            $query = $MLabs->query();
            $query->where(['OR'=>['lab_name LIKE' => '%'.$search.'%'] ]);
            $labs = $this->paginate($query);
        }
        else
        {
            //List all users without condition
            $this->paginate = [
                'limit' => 8,
                'order' => [
                    'MLabs.id' => 'desc'
                ]
            ];
            $labs = $this->paginate($this->MLabs);
        }
        
        //Return the taken results
        $this->set(compact('labs', $labs));
    }
    
    /**
	 * Description: Create new lab record
	 * Function: add
	 * @author: HuyDo
	 * @params: none
	 * @return: object
	 * @version: 1.0
	 */
    public function add()
    {
        //Set page Title
        $this->set('title', 'Add new lab');
        
        $lab = $this->MLabs->newEntity();
        if ($this->request->is('post')) {
            $lab = $this->MLabs->patchEntity($lab, $this->request->data);
            
            //Get upload file info
            $file = $this->request->data['lab_image_file'];
            
            // Check avatar will upload or not.
            if( $file['name'] != "" )
            {
                
                //debug($file);
                if( in_array( $file['type'] , array('image/png','image/jpg','image/jpeg','image/gif') ) )
                {
                    if( $file['size'] >= 1000000 )
                    {
                        $this->Flash->error(__( $GLOBALS['msg']['error']['file-size'] ));
                        return $this->redirect('/new-lab');
                    }
                }
                else
                {
                    $this->Flash->error(__( $GLOBALS['msg']['error']['file-type'] ));
                    return $this->redirect('/new-lab');
                }
            }            
            else
            {
                $film->lab_image = "/img/labs/default-lab.jpg";
            }
            
            if ($this->MLabs->save($lab)) 
            {
                $this->Flash->success(__( $GLOBALS['msg']['success']['update-success'] ));
                return $this->redirect('/edit-lab/'.$lab->id);
            }
            $this->Flash->error(__( $GLOBALS['msg']['error']['add-fail'] ));
        }
        $this->set('lab', $lab);
    }//end add

    /**
	 * Description: Create new lab record
	 * Function: edit
	 * @author: HuyDo
	 * @params: $id = null
	 * @return: object
	 * @version: 1.0
	 */
    public function edit($id = null)
    {
        //Set page Title
        $this->set('title', 'Update lab infomation');
        
        //Get chosen lab to edit 
        $labs = $this->MLabs->get($id);
        
        if ($this->request->is(['post', 'put'])) 
        {
            $this->MLabs->patchEntity($labs, $this->request->data);
            
            //Get upload file info
            $file = $this->request->data['lab_image_file'];
            // Check avatar will upload or not.
            if( $file['name'] != "" )
            {
                //debug($file);
                if( in_array( $file['type'] , array('image/png','image/jpg','image/jpeg','image/gif') ) )
                {
                    if( $file['size'] >= 1000000 )
                    {
                        $this->Flash->error(__( $GLOBALS['msg']['error']['file-size'] ));
                        return $this->redirect('/edit-lab/'.$labs['id']);
                    }
                }
                else
                {
                    $this->Flash->error(__( $GLOBALS['msg']['error']['file-type'] ));
                    return $this->redirect('/edit-lab/'.$labs['id']);
                }
            }
            
            if ($this->MLabs->save($labs)) 
            {
                $this->Flash->success(__( $GLOBALS['msg']['success']['update-success'] ));
                return $this->redirect('/edit-lab/'.$labs['id']);
            }
            $this->Flash->error(__( $GLOBALS['msg']['error']['update-fail'] ));
        }
    
        $this->set('labs', $labs);
    }//end edit
    
    /**
	 * Description: View lab info - For user
	 * Function: view
	 * @author: HuyDo
	 * @params: $id = null
	 * @return: object
	 * @version: 1.0
	 */
    public function view($id = null)
    {
        //Set page Title
        $this->set('title', 'View lab infomation');
        //Get chosen lab to edit 
        $labs = $this->MLabs->get($id);
        $this->set('labs', $labs);
    }//end view
    
    
    /*
    *
    * -------------------------
    * The functional functions
    * -------------------------
    *
    */
    
    

}//end class

?>