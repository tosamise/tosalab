<?php
/**
*
* Description: MPhotos controller to handle all actions in m_photos table
* Class: MUsersController
* @author: HuyDo
* Path: src/Controller/MPhotosController.php
*
**/
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;

//Loading the messages in config/message.php
$GLOBALS = ['msg' => Configure::read('msg')];
//Get and Set the images links for controller use
$GLOBALS = ['imglinks' => Configure::read('links')];

class MPhotosController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Paginator');
    }
    
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        if ($this->request->is('ajax')) 
        {
            $this->viewBuilder()->layout("ajax");
        }
    }
    
    //Check the permission to access to the User controller
    public function isAuthorized($user)
    {
        /* Stuff what normal user can do */
        if( in_array( $this->request->action , ['index','edit'] ) ) 
        {
            return true;
        }
            
        return parent::isAuthorized($user);
    }
    
    public $components=array('RequestHandler');
    
    
    /**
	 * Description: Main page - photo gallery
	 * Function: index
	 * @author: HuyDo
	 * @params: $search
	 * @return: object
	 * @version: 1.0
	 */
    public function index($search = null)
    {
        //Set page Title
        $this->set('title', 'Photo Gallery');
           
        if($this->request->is('post'))
        {
            //List all users search condition
            $this->paginate = [
                'limit' => 20,
                'order' => [
                    'MPhotos.id' => 'desc'
                ]
            ];
            
            $search = $this->request->data['search'];
            //$MPhotos = TableRegistry::get('MPhotos');
            $query = $this->MPhotos->query();
            $query->where(['OR'=>['title LIKE' => '%'.$search.'%']]);
            $photos = $this->paginate($query);
        }
        else
        {
            //List all users without condition
            $this->paginate = [
                'limit' => 8,
                'order' => [
                    'MPhotos.id' => 'desc'
                ]
            ];
            $photos = $this->paginate($this->MPhotos);
        }
        //Return the taken results
        $this->set(compact('photos', $photos));
    }
    
    /**
	 * Description: Add, upload photos
	 * Function: add
	 * @author: HuyDo
	 * @params: none
	 * @return: object
	 * @version: 1.0
	 */
    public function add()
    {
        //Set page Title
        $this->set('title', 'Upload Photo');
        
        if(!empty($_FILES)){
            
            $targetDir = $GLOBALS['imglinks']['photos'];
            $fileName = $_FILES['file']['name'];
            $targetFile = $targetDir.$fileName;
            
            if(move_uploaded_file($_FILES['file']['tmp_name'],$targetFile)){
                return json_encode($this->response);
            }
            return json_encode($this->response);
        }
        
        $photos = $this->MPhotos->newEntity();
        if ($this->request->is('post')) {
            //$photos = $this->MPhotos->patchEntity($photos, $this->request->data);
            
            debug( $_FILES['file']['name'] );
            
            //Get upload file info
            $file = $this->request->data['film_image_file'];
            
            // Check avatar will upload or not.
            if( $file['name'] != "" )
            {
                
                //debug($file);
                if( in_array( $file['type'] , array('image/png','image/jpg','image/jpeg','image/gif') ) )
                {
                    if( $file['size'] >= 1000000 )
                    {
                        $this->Flash->error(__('Your file is too big, just less than 1MB only'));
                        return $this->redirect('/new-film');
                    }
                }
                else
                {
                    $this->Flash->error(__('Your file type is not accepted'));
                    return $this->redirect('/new-film');
                }
            }
            
            if ($this->MPhotos->save($film)) {
                $this->Flash->success(__('Your film info has been saved.'));
                return $this->redirect('/edit-film/'.$film->id);
            }
            $this->Flash->error(__('Unable to add new film record.'));
        }
        $this->set('photos', $photos);
    }//end add
    
    /*
    *
    * -------------------------
    * The functional functions
    * -------------------------
    *
    */
    
    /**
	 * Description: Upload and make thumbnail photo
	 * Function: upload
	 * @author: HuyDo
	 * @copyright: http://www.codexworld.com/upload-image-create-thumbnail-using-php/
	 * @params: 
	 *  $field_name:    The name of the input file
	 *  $target_folder: The path to folder contains photo
	 *  $file_name:     The name of file after uploaded
	 *  $thumb:         Create thumbnail True/False - Default FALSE
	 *  $thumb_folder:  The path to folder contains thumbnail
	 *  $thumb_width:   The width of thumbnail will be
	 *  $thumb_height:  The height of the thumnail will be
	 * @return: $fileName
	 * @version: 1.0
	 */
    function upload($field_name = '', $target_folder = '', $file_name = '', $thumb = FALSE, $thumb_folder = '', $thumb_width = '', $thumb_height = ''){
    	//folder path setup
    	$target_path = $target_folder;
    	$thumb_path = $thumb_folder;
    	
    	//file name setup
    	$filename_err = explode(".",$_FILES[$field_name]['name']);
    	$filename_err_count = count($filename_err);
    	$file_ext = $filename_err[$filename_err_count-1];
    	if($file_name != '')
    	{
    		$fileName = $file_name.'.'.$file_ext;
    	}
    	else
    	{
    		$fileName = $_FILES[$field_name]['name'];
    	}
    	
    	//upload image path
    	$upload_image = $target_path.basename($fileName);
    	
    	//upload image
    	if(move_uploaded_file($_FILES[$field_name]['tmp_name'],$upload_image))
    	{
    		//thumbnail creation
    		if($thumb == TRUE)
    		{
    			$thumbnail = $thumb_path.$fileName;
    			list($width,$height) = getimagesize($upload_image);
    			$thumb_create = imagecreatetruecolor($thumb_width,$thumb_height);
    			switch($file_ext){
    				case 'jpg':
    					$source = imagecreatefromjpeg($upload_image);
    					break;
    				case 'jpeg':
    					$source = imagecreatefromjpeg($upload_image);
    					break;
    				case 'png':
    					$source = imagecreatefrompng($upload_image);
    					break;
    				case 'gif':
    					$source = imagecreatefromgif($upload_image);
    					break;
    				default:
    					$source = imagecreatefromjpeg($upload_image);
    			}
    			imagecopyresized($thumb_create,$source,0,0,0,0,$thumb_width,$thumb_height,$width,$height);
    			switch($file_ext){
    				case 'jpg' || 'jpeg':
    					imagejpeg($thumb_create,$thumbnail,100);
    					break;
    				case 'png':
    					imagepng($thumb_create,$thumbnail,100);
    					break;
    				case 'gif':
    					imagegif($thumb_create,$thumbnail,100);
    					break;
    				default:
    					imagejpeg($thumb_create,$thumbnail,100);
    			}
    		}
    
    		return $fileName;
    	}
    	else
    	{
    		return false;
    	}
    }
    

}//end class

?>