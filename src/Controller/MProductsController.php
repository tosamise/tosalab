<?php
/**
*
* Description: MProducts controller to handle all action in m_clothes table
* Class: MProductsController
* @author: HuyDo
* Path: /src/Controller/MProductsController.php
*
**/
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;

//Loading the messages in config/message.php
$GLOBALS = ['msg' => Configure::read('msg')];

class MProductsController extends AppController
{
    
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Paginator');
        
    }
    
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        
        $this->Auth->allow(['']);
    }
    
    //Check the permission to access to the User controller
    public function isAuthorized($user)
    {
        if( in_array( $user['role'] , ['3'] ) )
        {
            if( in_array( $this->request->action , ['index','view','add'] ) ) 
            {
                return true;
            }
        }
        else
        {
            if( in_array( $this->request->action , ['index','view'] ) ) 
            {
                return true;
            }
        }
            
        return parent::isAuthorized($user);
    }
    
    public $components=array('RequestHandler','Paginator');
    
    /**
	 * Description: Show product list
	 * Function: index
	 * @author: HuyDo
	 * @params: none
	 * @return: object
	 * @version: 1.0
	 */
    public function index()
    {
        //Set page Title
        $this->set('title', 'Products list');
        
        //Build init paginate options
        $this->paginate = [
            'fields'=>[
                'pro_id'=>'MProducts.id',
                'pro_code'=>'MProducts.pro_code',
                'cate_code'=>'MProducts.cate_code',
                'color'=>'MProducts.color',
                'quality'=>'MProducts.quality_stt',
                'quantity'=>'MProducts.quantity',
                'price'=>'MProducts.price',
                'price_up'=>'MProducts.price_up',
                'tags_id'=>'MProducts.tags_id',
                'import_date'=>'MProducts.import_date',
                'img_name'=>'MProimgs.img_name',
                'is_sold'=>'MProducts.is_sold',
            ],
            'limit' => 8,
            'order' => [
                'MProducts.id' => 'desc'
            ],
            'group' => ['MProducts.id'],
        ];
        
        //Build the custom query with conditions
        $query = $this->MProducts->query();
        
        if( isset($this->request->query['cate']) )
        {
            $cate = $this->request->query['cate'];
            $query->join([
                'CnnProimgs' =>[
                    'table' => 'tsl_cnnproimgs',
                    'type' => 'LEFT',
                    'conditions' => 'CnnProimgs.img_id = MProducts.id'
                ],
                'MProimgs' => [
                    'table' => 'tsl_proimgs',
                    'type' => 'LEFT',
                    'conditions' => 'MProimgs.id = CnnProimgs.img_id'
                ],
            ]);
            $query->where(['MProducts.cate_code' => $cate]);
        }
        else if( $this->request->is('post') && $this->request->data['search'] )
        {
            $search_data = $this->request->data['search'];
            $query->where(['OR' => ['MProducts.cate_code LIKE' => '%'.$search_data.'%',
                                    'MProducts.pro_code LIKE' => '%'.$search_data.'%',
                                    'MProducts.color LIKE' => '%'.$search_data.'%',
                            ]]);
        }
        
        $products = $this->paginate($query);
        
        //Return the taken results
        $cates = $this->__getCategories();
        $products = $products->toArray();
        $tags = $this->__getTags();
        $this->set(compact('cates','products','tags'));
    }    
        
    /**
	 * Description: Add new clothes record
	 * Function: add
	 * @author: HuyDo
	 * @params: none
	 * @return: object
	 * @version: 1.0
	 */
    public function add()
    {
        //Set page Title
        $this->set('title', 'Clothes & Accessaries');
        
        if ($this->request->is('post')) {
            
            //Process the input tags
            $postTags = $this->request->data['tags_id'];
            $insertTags = array();
            $newTagsID = array();
            $tagsTable = TableRegistry::get('MTags');

            if( !empty($this->request->data['tags_id']) )
            {
                for( $i=0; $i < count($postTags); $i++ )
                {
                    if( $this->__checkTags($postTags[$i] ) )
                    {
                        array_push( $insertTags , $postTags[$i] );
                        continue;
                    }
                    else
                    {
                        $data = ['tag' => $postTags[$i]];
                        $tag = $tagsTable->newEntity();
                        $tag = $tagsTable->patchEntity($tag, $data);
                        $tagsTable->save($tag);
                        array_push( $newTagsID , $tag->id );
                    }
                }
                
                $tagID = "";
                $insertTags = array_merge( $insertTags , $newTagsID );
                for( $j=0; $j < count($insertTags); $j++ )
                {
                    $tagID = $tagID.",".$insertTags[$j]."";
                }
                $this->request->data['tags_id'] = substr( $tagID , 1 );
            }
            
            $this->request->data['price'] = str_replace( ',', '', $this->request->data['price'] );
            $this->request->data['price_up'] = str_replace( ',', '', $this->request->data['price_up'] );
            
            $clothes = $this->MProducts->newEntity();
            $clothes = $this->MProducts->patchEntity($clothes, $this->request->data);
            if ($this->MProducts->save($clothes)) {
                $this->Flash->success(__( $GLOBALS['msg']['success']['add-success'] ));
                return $this->redirect('/add-product');
            }
            $this->Flash->error(__( $GLOBALS['msg']['error']['add-fail'] ));
        }
        
        //Return the taken results
        $cates = $this->__getCategories();
        $sups = $this->__getSuppliers();
        $tags = $this->__getTags();
        $this->set(compact('tags','cates','sups'));
        
    }//end add
    
    /**
	 * Description: Ajax function, solve the request from ajax
	 * Function: ajax
	 * @author: HuyDo
	 * @params: none
	 * @return: boolean
	 * @version: 1.0
	 */
    public function ajax()
    {
        $data = array();
        
        if( isset( $this->request->data['check_code'] ) ) 
        {
            $data = $this->__checkprocode( $this->request->data['check_code'], $this->request->data['cate_code'] );
        }
        elseif( isset( $this->request->data['update_data'] ) )
        {
            $data = $this->__updateProduct($this->request->data['update_data']);
        }
        elseif ( isset( $this->request->data['sold'] ) ) {
            $data = $this->__soldProduct($this->request->data['sold']);
        }
        
        echo json_encode( ['data' => $data] );
        exit();
    }
    
    /**
    * Description: Get tags data
    * Function: __getTags()
    * @author: HuyDo
    * @params: none
    * @return: array data
    */
    private function __getTags()
    {
        $MTags = TableRegistry::get('MTags');
        $query = $MTags->find('list', [
            'keyField' => 'id',
            'valueField' => 'tag',
            'conditions'=> ['is_delete' => 0],
        ]);
        return $query;
    }
    
    /**
    * Description: Get tags data
    * Function: __checkTags()
    * @author: HuyDo
    * @params: $type, $tag_data
    * @return: array data
    */
    private function __checkTags($tag_id)
    {
        $MTags = TableRegistry::get('MTags');
      
        $query = $MTags->find()->where(['id' => $tag_id])->first();
        
        if( $query != null ){
            return true;
        }else{
            return false;
        }
    }
    
    /**
    * Description: Get existed product code
    * Function: __checkprocode()
    * @author: HuyDo
    * @params: $code, $cate
    * @return: array data
    */
    private function __checkprocode($code, $cate)
    {
        $query = $this->MProducts->find()
                       ->where(['pro_code' => $code,
                                'cate_code' => $cate])
                       ->first();
        ;
        if( $query != null ){
            return false;
        }else{
            return true;
        }
    }
    
    /**
    * Description: Get the categories data for select box
    * Function: __getCategories()
    * @author: HuyDo
    * @params: none
    * @return: array data
    */
    private function __getCategories()
    {
        $MCategories = TableRegistry::get('MCategories');
        $query = $MCategories->find('list', [
            'keyField' => 'cate_code',
            'valueField' => 'cate_name',
            'conditions'=> ['is_delete' => 0],
        ]);
        return $query->toArray();
    }
    
    /**
    * Description: Get the suppliers data for select box
    * Function: __getSuppliers()
    * @author: HuyDo
    * @params: none
    * @return: array data
    */
    private function __getSuppliers()
    {
        $MSuppliers = TableRegistry::get('MSuppliers');
        $query = $MSuppliers->find('list', [
            'keyField' => 'id',
            'valueField' => 'sup_name',
            'conditions'=> ['is_delete' => 0],
        ]);
        return $query->toArray();
    }
    
    /**
    * Description: Update product data with ajax
    * Function: __updateProduct()
    * @author: HuyDo
    * @params: update_data
    * @return: array data
    */
    private function __updateProduct($update_data)
    {
        $result = false;
        $tags = explode( "," , $update_data['tags_id'] );
        $tagsTable = TableRegistry::get('MTags');
        $insertTags = [];
        $newTagsID = [];
        
        for( $i=0; $i < count($tags); $i++ )
        {   
            if( !$this->__checkTags($tags[$i]) )
            {
                $data = ['tag' => $tags[$i],
                         'created' => date("Y-m-d h:i:s")
                    ];
                $tag = $tagsTable->newEntity();
                $tag = $tagsTable->patchEntity($tag, $data);
                $result = $tagsTable->save($tag);
                array_push( $newTagsID , $result->id );
            }
            else
            {
                array_push( $newTagsID , $tags[$i] );
            }
        }
        
        if( !empty($update_data['id']) )
        {
            $products = $this->MProducts->newEntity($update_data);
            $products->modified = date("Y-m-d h:i:s");
            $products->tags_id = implode( "," , $newTagsID );
            if( $this->MProducts->save($products) )
            {
                $result = true;
            }
        }
        return $result; 
    }

    /**
    * Description: Update product data with ajax
    * Function: __soldProduct()
    * @author: HuyDo
    * @params: pro_id
    * @return: array data
    */
    private function __soldProduct($pro_id)
    {
        $result = false;
        if( !empty($pro_id) )
        {
            $products = $this->MProducts->newEntity();
            $products->id = $pro_id;
            $products->is_sold = 1;
            $products->modified = date("Y-m-d h:i:s");
            if( $this->MProducts->save($products) )
            {
                $result = true;
            }
        }
        return $result; 
    }
    
    
  
}//end class

?>