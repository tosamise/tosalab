<?php
/**
*
* Description: MProimgs controller to handle all actions in tsl_proimgs table
* Class: MUsersController
* @author: HuyDo
* Path: src/Controller/MProimgsController.php
*
**/
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;

//Loading the messages in config/message.php
$GLOBALS = ['msg' => Configure::read('msg')];
//Get and Set the images links for controller use
$GLOBALS = ['imglinks' => Configure::read('links')];

class MProimgsController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Paginator');
    }
    
    public function beforeFilter(Event $event)
    {
			parent::beforeFilter($event);
			if ($this->request->is('ajax')) 
			{
					$this->viewBuilder()->layout("ajax");
			}
    }
    
    //Check the permission to access to the User controller
    public function isAuthorized($user)
    {
        /* Stuff what normal user can do */
        if( in_array( $this->request->action , ['index','adđ'] ) ) 
        {
            return true;
        }
            
        return parent::isAuthorized($user);
    }
    
    public $components=array('RequestHandler');
    
    
    /**
	 * Description: Main page - photo gallery
	 * Function: index
	 * @author: HuyDo
	 * @params: $search
	 * @return: object
	 * @version: 1.0
	 */
    public function index($search = null)
    {
        //Set page Title
        $this->set('title', 'Product images');
        
        //Search function
        if( $this->request->is('post') && isset($this->request->data['searchbtn']) )
        {
            //List all users search condition
            $this->paginate = [
                'limit' => 8,
                'order' => [
                    'MProimgs.id' => 'desc'
                ]
            ];
            
            $search = $this->request->data['search'];
            $query = $this->MProimgs->query();
            $query->where(['OR'=>['url LIKE' => '%'.$search.'%',
                                'img_name LIKE' => '%'.$search.'%',]]);
            $photos = $this->paginate($query);
        }
        else
        {
            //List all users without condition
            $this->paginate = [
                'limit' => 8,
                'order' => [
                    'MProimgs.id' => 'desc'
                ]
            ];
            $photos = $this->paginate($this->MProimgs);
        }
        
        //Add image to product
        if( $this->request->is('post') && isset($this->request->data['img_id']) )
        {
            $CnnProimgs = TableRegistry::get('CnnProimgs');
            
            $img_id = explode("," , $this->request->data['img_id']);
            $count_id = count( $img_id );
            $count_done = 0;
            
            for($i = 0; $i < $count_id; $i++)
            {
                $img_pro = $CnnProimgs->newEntity();
                $data['created'] = date('Y-m-d H:i:s');
                $data['pro_id'] = $this->request->data['pro_id'];
                $data['img_id'] = $img_id[$i];
                $img_pro = $CnnProimgs->patchEntity($img_pro, $data);
                
                if ( $CnnProimgs->save($img_pro) ) 
                {
                    $count_done++;
                }
            }
            
            if ( $count_done == $count_id ) 
            {
                $this->Flash->success(__( $GLOBALS['msg']['success']['add-success'] ));
                return $this->redirect('product-images');
            }
            else
            {
                $this->Flash->error(__( $GLOBALS['msg']['error']['add-fail'] ));
            }
        }
        
        //Get product code
        $procodes = $this->__getProcode();
        //Return the taken results
        $this->set( compact('photos','procodes') ); 
        
        
    }
    
    /**
	 * Description: Add, upload photos
	 * Function: add
	 * @author: HuyDo
	 * @params: none
	 * @return: object
	 * @version: 1.0
	 */
    public function add()
    {
        if(!empty($_FILES))
        {
            $targetDir = $GLOBALS['imglinks']['pro_images'];
            $fileName = $_FILES['file']['name'];
            $targetFile = $targetDir.$fileName;
            $photos = $this->MProimgs->newEntity();
            
            if(move_uploaded_file($_FILES['file']['tmp_name'],$targetFile)){

                $data['img_name'] = $fileName;
                $data['created'] = date('Y-m-d H:i:s');
                $photos = $this->MProimgs->patchEntity($photos, $data);
                
                if ($this->MProimgs->save($photos)) {
                    return $this->response;
                }
                return false;
            }
            return false;
        }
    }//end add
    
    /**
	 * Description: Ajax function, solve the request from ajax
	 * Function: ajax
	 * @author: HuyDo
	 * @params: none
	 * @return: boolean
	 * @version: 1.0
	 */
    public function ajax()
    {
        $data = array();
        
        if( isset( $this->request->data['pro_id'] ) ) 
        {
            $data = $this->__getProimg( $this->request->data['pro_id'] );
        }
        
        echo json_encode( ['data' => $data] );
        exit();
    }
    
    /**
    * Description: Get product code
    * Function: __getProcode()
    * @author: HuyDo
    * @params: $code, $cate
    * @return: array data
    */
    private function __getProcode()
    {
        $MProducts = TableRegistry::get('MProducts');
        return $MProducts->find()
                            ->select(['id','pro_code','cate_code'])
                            ->formatResults(function($results) {
                                return $results->combine(
                                    'id',
                                    function($row) {
                                        return $row['cate_code'] . $row['pro_code'];
                                    }
                                );
                            })
                            ->where(['is_sold' => '0'])
                            ->all();
    }
    
    /**
    * Description: Get product image data
    * Function: __getProimg()
    * @author: HuyDo
    * @params: $id
    * @return: array data
    */
    private function __getProimg($id)
    {
        return $this->MProimgs->find( 'all' )
            ->join([
                'CnnProimgs' =>[
                    'table' => 'tsl_cnnproimgs',
                    'type' => 'LEFT',
                    'conditions' => 'MProimgs.id = CnnProimgs.img_id'
                ],
            ])
            ->select(['CnnProimgs.pro_id',
                      'CnnProimgs.img_id',
                      'MProimgs.url',
                      'MProimgs.img_name',
                     ])
            ->where( [  'CnnProimgs.pro_id' => $id,
                        'CnnProimgs.is_delete' => '0', 
                        'MProimgs.is_delete' => '0' ] )
            // ->order( [ 'videos_details.release_time' => 'DESC',
            // 		'videos_details.id' => 'DESC'            		
            // ] )
            ->all();
    }

}//end class

?>