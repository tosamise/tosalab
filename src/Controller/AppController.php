<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Network\Session\DatabaseSession;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
        $this->loadComponent('Auth', [
            'authenticate' => array(
                'Form' => array(
                    'userModel' => 'MUsers',
                    'fields' => array('username' => 'username','password' => 'password'),
                    'scope' => array('is_delete' => '0')
                )
            ),
            'loginAction' => [
                'controller' => 'MUsers',
                'action' => 'login'            
            ],
            'authorize' => ['Controller'],
            'loginRedirect' => [
                'controller' => 'Pages',
                'action' => 'dashboard'
            ],
            'logoutRedirect' => [
                'controller' => 'MUsers',
                'action' => 'login'
            ],
            'unauthorizedRedirect' => true,
            'storage' => 'Session',
            'authError' => 'Woopsie, you are not authorized to access this area.',
            'flash' => [
                'params' => [
                    'class' => 'alert alert-danger alert-dismissible text-c',
                            ]
                        ]
        ]);
        
        //Define layout template for all site
        $this->viewBuilder()->layout("main_UI");

    }//end initialize

    /**
     * Before render callback.
     *
     * @param \Cake\Event\Event $event The beforeRender event.
     * @return void
     */
    public function beforeRender(Event $event)
    {
        if (!array_key_exists('_serialize', $this->viewVars) &&
            in_array($this->response->type(), ['application/json', 'application/xml'])
        ) {
            $this->set('_serialize', true);
        }
        
        //Get and Set role info for all app
        $this->set('roles', $this->getrole());
				//Get and Set the images links for view use
				$this->set('imglinks', Configure::read('links'));
    }
    
    /**
     * Before filter callback.
     *
     * @param \Cake\Event\Event $event The Before filter event.
     * @return void
     */
    public function beforeFilter(Event $event)
    {
			$this->Auth->deny();
    }
    
    public function isAuthorized($user)
    {
        $LoggedUser = $this->request->session();
        // Admin can access every action
        if (isset($user['role']) && $user['role'] == 1)
        {
            return true;
        }
        elseif($user['role'] != 1)
        {
            if ($this->request->action === 'dashboard') 
            {
                return true;
            }
        }
        // Default deny
        return false;
    }
    
    /**
	 * Description: Get the list of roles
	 * Function: getrole
	 * @author: HuyDo
	 * @params: none
	 * @return: object
	 * @version: 1.0
	 */
    public function getrole()
    {
        //Get model MRoles
        $MRoles = TableRegistry::get('MRoles');
        $query = $MRoles->find('list', [
            'keyField' => 'id',
            'valueField' => 'role'
        ]);
        $data_init = ['0'=>'Select role'];
        $data = $data_init + $query->toArray();
        return $data;
    }//getrole
    
}//end class
