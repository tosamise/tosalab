<?php
/**
*
* Description: MCameras controller to handle all action in m_cameras table
* Class: MCamerasController
* @author: HuyDo
* Path: /src/Controller/MCamerasController.php
*
**/
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;

//Loading the messages in config/message.php
$GLOBALS = ['msg' => Configure::read('msg')];

class MCamerasController extends AppController
{
    
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Paginator');
        
    }
    
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        
        $this->Auth->allow(['']);
    }
    
    //Check the permission to access to the User controller
    public function isAuthorized($user)
    {
        if( in_array( $this->request->action , ['index','view'] ) ) 
        {
            return true;
        }
            
        return parent::isAuthorized($user);
    }
    
    public $components=array('RequestHandler');
    
    /**
	 * Description: Main page - Camera list
	 * Function: index
	 * @author: HuyDo
	 * @params: $search = null
	 * @return: object
	 * @version: 1.0
	 */
    public function index($search = null)
    {
        //Set page Title
        $this->set('title', 'Camera list');
   
        //debug($this->request->params);
           
        if($this->request->is('post'))
        {
            //List all users search condition
            $this->paginate = [
                'limit' => 8,
                'order' => [
                    'MCameras.id' => 'desc'
                ]
            ];
            
            $search = $this->request->data['search'];
            $MCameras = TableRegistry::get('MCameras');
            $query = $MCameras->query();
            $query->where(['OR'=>['cam_name LIKE' => '%'.$search.'%',
                                'cam_type LIKE' => '%'.$search.'%',
                                'lens LIKE' => '%'.$search.'%',
                                'bought_price LIKE' => '%'.$search.'%'
                                ]
            ]);
            $cameras = $this->paginate($query);
        }
        else
        {
            //List all users without condition
            $this->paginate = [
                'limit' => 8,
                'order' => [
                    'MCameras.id' => 'desc'
                ]
            ];
            $cameras = $this->paginate($this->MCameras);
        }
        //Return the taken results
        $this->set(compact('cameras', $cameras));
    }
    
    /**
	 * Description: Create new camera record
	 * Function: add
	 * @author: HuyDo
	 * @params: none
	 * @return: object
	 * @version: 1.0
	 */
    public function add()
    {
        //Set page Title
        $this->set('title', 'Create new camera');
        
        //Get the camera owner
        $this->set( 'owners' , $this->getowner() );
        
        $camera = $this->MCameras->newEntity();
        if ($this->request->is('post')) {
            $camera = $this->MCameras->patchEntity($camera, $this->request->data);
            
            //Get upload file info
            $file = $this->request->data['cam_image_file'];
            
            // Check avatar will upload or not.
            if( $file['name'] != "" )
            {
                
                //debug($file);
                if( in_array( $file['type'] , array('image/png','image/jpg','image/jpeg','image/gif') ) )
                {
                    if( $file['size'] >= 1000000 )
                    {
                        $this->Flash->error(__( $GLOBALS['msg']['error']['file-size'] ));
                        return $this->redirect('/new-camera');
                    }
                }
                else
                {
                    $this->Flash->error(__( $GLOBALS['msg']['error']['file-type'] ));
                    return $this->redirect('/new-camera');
                }
            }            
            else
            {
                $film->cam_image = "/img/cameras/default-camera.jpg";
            }
            
            if ($this->MCameras->save($camera)) {
                $this->Flash->success(__( $GLOBALS['msg']['success']['add-success'] ));
                return $this->redirect('/edit-camera/'.$camera->id);
            }
            $this->Flash->error(__( $GLOBALS['msg']['error']['add-fail'] ));
            
        }
        $this->set('camera', $camera);
    }//end add

    /**
	 * Description: Edit camera info
	 * Function: edit
	 * @author: HuyDo
	 * @params: $id = null
	 * @return: object
	 * @version: 1.0
	 */
    public function edit($id = null)
    {
        //Set page Title
        $this->set('title', 'Update camera infomation');
        
        //Get the camera owner
        $this->set( 'owners' , $this->getowner() );
        
        //Get chosen camera to edit 
        $camera = $this->MCameras->get($id);
        
        if ($this->request->is(['post', 'put'])) 
        {
            $this->MCameras->patchEntity($camera, $this->request->data);
            
            //Get upload file info
            $file = $this->request->data['cam_image_file'];
            // Check avatar will upload or not.
            if( $file['name'] != "" )
            {
                //debug($file);
                if( in_array( $file['type'] , array('image/png','image/jpg','image/jpeg','image/gif') ) )
                {
                    if( $file['size'] >= 1000000 )
                    {
                        $this->Flash->error(__( $GLOBALS['msg']['error']['file-size'] ));
                        return $this->redirect('/edit-camera/'.$camera['id']);
                    }
                }
                else
                {
                    $this->Flash->error(__( $GLOBALS['msg']['error']['file-type'] ));
                    return $this->redirect('/edit-camera/'.$camera['id']);
                }
            }
            
            if ($this->MCameras->save($camera)) 
            {
                $this->Flash->success(__( $GLOBALS['msg']['success']['add-success'] ));
                return $this->redirect('/edit-camera/'.$camera['id']);
            }
            $this->Flash->error(__( $GLOBALS['msg']['error']['add-fail'] ));
        }
    
        $this->set('camera', $camera);
    }//end edit
    
    /**
	 * Description: View lab info - For user
	 * Function: view
	 * @author: HuyDo
	 * @params: $id = null
	 * @return: object
	 * @version: 1.0
	 */
	public function view($id = null)
    {
        //Set page Title
        $this->set('title', 'View camera infomation');
        //Get chosen lab to edit 
        $cameras = $this->MCameras->get($id);
        $this->set('camera', $cameras);
        
        //Get user info who own the camera
        $MUsers = TableRegistry::get('MUsers');
        $query = $MUsers->find('all', [
            'fields' => ['nice_name'],
            'conditions' => ['id' => $cameras['belong_to']]
        ]);
        $data = $query->toArray();
        $this->set( compact('data') );
    }//end view
    
    /*
    *
    * -------------------------
    * The functional functions
    * -------------------------
    *
    */
    
    /**
	 * Description: Get camera owner
	 * Function: getowner
	 * @author: HuyDo
	 * @params: none
	 * @return: object
	 * @version: 1.0
	 */
    protected function getowner()
    {
        $MUsers = TableRegistry::get('MUsers');
        $query = $MUsers->find('list', [
            'keyField' => 'id',
            'valueField' => 'nice_name',
        ]);
        $data_init = ['0'=>'Select owner'];
        $data = $data_init + $query->toArray();
        return $data;
    }
    
    /**
	 * Description: Mark camera as sold
	 * Function: sold
	 * @author: HuyDo
	 * @params: none
	 * @return: object
	 * @version: 1.0
	 */
    public function sold()
    {
        if ($this->request->is(['post', 'put']))
        {
            $id = $this->request->data['id'];
            $MCameras = TableRegistry::get('MCameras');
            $query = $MCameras->query();
            $query->update()
                ->set(['is_sold' => '1',
                    'modified' => $this->request->data['modified']
                ])
                ->where(['id' => $id])
                ->execute();
            if( $query != false)
            {
                $this->Flash->success(__( $GLOBALS['msg']['success']['cam-sold'] ));
                return $this->redirect('/edit-camera/'.$id);
            }
            else
            {
                $this->Flash->error(__( $GLOBALS['msg']['error']['update-fail'] ));
                return $this->redirect('/edit-camera/'.$id);
            }
        }
        $this->Flash->error(__( $GLOBALS['msg']['error']['invalid-request'] ));
        return $this->redirect('/camera');
    }
    
    

}//end class

?>