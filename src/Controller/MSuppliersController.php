<?php
/**
*
* Description: MSuppliers controller to handle all action in m_suppliers table
* Class: MCategoriesController
* @author: HuyDo
* Path: /src/Controller/MSuppliersController.php
*
**/
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;

//Loading the messages in config/message.php
$GLOBALS = ['msg' => Configure::read('msg')];

class MSuppliersController extends AppController
{
    
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Paginator');
        
    }
    
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        
        $this->Auth->allow(['']);
    }
    
    //Check the permission to access to the User controller
    public function isAuthorized($user)
    {

        if( in_array( $this->request->action , ['index','view'] ) ) 
        {
            return true;
        }
            
        return parent::isAuthorized($user);
    }
    
    public $components=array('RequestHandler');
    
    /**
	 * Description: Suppliers list
	 * Function: index
	 * @author: HuyDo
	 * @params: $search = null
	 * @return: object
	 * @version: 1.0
	 */
    public function index($search = null)
    {
        //Set page Title
        $this->set('title', 'Suppliers');
        
        if($this->request->is('post'))
        {
            //List all users search condition
            $this->paginate = [
                'limit' => 5,
                'order' => [
                    'MSuppliers.id' => 'desc'
                ]
            ];
            
            $search = $this->request->data['search'];
            
            $query = $this->MSuppliers->query();
            $query->where(['OR'=>['sup_name LIKE' => '%'.$search.'%',
                                'sup_phone LIKE' => '%'.$search.'%',
                                'sup_address LIKE' => '%'.$search.'%',
                                'sup_website LIKE' => '%'.$search.'%',
                                ]
            ]);
            $suppliers = $this->paginate($query);
        }
        else
        {
            //List all users without condition
            $this->paginate = [
                'limit' => 5,
                'order' => [
                    'MSuppliers.id' => 'desc'
                ]
            ];
            $suppliers = $this->paginate($this->MSuppliers);
        }
        //Return the taken results
        $tags = $this->__getTags();
        $this->set(compact('suppliers','tags'));
    }
    
    /**
	 * Description: Add new supplier
	 * Function: add
	 * @author: HuyDo
	 * @params: none
	 * @return: object
	 * @version: 1.0
	 */
    public function add()
    {
        if ($this->request->is('post')) {
            
            $postTags = $this->request->data['tags_id'];
            $insertTags = array();
            $newTagsID = array();
            $tagsTable = TableRegistry::get('MTags');
           
            for( $i=0; $i < count($postTags); $i++ )
            {
                if( $this->__checkTags($postTags[$i] ) )
                {
                    array_push( $insertTags , $postTags[$i] );
                    continue;
                }
                else
                {
                    $data = ['tag' => $postTags[$i]];
                    $tag = $tagsTable->newEntity();
                    $tag = $tagsTable->patchEntity($tag, $data);
                    $tagsTable->save($tag);
                    array_push( $newTagsID , $tag->id );
                }
            }
            
            $tagID = "";
            $insertTags = array_merge( $insertTags , $newTagsID );
            for( $j=0; $j < count($insertTags); $j++ )
            {
                $tagID = $tagID.",".$insertTags[$j]."";
            }
            $this->request->data['tags_id'] = substr( $tagID , 1 );
            $sups = $this->MSuppliers->newEntity();
            $sups = $this->MSuppliers->patchEntity($sups, $this->request->data);
            
            if ($this->MSuppliers->save($sups)) {
                $this->Flash->success(__( $GLOBALS['msg']['success']['add-success'] ));
                return $this->redirect('/suppliers');
            }
            $this->Flash->error(__( $GLOBALS['msg']['error']['add-fail'] ));
        }
        $this->Flash->error(__( $GLOBALS['msg']['error']['invalid-request'] ));
    }//end add
    
    /**
    * Description: Edit the supplier info
    * Function: edit()
    * @author: HuyDo
    * @params: none
    * @return: boolean
    */
    public function edit()
    {
        if ($this->request->is(['post', 'put'])) 
        {
            //Tags
            $postTags = $this->request->data['tags_id'];
            $insertTags = array();
            $newTagsID = array();
            $tagsTable = TableRegistry::get('MTags');
           
            for( $i=0; $i < count($postTags); $i++ )
            {
                if( $this->__checkTags($postTags[$i] ) )
                {
                    array_push( $insertTags , $postTags[$i] );
                    continue;
                }
                else
                {
                    $data = ['tag' => $postTags[$i]];
                    $tag = $tagsTable->newEntity();
                    $tag = $tagsTable->patchEntity($tag, $data);
                    $tagsTable->save($tag);
                    array_push( $newTagsID , $tag->id );
                }
            }
            
            $tagID = "";
            $insertTags = array_merge( $insertTags , $newTagsID );
            for( $j=0; $j < count($insertTags); $j++ )
            {
                $tagID = $tagID.",".$insertTags[$j]."";
            }
            $this->request->data['tags_id'] = substr( $tagID , 1 );
            // $sups = $this->MSuppliers->newEntity();
            // $sups = $this->MSuppliers->patchEntity($sups, $this->request->data);
            
            //Suppliers
            $sup_data = $this->request->data;

            $query = $this->MSuppliers->query();
            $query->update()
                ->set($sup_data)
                ->where(['id' => $sup_data['id']])
                ->execute();
                
            if( $query != false){
                $this->Flash->success(__( $GLOBALS['msg']['success']['update-success'] ));
                return $this->redirect('/suppliers');
            }else{
                $this->Flash->error(__( $GLOBALS['msg']['error']['edit-fail'] ));
                return $this->redirect('/suppliers');
            }
        }
        $this->Flash->error(__( $GLOBALS['msg']['error']['invalid-request'] ));
        return $this->redirect('/suppliers');
    }
    
    /**
    * Description: Get tags data
    * Function: __getTags()
    * @author: HuyDo
    * @params: none
    * @return: array data
    */
    private function __getTags()
    {
        $MTags = TableRegistry::get('MTags');
        $query = $MTags->find('list', [
            'keyField' => 'id',
            'valueField' => 'tag',
            'conditions'=> ['is_delete' => 0],
        ]);
        return $query->toArray();
    }
    
    /**
    * Description: Get tags data
    * Function: __checkTags()
    * @author: HuyDo
    * @params: none
    * @return: array data
    */
    private function __checkTags($id)
    {
        $MTags = TableRegistry::get('MTags');
        $query = $MTags->find()
                       ->where(['id' => $id])
                       ->first();
        ;
        if( $query != null ){
            return true;
        }else{
            return false;
        }
    }
    
    /**
	 * Description: Ajax function, solve the request from ajax
	 * Function: ajax
	 * @author: HuyDo
	 * @params: none
	 * @return: boolean
	 * @version: 1.0
	 */
    public function ajax()
    {
        $data = array();
        
        if( isset( $this->request->data['del'] ) ) 
        {
            $data = $this->__del( $this->request->data['del'] );
        }
        
        echo json_encode( ['data' => $data] );
        exit();
    }
    
    /**
    * Description: Logical delete the supplier record
    * Function: __del()
    * @author: HuyDo
    * @params: $data
    * @return: array data
    */
    private function __del($data)
    {
        if($data != "")
        {
            $query = $this->MSuppliers->query();
            $query->update()
                ->set([
                    'is_delete' => 1,
                ])
                ->where(['id' => $data['id']])
                ->execute();
                
            if( $query != false){
                return true;
            }else{
                return false;
            }
        }
    }

}//end class

?>