<?php
// src/Controller/MFilmsController.php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;

//Loading the messages in config/message.php
$GLOBALS = ['msg' => Configure::read('msg')];

class MFilmsController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Paginator');
    }
    
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
    }
    
    //Check the permission to access to the User controller
    public function isAuthorized($user)
    {
        /* Stuff what normal user can do */
        if( in_array( $this->request->action , ['index','view'] ) ) 
        {
            return true;
        }
            
        return parent::isAuthorized($user);
    }
    
    public $components=array('RequestHandler');
    
    /**
	 * Description: Main page - Films type list
	 * Function: index
	 * @author: HuyDo
	 * @params: $search
	 * @return: boolean
	 * @version: 1.0
	 */
    public function index($search = null)
    {
        //Set page Title
        $this->set('title', 'Films list');
   
        //debug($this->request->params);
           
        if($this->request->is('post'))
        {
            //List all users search condition
            $this->paginate = [
                'limit' => 8,
                'order' => [
                    'MFilms.id' => 'desc'
                ]
            ];
            
            $search = $this->request->data['search'];
            $MFilms = TableRegistry::get('MFilms');
            $query = $MFilms->query();
            $query->where(['OR'=>[
                                'model LIKE' => '%'.$search.'%',
                                'film_type LIKE' => '%'.$search.'%',
                                'film_name LIKE' => '%'.$search.'%',
                                'exp LIKE' => '%'.$search.'%',
                                'iso LIKE' => '%'.$search.'%',
                                'made_in LIKE' => '%'.$search.'%'
                                ]
            ]);
            $films = $this->paginate($query);
        }
        else
        {
            //List all users without condition
            $this->paginate = [
                'limit' => 8,
                'order' => [
                    'MFilms.id' => 'desc'
                ]
            ];
            $films = $this->paginate($this->MFilms);
        }
        //Return the taken results
        $this->set(compact('films', $films));
    }
    
    /**
	 * Description: Create new film record
	 * Function: add
	 * @author: HuyDo
	 * @params: none
	 * @return: boolean
	 * @version: 1.0
	 */
    public function add()
    {
        //Set page Title
        $this->set('title', 'Add new film');
        
        $film = $this->MFilms->newEntity();
        if ($this->request->is('post')) {
            $film = $this->MFilms->patchEntity($film, $this->request->data);
            
            //Get upload file info
            $file = $this->request->data['film_image_file'];
            
            // Check avatar will upload or not.
            if( $file['name'] != "" )
            {
                
                //debug($file);
                if( in_array( $file['type'] , array('image/png','image/jpg','image/jpeg','image/gif') ) )
                {
                    if( $file['size'] >= 1000000 )
                    {
                        $this->Flash->error(__( $GLOBALS['msg']['error']['file-size'] ));
                        return $this->redirect('/new-film');
                    }
                }
                else
                {
                    $this->Flash->error(__( $GLOBALS['msg']['error']['file-type'] ));
                    return $this->redirect('/new-film');
                }
            }
            else
            {
                $film->film_image = "/img/films/default-film.jpg";
            }
            
            if ($this->MFilms->save($film)) {
                $this->Flash->success(__( $GLOBALS['msg']['success']['update-success'] ));
                return $this->redirect('/edit-film/'.$film->id);
            }
            $this->Flash->error(__( $GLOBALS['msg']['error']['update-fail'] ));
        }
        $this->set('film', $film);
    }//end add

    /**
	 * Description: Edit film info
	 * Function: edit
	 * @author: HuyDo
	 * @params: $id
	 * @return: boolean
	 * @version: 1.0
	 */
    public function edit($id = null)
    {
        //Set page Title
        $this->set('title', 'Update film infomation');
        
        //Get chosen film to edit 
        $film = $this->MFilms->get($id);
        
        if ($this->request->is(['post', 'put'])) 
        {
            $this->MFilms->patchEntity($film, $this->request->data);
            
            //Get upload file info
            $file = $this->request->data['film_image_file'];
            // Check avatar will upload or not.
            if( $file['name'] != "" )
            {
                //debug($file);
                if( in_array( $file['type'] , array('image/png','image/jpg','image/jpeg','image/gif') ) )
                {
                    if( $file['size'] >= 500*1024 )
                    {
                        $this->Flash->error(__( $GLOBALS['msg']['error']['file-size'] ));
                        return $this->redirect('/edit-film/'.$film['id']);
                    }
                }
                else
                {
                    $this->Flash->error(__( $GLOBALS['msg']['error']['file-type'] ));
                    return $this->redirect('/edit-film/'.$film['id']);
                }
            }
            
            if ($this->MFilms->save($film)) 
            {
                $this->Flash->success(__( $GLOBALS['msg']['success']['update-success'] ));
                return $this->redirect('/edit-film/'.$film['id']);
            }
            $this->Flash->error(__( $GLOBALS['msg']['error']['update-fail'] ));
        }
    
        $this->set('film', $film);
    }//end edit
    
    /**
	 * Description: View film info
	 * Function: view
	 * @author: HuyDo
	 * @params: $id
	 * @return: boolean
	 * @version: 1.0
	 */
    public function view($id = null)
    {
        //Set page Title
        $this->set( 'title', 'View film infomation' );
        
        //Get chosen film to edit 
        $this->set( 'film', $this->MFilms->get($id) );
    }//end view
    

    /*
    *
    * -------------------------
    * The functional functions
    * -------------------------
    *
    */
    
    /**
	 * Description: ADMIN role - Discontinue product film
	 * Function: deactivate
	 * @author: HuyDo
	 * @params: none
	 * @return: boolean
	 * @version: 1.0
	 */
    public function deactivate()
    {
        if ($this->request->is(['post', 'put']))
        {
            $id = $this->request->data['id'];
            $MFilms = TableRegistry::get('MFilms');
            $query = $MFilms->query();
            $query->update()
                ->set(['is_unproductive' => '1',
                    'modified' => $this->request->data['modified']
                ])
                ->where(['id' => $id])
                ->execute();
            if( $query != false)
            {
                $this->Flash->success(__( $GLOBALS['msg']['success']['deactivate-film'] ));
                return $this->redirect('/edit-film/'.$id);
            }
            else
            {
                $this->Flash->error(__( $GLOBALS['msg']['error']['update-fail'] ));
                return $this->redirect('/edit-film/'.$id);
            }
        }
        $this->Flash->error(__( $GLOBALS['msg']['error']['invalid-request'] ));
        return $this->redirect('/film-list');
    }
    
    /**
	 * Description: ADMIN role - Active film
	 * Function: activate
	 * @author: HuyDo
	 * @params: none
	 * @return: boolean
	 * @version: 1.0
	 */
    public function activate()
    {
        if ($this->request->is(['post', 'put']))
        {
            $id = $this->request->data['id'];
            $MFilms = TableRegistry::get('MFilms');
            $query = $MFilms->query();
            $query->update()
                ->set(['is_unproductive' => '0',
                    'modified' => $this->request->data['modified']
                ])
                ->where(['id' => $id])
                ->execute();
            if( $query != false)
            {
                $this->Flash->success(__( $GLOBALS['msg']['success']['active-film'] ));
                return $this->redirect('/edit-film/'.$id);
            }
            else
            {
                $this->Flash->error(__( $GLOBALS['msg']['error']['update-fail'] ));
                return $this->redirect('/edit-film/'.$id);
            }
        }
        $this->Flash->error(__( $GLOBALS['msg']['error']['invalid-request'] ));
        return $this->redirect('/film-list');
    }
    //** ADMIN role **//
    
    /**
	 * Description: Ajax function, solve the request from ajax
	 * Function: ajax
	 * @author: HuyDo
	 * @params: none
	 * @return: boolean
	 * @version: 1.0
	 */
    public function ajax()
    {
        $data = array();

        if( isset( $this->request->data[ 'model' ] ) ) 
        {
            $data = $this->__getModelList();
        }
        elseif( isset( $this->request->data[ 'type' ] ) ) 
        {
            $data = $this->__getFilmTypeList();
        }
        elseif( isset( $this->request->data[ 'film_name' ] ) ) 
        {
            $data = $this->__getFilmName($this->request->data[ 'film_name' ]);
        }
        
        echo json_encode( [ 'data' => $data ] );
        exit();
    }
    
    /**
    * Description: Get data for export csv hot_videos page
    * Function: __getModelList()
    * @author: HuyDo
    * @params: none
    * @return: array data
    */
    private function __getModelList()
    {
        return $this->MFilms->find( 'all' )->select('model')->distinct('model')->where( [ 'is_delete' => '0' ] )->all();
    }
    
    /**
    * Description: Get data for export csv hot_videos page
    * Function: __getModelList()
    * @author: HuyDo
    * @params: none
    * @return: array data
    */
    private function __getFilmTypeList()
    {
        return $this->MFilms->find( 'all' )->select('film_type')->distinct('film_type')->where( [ 'is_delete' => '0' ] )->all();
    }
    
    /**
    * Description: Check unique of film name
    * Function: __getFilmName()
    * @author: HuyDo
    * @params: none
    * @return: array data
    */
    private function __getFilmName($name = null)
    {
        return $this->MFilms->find( 'all' )->select('film_name')->where( [ 'is_delete' => '0' ] )->all();
    }

}//end class

?>