<!-- src/Template/MLabs/view.ctp -->
<?= $this->Flash->render('flash') ?>

    <div class="row">
        
        <!-- left column -->
        <div class="col-md-4">
            <!-- general form elements -->
            <div class="box box-warning">
                <div class="box-body text-c">
                    <?= $this->Html->image($labs->lab_image, ['class'=>'w100p']) ?>
                    <caption><i><?= $labs['lab_name']?></i></caption>
                </div>
            </div>
            <!--/.box -->
        </div>
        <!--/.col (left) -->

        <!-- right column -->
        <div class="col-md-8">
            <!-- general form elements -->
            <div class="box box-warning">
                <div class="box-header with-border ml20">
                    <h3><?= $labs['lab_name'] ?></h3>
                </div>

                <div class="box-body fsize16">
                    <table class="ml20">
                        <tr>
                            <td class="w100"><b>Address:</b></td>
                            <td><?= $labs['lab_address'] ?></td>
                        </tr>
                        <tr>
                            <td><b>Phone:</b></td>
                            <td><?= $labs['lab_phone'] ?></td>
                        </tr>
                        <tr>
                            <td><b>Website:</b></td>
                            <td><?= $labs['lab_website'] ?></td>
                        </tr>
                        <?php if( $labs['is_close'] == 1 ): ?>
                        <tr>
                            <td><b>Status:</b></td>
                            <td>
                                <span class='red-text'>This lab have been closed</span>
                            </td>
                        </tr>
                        <?php endif; ?>
                        
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
            
            <?php if( $labs['map_link'] != "" ): ?>
            <div class="box box-warning">
                <iframe src="<?= $labs['map_link'] ?>" width="100%" height="auto" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
            <?php endif; ?>
            
            <?= $this->Html->link('<i class="fa fa-film"></i> Back to list', '/lab',  ['class' => 'btn btn-app fr','id'=>'list_btn','target' => '','escape' => false]); ?>  
        </div>
        <!--/.col (right) -->
        
        <!-- below column -->
        <div class="col-md-12">
            <div class="box box-warning">
                <div class="box-header with-border">
                    <h4>Sample photos of this lab developed</h4>
                </div>
                <div class="box-body fsize18">
                    <?= $this->Html->image('login-bg.jpg', ['class'=>'w300 border ml10 mr20']) ?>
                    <?= $this->Html->image($labs->lab_image, ['class'=>'w300 border mr20']) ?>
                    <?= $this->Html->image($labs->lab_image, ['class'=>'w300 border mr20']) ?>
                    <?= $this->Html->image('login-bg.jpg', ['class'=>'w300 border mr20']) ?>
                </div>
                <div class="box-footer">
                    <?= $this->Html->link('<i class="fa fa-hand-o-right"></i> See more...', '/photo',  ['class' => 'text-bold','id'=>'see_more','target' => '','escape' => false]); ?>  
                </div>
            </div>
            <!-- /.box --> 
        </div>
        <!--/.col (below) -->
        
    </div>
    <!-- /.row -->