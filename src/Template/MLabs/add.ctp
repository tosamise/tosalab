<!-- src/Template/MLabs/add.ctp -->
<?= $this->Flash->render('flash') ?>

<div class="row">
    <!-- left column -->
    <div class="col-md-8">
        <!-- general form elements -->
        <div class="box box-warning">
            <div class="box-body">
                <div class="users form">
                <?= $this->Form->create($lab, ['enctype' => 'multipart/form-data','id'=>'add_lab_form']) ?>
                  
                    <div class="form-group">
                        <label>Lab name:</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-flask"></i>
                            </div>
                            <?= $this->Form->input('lab_name', ['class'=>'form-control','id'=>'lab_name','placeholder'=>'','div'=>false,'label'=>'','error'=>false] ); ?>
                        </div>
                        <?= $this->Form->error('lab_name', null, array('class' => 'error-message','div'=>false)); ?>
                    </div>
                    
                    <div class="form-group">
                        <label>Address:</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-map-marker"></i>
                            </div>
                            <?= $this->Form->input('lab_address', ['class'=>'form-control','id'=>'lab_address','placeholder'=>'','div'=>false,'label'=>'','error'=>false] ); ?>
                        </div>
                        <?= $this->Form->error('lab_address', null, array('class' => 'error-message','div'=>false)); ?>
                    </div>
                    
                    <div class="form-group">
                        <label>Phone:</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-phone"></i>
                            </div>
                            <?= $this->Form->input('lab_phone', ['class'=>'form-control','id'=>'lab_phone','placeholder'=>'','div'=>false,'label'=>'','error'=>false] ); ?>
                        </div>
                        <?= $this->Form->error('lab_phone', null, array('class' => 'error-message','div'=>false)); ?>
                    </div>
                    
                    <div class="form-group">
                        <label>Website:</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-link"></i>
                            </div>
                            <?= $this->Form->input('lab_website', ['class'=>'form-control','id'=>'lab_website','placeholder'=>'','div'=>false,'label'=>'','error'=>false] ); ?>
                        </div>
                        <?= $this->Form->error('lab_website', null, array('class' => 'error-message','div'=>false)); ?>
                    </div>
                    
                    <div class="form-group">
                        <label>Map:</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-map"></i>
                            </div>
                            <?= $this->Form->input('map_link', ['class'=>'form-control','id'=>'map_link','placeholder'=>'','div'=>false,'label'=>'','error'=>false,'rows'=>5,'columns'=>20] ); ?>
                        </div>
                        <?= $this->Form->error('map_link', null, array('class' => 'error-message','div'=>false)); ?>
                    </div>
                    
                    <div class="form-group">
                        <label>Lab image:</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-photo"></i>
                            </div>
                            <?= $this->Form->file('lab_image_file', ['class'=>'form-control','id'=>'lab_image','div'=>false,'label'=>'','error'=>false] ); ?>
                        </div>
                        <?= $this->Form->error('lab_image', null, array('class' => 'error-message','div'=>false)); ?>
                    </div>
                    
                    <?= $this->Form->hidden('created', ['value'=> date('Y-m-d H:i:s')] );?>
                
                <?= $this->Form->button(__('<i class="fa fa-repeat"></i> Clear All'), ['type'=>'button','id'=>'clearAllbtn','class'=>'btn btn-app fr','escape'=>false]); ?>    
                <?= $this->Form->button(__('<i class="fa fa-save"></i> Save'), ['class'=>'btn btn-app fr','id'=>'add_lab_btn','escape'=>false]); ?>
                <?= $this->Form->end() ?>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <!--/.col (right) -->
    
</div>
<!-- /.row -->
<script type="text/javascript">
    
    $.validator.addMethod('validPhone', function (value, element) {
        // return this.optional(element) || /^[0]{1}[19]{1}[0-9]{8}$/i.test(value);
        if( value.length != 0 && value.length == 10)
        {
            return this.optional(element) || /^[0]{1}[9]{1}[0-9]{8}$/i.test(value);           
        }
        else if( value.length != 0 && value.length == 11 )
        {
            return this.optional(element) || /^[0]{1}[1]{1}[0-9]{9}$/i.test(value);
        }
    }, "Please enter a valid phone number");

	$().ready(function() {
	    $("#add_lab_form").validate({
			rules: {
				lab_name: "required",
				lab_address: "required",
				lab_phone: {
				     required : false,  
				     validPhone : true,
				}
			},
			messages: {
				lab_name: "Please enter lab name",
				lab_address: "Please enter your nice name",
			},
			highlight: function(element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-group').removeClass('has-error');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function(error, element) {
                if(element.parents('.input-group').length) {
                    error.insertAfter(element.parents(".input-group"));
                } else {
                    error.insertAfter(element);
                }
            }
	    }); 
	    
	});
	
	$('#add_lab_btn').on('click',function(e){
    e.preventDefault();
    var form = $('#add_lab_btn').parents('#add_lab_form');
        swal({
            title: "Are you sure?",
            text: "You will add new camera!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#008d4c",
            confirmButtonText: "Yes, do it!",
            closeOnConfirm: true
        }, function(isConfirm){
            if (isConfirm) form.submit();
        });
    });
</script>