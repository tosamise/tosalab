<!-- Photo Gallery -->
<?php $LoggedUser = $this->request->session()->read('Auth.User'); ?>
<div class="row">
    <div class="col-md-12 ">
        <div class="box box-warning">
            <div class="box-header with-border">
                
                <?= $this->Html->link('<i class="fa fa-upload"></i> Upload', '#uploadMediaImageForm',['data-effect'=>'mfp-3d-unfold','id'=>'upload','class'=>'uploadMediaImageForm btn btn-app ml10','escape'=>false]); ?>

                <div class="box-tools pull-right">
                    <span class="label label-danger">
                        <?php echo $this->Paginator->counter(['format' => 'Page {{page}} of {{pages}}' ]); ?>
                    </span>
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                
                <?php foreach($photos as $photo): ?>
                <div class="col-md-3 minh150 minw150">
                    <div class="box">
                        <div class="box-body text-c">
                            <?php 
                            $img = $this->Html->image($photo->file_name, ['class'=>'w100p']);
                            echo $this->Html->link( $img ,'#', ['class' => '', 'target' => '','escape' => false]); 
                            ?>
                            <div class="cover-box"></div>
                        </div>
                    </div>
                </div>
                <?php endforeach; ?>
                
                <!--<div class="popup-gallery">-->
                <!--	<a href="http://farm9.staticflickr.com/8242/8558295633_f34a55c1c6_b.jpg" title="The Cleaner" author="TommyDo"><img src="http://farm9.staticflickr.com/8242/8558295633_f34a55c1c6_s.jpg" width="100" height="100"></a>-->
                <!--	<a href="http://farm9.staticflickr.com/8382/8558295631_0f56c1284f_b.jpg" title="Winter Dance"><img src="http://farm9.staticflickr.com/8382/8558295631_0f56c1284f_s.jpg" width="75" height="75"></a>-->
                <!--	<a href="http://farm9.staticflickr.com/8225/8558295635_b1c5ce2794_b.jpg" title="The Uninvited Guest"><img src="http://farm9.staticflickr.com/8225/8558295635_b1c5ce2794_s.jpg" width="75" height="75"></a>-->
                <!--	<a href="http://farm9.staticflickr.com/8383/8563475581_df05e9906d_b.jpg" title="Oh no, not again!"><img src="http://farm9.staticflickr.com/8383/8563475581_df05e9906d_s.jpg" width="75" height="75"></a>-->
                <!--	<a href="http://farm9.staticflickr.com/8235/8559402846_8b7f82e05d_b.jpg" title="Swan Lake"><img src="http://farm9.staticflickr.com/8235/8559402846_8b7f82e05d_s.jpg" width="75" height="75"></a>-->
                <!--	<a href="http://farm9.staticflickr.com/8235/8558295467_e89e95e05a_b.jpg" title="The Shake"><img src="http://farm9.staticflickr.com/8235/8558295467_e89e95e05a_s.jpg" width="75" height="75"></a>-->
                <!--	<a href="http://farm9.staticflickr.com/8378/8559402848_9fcd90d20b_b.jpg" title="Who's that, mommy?"><img src="http://farm9.staticflickr.com/8378/8559402848_9fcd90d20b_s.jpg" width="75" height="75"></a>-->
                <!--</div>-->
                
            </div>
            <!-- /.box-body -->
            <div class="box-footer text-center">
                <ul class="pagination pagination-sm no-margin pull-right">
                    <?php 
                        // Shows the next links
                        echo $this->Paginator->prev('« Previous');
                        //Shows the page numbers
                        echo $this->Paginator->numbers();
                        // Shows the previous links
                        echo $this->Paginator->next('Next »');
                    ?>
                </ul>
                
                <button class="btn btn-default fl" type="button">
                    <?php echo $this->Paginator->counter(['format' => '{{current}} of {{count}}']);?>
                </button>
                
            </div>
            <!-- /.box-footer -->
        </div>
        <!-- .box -->
        
        <?= $this->Form->create('search', ['url'=>'/film','class'=>'sidebar-form m0 custom-search-bar']) ?>
          <div class="input-group">
            <?php echo $this->Form->input('Search', ['class'=>'form-control','placeholder'=>'Search films...','name'=>'search','div'=>false,'label'=>false]);?>
            <span class="input-group-btn">
              <?= $this->Form->button('<i class="fa fa-search"></i>',['class'=>'btn btn-flat','escape'=>false,'name'=>'searchbtn','id'=>'search-btn','div'=>false]) ?>
            </span>
          </div>
        <?= $this->Form->end() ?>
        
        <div id="uploadMediaImageForm" class="uploadPopup mfp-with-anim mfp-hide">
            <?= $this->Form->create($photos, ['enctype' => 'multipart/form-data', 'class'=>'dropzone','id'=>'my-awesome-dropzone']) ?>
                
                <?= $this->Form->hidden('created', ['value'=> date('Y-m-d H:i:s')] );?>
                
            <?= $this->Form->end() ?>
            <?= $this->Form->button(__('<i class="fa fa-undo"></i> Cancel'), ['id'=>'cancel-image','class'=>'btn btn-default fr mt20','escape'=>false]); ?>
            <?= $this->Form->button(__('<i class="fa fa-save"></i> Upload'), ['id'=>'upload-image','class'=>'btn btn-warning fr mr10 mt20','escape'=>false]); ?>
        </div>
        
    </div>
    <!-- .col -->
</div>

<!-- Upload javascript -->
<?php echo $this->Html->script('upload.js'); ?>