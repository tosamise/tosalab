<!-- src/Template/MPhotos/add.ctp -->
<?= $this->Flash->render('flash') ?>

<div class="row">
    <!-- left column -->
    <div class="col-md-8">
        <!-- general form elements -->
        <div class="box box-warning">
            <div class="box-body">
                <div class="users form">
                <?= $this->Form->create($photos, ['enctype' => 'multipart/form-data', 'class'=>'dropzone','id'=>'my-awesome-dropzone']) ?>
                
                   <?php //$this->Form->file('file_name', ['class'=>'form-control','id'=>'file_name','multiple'=>true,'div'=>false,'label'=>'','error'=>false] ); ?>
                    
                    <?= $this->Form->hidden('created', ['value'=> date('Y-m-d H:i:s')] );?>
                
                <?php //$this->Form->button(__('<i class="fa fa-repeat"></i> Clear All'), ['type'=>'button','id'=>'upload-image','class'=>'btn btn-app fr','escape'=>false]); ?>    
                <?= $this->Form->button(__('<i class="fa fa-save"></i> Save'), ['id'=>'','class'=>'btn btn-app fr','escape'=>false]); ?>
                <?= $this->Form->end() ?>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <!--/.col (right) -->
    
</div>
<!-- /.row -->

<!-- Upload javascript -->
<?php echo $this->Html->script('upload.js'); ?>

