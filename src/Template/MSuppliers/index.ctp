<!-- Categories index -->
<?php $LoggedUser = $this->request->session()->read('Auth.User'); ?>
<?= $this->Flash->render('flash') ?>
<div class="row">
    <div class="col-md-12 ">
        <div class="box box-warning">
            <div class="box-header with-border">
                <h3 class="box-title cur-point" id="add_new">Add new </h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
            </div>
            <!-- /.box-header -->
            <div id="add_new_wrap" class="box-body display-n">
                <div class="users form">
                <?= $this->Form->create(null, ['url' => 'add-supplier','id'=>'add_sup_form']) ?>
                  
                    <div class="form-group">
                        <label>Name:</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-font"></i>
                            </div>
                            <?= $this->Form->input('sup_name', ['class'=>'form-control','id'=>'sup_name','placeholder'=> 'name','div'=>false,'label'=>'','error'=>false] ); ?>
                        </div>
                        <?= $this->Form->error('sup_name', null, array('class' => 'error-message','div'=>false)); ?>
                    </div>

                    <div class="form-group">
                        <label>Phone:</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-phone"></i>
                            </div>
                            <?= $this->Form->input('sup_phone', ['class'=>'form-control','id'=>'sup_phone','placeholder'=>'phone','div'=>false,'label'=>'','error'=>false] ); ?>
                        </div>
                        <?= $this->Form->error('sup_phone', null, array('class' => 'error-message','div'=>false)); ?>
                    </div>
                    
                    <div class="form-group">
                        <label>Address:</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-map"></i>
                            </div>
                            <?= $this->Form->input('sup_address', ['class'=>'form-control','id'=>'sup_address','placeholder'=>'address','div'=>false,'label'=>'','error'=>false] ); ?>
                        </div>
                        <?= $this->Form->error('sup_address', null, array('class' => 'error-message','div'=>false)); ?>
                    </div>
                    
                    <div class="form-group">
                        <label>Website:</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-link"></i>
                            </div>
                            <?= $this->Form->input('sup_website', ['class'=>'form-control','id'=>'sup_website','placeholder'=>'website','div'=>false,'label'=>'','error'=>false] ); ?>
                        </div>
                        <?= $this->Form->error('sup_website', null, array('class' => 'error-message','div'=>false)); ?>
                    </div>
                    
                    <div class="form-group">
                        <label>Bank infomation:</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-university"></i>
                            </div>
                            <?= $this->Form->input('bank_info', ['class'=>'form-control','id'=>'bank_info','placeholder'=>'','div'=>false,'label'=>'','error'=>false,'type'=>'textarea'] ); ?>
                        </div>
                        <?= $this->Form->error('bank_info', null, array('class' => 'error-message','div'=>false)); ?>
                    </div>
                    
                    <div class="form-group">
                    <?= $this->Form->input('tags_id', [
                            'options' => $tags,
                            'id' => 'tags',
                            'class'=>'',
                            'multiple' => true,
                            'error'=>false
                        ]) ?>
                        <?= $this->Form->error('tags_id[]', null, array('class' => 'error-message','div'=>false)); ?>
                    </div>

                    <?= $this->Form->hidden('created', ['value'=> date('Y-m-d H:i:s')] );?>
                
                <?= $this->Form->button(__('<i class="fa fa-repeat"></i> Clear All'), ['type'=>'button','id'=>'clearAllbtn','class'=>'btn btn-app','escape'=>false]); ?>        
                <?= $this->Form->button(__('<i class="fa fa-save"></i> Add'), ['class'=>'btn btn-app','id'=>'add_sup_btn','escape'=>false]); ?>
                <?= $this->Form->end() ?>
                </div>
            </div>
            <!-- /.box-body -->
            
            <div class="box-header with-border">
                <h3 class="box-title">Suppliers list</h3>
        
                <div class="box-tools pull-right">
                    <span class="label label-danger">
                        <?php echo $this->Paginator->counter(['format' => 'Page {{page}} of {{pages}}' ]); ?>
                    </span>
                </div>
            </div>
            <!-- /.box-header -->
            
            <div class="box-body">
                <div class="col-xs-12 col-md-8">
                    <div class="box box-warning">
                        <div class="box-body table-responsive no-padding">
                            <table class="table table-hover">
                            <tr>
                                <th class="hidden-xs">No.</th>
                                <th>Name</th>
                                <th>Phone</th>
                                <th>Address</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            <?php $no = 1; ?>
                            <?php foreach($suppliers as $sup): ?>
                            <tr class="sup_show">
                                <td class="hidden-xs"><?= $no ?></td>
                                <td><?= $sup->sup_name ?></td>
                                <td><?= $sup->sup_phone ?></td>
                                <td><?= $sup->sup_address ?></td>
                                <td>
                                    <?php if($sup->is_delete == 0): ?>
                                    <span class="label label-success">Open</span>
                                    <?php elseif($sup->is_delete == 1): ?>
                                    <span class="label label-danger">Close</span>
                                    <?php endif; ?>
                                </td>
                                <td>
                                    <?php if($sup->is_delete == 0): ?>
                                    <a href="#edit_sup_popup" class="btn btn-default view-sup-btn popup-modal" title="View Details">
                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                    </a>
                                    <input type="hidden" id="data_id" value="<?=$sup->id ?>" />
                                    <input type="hidden" id="data_name" value="<?=$sup->sup_name ?>" />
                                    <input type="hidden" id="data_phone" value="<?=$sup->sup_phone ?>" />
                                    <input type="hidden" id="data_address" value="<?=$sup->sup_address ?>" />
                                    <input type="hidden" id="data_website" value="<?=$sup->sup_website ?>" />
                                    <input type="hidden" id="data_bank_info" value="<?=$sup->bank_info ?>" />
                                    <input type="hidden" id="data_tags" value="<?=$sup->tags_id ?>" />
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <?php $no++; ?>
                            <?php endforeach; ?>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer text-center">
                <ul class="pagination pagination-sm no-margin pull-right">
                    <?php 
                        // Shows the next links
                        echo $this->Paginator->prev('« Previous');
                        //Shows the page numbers
                        echo $this->Paginator->numbers();
                        // Shows the previous links
                        echo $this->Paginator->next('Next »');
                    ?>
                </ul>
                
                <button class="btn btn-default fl" type="button">
                    <?php echo $this->Paginator->counter(['format' => '{{current}} of {{count}}']);?>
                </button>
                
            </div>
            <!-- /.box-footer -->
        </div>
        <!-- .box -->
        
        <?= $this->Form->create('search', ['url'=>'/suppliers','class'=>'sidebar-form m0 custom-search-bar']) ?>
          <div class="input-group">
            <?php echo $this->Form->input('Search', ['class'=>'form-control','placeholder'=>'Search...','name'=>'search','div'=>false,'label'=>false]);?>
            <span class="input-group-btn">
              <?= $this->Form->button('<i class="fa fa-search"></i>',['class'=>'btn btn-flat','escape'=>false,'name'=>'searchbtn','id'=>'search-btn','div'=>false]) ?>
            </span>
          </div>
        <?= $this->Form->end() ?>
        
    </div>
    <!-- .col -->
</div>

<!-- Popup edit -->
<div id="edit_sup_popup" class="mfp-hide white-popup-block uploadPopup" style="border: 3px solid #f39c12;">
	<h3>"<span id="h3_sup_name"></span>" details</h3>
	<div class="box-body">
        <div class="users form">
        <?= $this->Form->create(null, ['url' => 'edit-supplier','id'=>'edit_sup_form']) ?>
          
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-font"></i>
                    </div>
                    <?= $this->Form->input('sup_name', ['class'=>'form-control','id'=>'edit_sup_name','placeholder'=> 'name','div'=>false,'label'=>'','error'=>false] ); ?>
                </div>
                <?= $this->Form->error('sup_name', null, array('class' => 'error-message','div'=>false)); ?>
            </div>

            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-phone"></i>
                    </div>
                    <?= $this->Form->input('sup_phone', ['class'=>'form-control','id'=>'edit_sup_phone','placeholder'=>'phone','div'=>false,'label'=>'','error'=>false] ); ?>
                </div>
                <?= $this->Form->error('sup_phone', null, array('class' => 'error-message','div'=>false)); ?>
            </div>
            
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-map"></i>
                    </div>
                    <?= $this->Form->input('sup_address', ['class'=>'form-control','id'=>'edit_sup_address','placeholder'=>'address','div'=>false,'label'=>'','error'=>false] ); ?>
                </div>
                <?= $this->Form->error('sup_address', null, array('class' => 'error-message','div'=>false)); ?>
            </div>
            
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-addon">
                        <a href="#" id="web_link" target="_blank"><i class="fa fa-link"></i></a>
                    </div>
                    <?= $this->Form->input('sup_website', ['class'=>'form-control','id'=>'edit_sup_website','placeholder'=>'website','div'=>false,'label'=>'','error'=>false] ); ?>
                </div>
                <?= $this->Form->error('sup_website', null, array('class' => 'error-message','div'=>false)); ?>
            </div>
            
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-university"></i>
                    </div>
                    <?= $this->Form->input('bank_info', ['class'=>'form-control','id'=>'edit_bank_info','placeholder'=>'','div'=>false,'label'=>'','error'=>false,'type'=>'textarea'] ); ?>
                </div>
                <?= $this->Form->error('bank_info', null, array('class' => 'error-message','div'=>false)); ?>
            </div>
            
            <div class="form-group">
                <select id="edit_tags" name="tags_id[]" multiple placeholder="Select a tag...">
                    <option value="">Select a tag...</option>
                    <?php if( ! empty( $tags ) ) : ?>
                        <?php foreach( $tags as $tag ) : ?>
                            <option value="<?php echo $tag->id; ?>"><?php echo $tag->tag; ?></option>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </select>
                <?= $this->Form->error('tags_id[]', null, array('class' => 'error-message','div'=>false)); ?>
            </div>

            <?= $this->Form->hidden('modified', ['value'=> date('Y-m-d H:i:s')] );?>
            <?= $this->Form->hidden('id', ['value'=> '', 'id'=>'edit_id']);?>
        
        <?= $this->Form->button(__('<i class="fa fa-trash-o"></i> Delete'), ['class'=>'btn btn-app del-btn','id'=>'del_sup_btn','escape'=>false]); ?>    
        <?= $this->Form->button(__('<i class="fa fa-stop"></i> Cancel'), ['class'=>'btn btn-app fr','id'=>'cancel_sup_btn','escape'=>false]); ?>
        <?= $this->Form->button(__('<i class="fa fa-save"></i> Save'), ['class'=>'btn btn-app fr','id'=>'edit_sup_btn','escape'=>false]); ?>
        <?= $this->Form->end() ?>
        </div>
    </div>
    <!-- /.box-body -->
</div>

<script type="text/javascript">
    $().ready(function() {
	    $("#add_sup_form").validate({
			rules: {
				sup_name: "required",
				sup_phone: {
				    minlength: 10,
				    maxlength: 12,
				    validPhone: true,
				},
				sup_website: {
				    validURL: true,
				}
			},
			messages: {
				sup_name: "This field is required",
				sup_phone: {
				    minlength: "Phone has at least 10 digits",
				    maxlength: "Phone has maximum 12 digits",
				    validPhone: "Phone must be digits only",
				},
				sup_website: "Invalid link type",
			},
			highlight: function(element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-group').removeClass('has-error');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function(error, element) {
                if(element.parents('.input-group').length) {
                    error.insertAfter(element.parents(".input-group"));
                } else {
                    error.insertAfter(element);
                }
            }
	    });
	    
	    $("#edit_sup_form").validate({
			rules: {
				sup_name: "required",
				sup_phone: {
				    minlength: 10,
				    maxlength: 12,
				    validPhone: true,
				},
				sup_website: {
				    validURL: true,
				}
			},
			messages: {
				sup_name: "This field is required",
				sup_phone: {
				    minlength: "Phone has at least 10 digits",
				    maxlength: "Phone has maximum 12 digits",
				    validPhone: "Phone must be digits only",
				},
				sup_website: "Invalid link type",
			},
			highlight: function(element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-group').removeClass('has-error');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function(error, element) {
                if(element.parents('.input-group').length) {
                    error.insertAfter(element.parents(".input-group"));
                } else {
                    error.insertAfter(element);
                }
            }
	    });
	    
	    $.validator.addMethod("validPhone", function(value, element) {
            return this.optional(element) || /\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/.test(value); 
        });
	    $.validator.addMethod("validURL", function(value, element) {
	        var regex = "";
            return this.optional(element) || /^(https?|s?ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(value); 
        });

    $(function () {
    	$('.popup-modal').magnificPopup({
    		type: 'inline',
    		preloader: false,
    		focus: '#edit_sup_name',
    		modal: true
    	});
    	$(document).on('click', '#cancel_sup_btn', function (e) {
    		e.preventDefault();
    		$.magnificPopup.close();
    	});
    });
    
    $(function() {
        $('#tags').selectize({
            maxItems: 5,
            persist: false,
            createOnBlur: true,
            create: true
        });
    });
    
    var $select = $('#edit_tags').selectize({
            maxItems: 5,
            persist: false,
            createOnBlur: true,
            create: true
        })[0].selectize;
        
    //Handler show the input to edit the content
    $( ".view-sup-btn" ).each(function(index) {
        $(this).on("click", function(){
           var a = $(this).nextAll("#data_name").val();
           var b = $(this).nextAll("#data_phone").val();
           var c = $(this).nextAll("#data_address").val();
           var d = $(this).nextAll("#data_website").val();
           var e = $(this).nextAll("#data_bank_info").val();
           var f = $(this).nextAll("#data_id").val();
           var g = $(this).nextAll("#data_tags").val();
           
           $("#edit_sup_name").val(a);
           $("#h3_sup_name").text(a)
           $("#edit_sup_phone").val(b);
           $("#edit_sup_address").val(c);
           $("#edit_sup_website").val(d);
           $('#web_link').attr('href',d);
           $("#edit_bank_info").val(e);
           $("#edit_id").val(f);
           $("#del_sup_btn").attr('data',f);
           
           var tag = [];
           for(var i = 0; i <= g.length; i++)
           {
              tag.push(g[i]);
           }
           $select.setValue(tag);
           
        });
    });
	    
	});//end ready
	
	
    
	$('#add_sup_btn').on('click',function(e){
    e.preventDefault();
    var form = $('#add_sup_btn').parents('#add_sup_form');
        swal({
            title: "Are you sure?",
            text: "You will add new supplier !",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#008d4c",
            confirmButtonText: "Yes, do it!",
            closeOnConfirm: true
        }, function(isConfirm){
            if (isConfirm) form.submit();
        });
    });
    
    $('#edit_sup_btn').on('click',function(e){
    e.preventDefault();
    var form = $('#edit_sup_btn').parents('#edit_sup_form');
        swal({
            title: "Are you sure?",
            text: "You will edit supplier information !",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#008d4c",
            confirmButtonText: "Yes, do it!",
            closeOnConfirm: true
        }, function(isConfirm){
            if (isConfirm) form.submit();
        });
    });
    
    //Delete button
    $( ".del-btn" ).each(function(index) {
        $(this).on("click", function(e){
            var id = $(this).attr('data');
            e.preventDefault();
            swal({
            title: "Are you sure?",
            text: "This supplier record will be deleted",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#008d4c",
            confirmButtonText: "Yes, do it!",
            closeOnConfirm: true
            }, function(isConfirm){
                if(isConfirm){
                    //Call ajax function to update the editing
                    $.ajax({
                    type: "POST",
                    url: "/ajax-sup",
                    async: false,
                    data: { 'del': {'id': id } },
                    	success: function(data){
                    		if(data != "")
                    		{
                    			//Parse data got from server as JSON
                    			obj = jQuery.parseJSON(data);
                                if(obj['data'] == true){
                                    swal("Delete", "Record has been deleted", "success");
                                    location.reload();
                                }else{
                    			    swal("Opps!", "Something wrong, please refresh page", "error");
                                }
                    		}//end if
                    	},
                    	error: function(){
                            swal("Opps!", "Something wrong, please refresh page", "error");
                    	}
                    });//end ajax
                }else{
                    return false;
                }
            });

        });
    });
    
    $('#add_new').on('click',function(e){
        $("#add_new_wrap").slideToggle();
    });
    
    // $('#cate_code').on('change keyup',function(e){
    //     var a = $(this).val();
    //     a = a.toUpperCase();
    //     $(this).val(a);
    // });
</script>