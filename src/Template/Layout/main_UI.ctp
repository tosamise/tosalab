<!DOCTYPE html>
<html>
<head>
    <?php echo $this->Html->charset(); ?>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="google-site-verification" content="iIrtf2MQVWfXDbCroelfGASUs4X2y_FEOb5GuO7fyvI" />
    
    <title>ToSaLab</title>
    
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    
    <!-- Jquery UI -->
    <?php echo $this->Html->css('jquery/jquery-ui.css'); ?>
    <!-- Bootstrap 3.3.6 -->
    <?php echo $this->Html->css('bootstrap/css/bootstrap.min.css'); ?>
    <!-- Font Awesome -->
    <?php echo $this->Html->css('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css'); ?>
    <!-- Ionicons -->
    <?php echo $this->Html->css('https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css'); ?>
    <!-- Sweet Alert -->
    <?php echo $this->Html->css('plugins/sweetalert/sweetalert.css'); ?>
    <!-- Magnific Popup -->
    <?php echo $this->Html->css('plugins/magnific-popup/magnific-popup.css'); ?>
    <!-- Dropzone -->
    <?php echo $this->Html->css('plugins/dropzone/dropzone.css'); ?>
    <!-- Selectize -->
    <?php echo $this->Html->css('plugins/selectize/selectize.css'); ?>
    <?php echo $this->Html->css('plugins/selectize/selectize.bootstrap3.css'); ?>
    <?php echo $this->Html->css('plugins/selectize/selectize.default.css'); ?>
    <!-- RangeSlider -->
    <?php echo $this->Html->css('plugins/rangeslider/rangeslider.css'); ?>
    <!-- Theme style -->
    <?php echo $this->Html->css('main.css'); ?>
    <!-- Skin for template -->
    <?php echo $this->Html->css('skin-yellow.css'); ?>
    <!-- Custom common CSS -->
    <?php echo $this->Html->css('common.css'); ?>
    <!-- Custom CSS -->
    <?php echo $this->Html->css('custom.css'); ?>
    
    <!-- jQuery 2.2.0 -->
    <?php echo $this->Html->script('//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js'); ?>
    <!-- JQuery UI -->
    <?php echo $this->Html->script('jquery/jquery-ui.js'); ?>
    <!-- Validaion Plugin -->
    <?php echo $this->Html->script('plugins/validation/jquery.validate.min.js'); ?>
    <!-- Validaion Plugin Method -->
    <?php echo $this->Html->script('plugins/validation/additional-methods.min.js'); ?>
    <!-- Bootstrap 3.3.6 -->
    <?php echo $this->Html->script('bootstrap/bootstrap.min.js'); ?>
    <!-- AdminLTE App -->
    <?php echo $this->Html->script('app.js'); ?>
    <!-- iCheck -->
    <?php echo $this->Html->script('plugins/iCheck/icheck.min.js'); ?>
    <!-- inputMask -->
    <?php echo $this->Html->script('plugins/inputmask/jquery.inputmask.js'); ?>
    <!-- Sweet Alert -->
    <?php echo $this->Html->script('plugins/sweetalert/sweetalert.min.js'); ?>
    <!-- Magnific Popup -->
    <?php echo $this->Html->script('plugins/magnific-popup/jquery.magnific-popup.min.js'); ?>
    <!-- Dropzone -->
    <?php echo $this->Html->script('plugins/dropzone/dropzone.js'); ?>
    <!-- RangeSlider -->
    <?php echo $this->Html->script('plugins/rangeslider/rangeslider.min.js'); ?>
    <!-- Selectize -->
    <?php echo $this->Html->script('plugins/selectize/selectize.min.js'); ?>
    
    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition skin-yellow sidebar-mini">
<div class="wrapper">
  
    <div id="divLoading" class=""></div>

    <?php echo $this->element('main_UI/header'); ?>
  
    <?php echo $this->element('main_UI/sidebar'); ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    
    <section class="content-header">
        
      <h1><?php if(isset($title)){echo $title;} ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class='active'>
        <?php
            $con = $this->request->param('controller');
            $act = $this->request->param('action');
            switch ($con.'|'.$act) 
            {
            //Pages
            case 'Pages|dashboard' :
                echo("<a href='/'>Dashboard</a>");
                break;
            //Users
            case 'MUsers|add' :
                echo("<a href='/register'>".$title."</a>");
                break;
            case 'MUsers|index' :
                echo("<a href='/user-list'>".$title."</a>");
                break;
            //Camera
            case 'MCameras|add' :
                echo("<a href='/new-camera'>".$title."</a>");
                break;
            case 'MCameras|index' :
                echo("<a href='/camera'>".$title."</a>");
                break;
            //Labs
            case 'MLabs|add' :
                echo("<a href='/new-lab'>".$title."</a>");
                break;
            case 'MLabs|index' :
                echo("<a href='/lab'>".$title."</a>");
                break;
            //Films
            case 'MFilms|add' :
                echo("<a href='/new-film'>".$title."</a>");
                break;
            case 'MFilms|index' :
                echo("<a href='/film'>".$title."</a>");
                break;
            //Photos
            case 'MPhotos|index' :
                echo("<a href='/gallery'>".$title."</a>");
                break;
            //ToSa L'amour
            //Products
            case 'MProducts|add' :
                echo("<a href='/add-product'>".$title."</a>");
                break;
            case 'MProducts|index' :
                echo("<a href='/products?cate=".$this->request->query('cate')."'>".$title."</a>");
                break;
            //ProductsImages
            case 'MProimgs|index' :
                echo("<a href='/product-images'>".$title."</a>");
                break;
            //Categories
            case 'MCategories|index' :
                echo("<a href='/categories'>".$title."</a>");
                break;
            //Supplier
            case 'MSuppliers|index' :
                echo("<a href='/suppliers'>".$title."</a>");
                break;
            }
        ?>
        </li>
      </ol>
      
    </section>
    
    <!-- Main content -->
    <section class="content">
    
    <!-- This place for main content -->
    <?= $this->fetch('content') ?>
    
    <?php 
      //debug($userlist);
    ?>
      
      
    </section>
    <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <?php echo $this->element('main_UI/footer'); ?>

</div>
<!-- ./wrapper -->

</body>
    <!-- Main javascript -->
    <?php echo $this->Html->script('main.js'); ?>
</html>
