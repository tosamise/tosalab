<!-- Categories index -->
<?php $LoggedUser = $this->request->session()->read('Auth.User'); ?>
<?= $this->Flash->render('flash') ?>
<div class="row">
    <div class="col-md-12 ">
        <div class="box box-warning">
            
            <div class="box-header with-border">
                <h3 class="box-title">Add new</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="users form">
                <?= $this->Form->create(null, ['url' => 'add-category','id'=>'add_cate_form']) ?>
                  
                    <div class="form-group">
                        <label>Category code:</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-code"></i>
                            </div>
                            <?= $this->Form->input('cate_code', ['class'=>'form-control','id'=>'cate_code','placeholder'=>'category code','div'=>false,'label'=>'','error'=>false] ); ?>
                        </div>
                        <?= $this->Form->error('cate_code', null, array('class' => 'error-message','div'=>false)); ?>
                    </div>

                    <div class="form-group">
                        <label>Category name:</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-font"></i>
                            </div>
                            <?= $this->Form->input('cate_name', ['class'=>'form-control','id'=>'cate_name','placeholder'=>'category name','div'=>false,'label'=>'','error'=>false] ); ?>
                        </div>
                        <?= $this->Form->error('cate_name', null, array('class' => 'error-message','div'=>false)); ?>
                    </div>

                    <?= $this->Form->hidden('created', ['value'=> date('Y-m-d H:i:s')] );?>
                    
                <?= $this->Form->button(__('<i class="fa fa-save"></i> Add'), ['class'=>'btn btn-app','id'=>'add_cate_btn','escape'=>false]); ?>
                <?= $this->Form->end() ?>
                </div>
            </div>
            <!-- /.box-body -->
            
            <div class="box-header with-border">
                <h3 class="box-title">Categories list</h3>
        
                <div class="box-tools pull-right">
                    <span class="label label-danger">
                        <?php echo $this->Paginator->counter(['format' => 'Page {{page}} of {{pages}}' ]); ?>
                    </span>
                </div>
            </div>
            <!-- /.box-header -->
            
            <div class="box-body">
                <div class="col-xs-12 col-md-8">
                    <div class="box box-warning">
                        <div class="box-body table-responsive no-padding">
                            <table class="table table-hover">
                            <tr>
                                <th class="hidden-xs">No.</th>
                                <th>Code</th>
                                <th>Name</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            <?php $no = 1; ?>
                            <?php foreach($categories as $cate): ?>
                            <tr class="cate_show">
                                <td class="hidden-xs"><?= $no ?></td>
                                <td><?= $cate->cate_code ?></td>
                                <td><?= $cate->cate_name ?></td>
                                <td>
                                    <?php if($cate->is_delete == 0): ?>
                                    <span class="label label-success">Available</span>
                                    <?php elseif($cate->is_delete == 1): ?>
                                    <span class="label label-danger">Deny</span>
                                    <?php endif; ?>
                                </td>
                                <td>
                                    <?php if($cate->is_delete == 0): ?>
                                    <button class="btn btn-default edit-btn" id="edit-cate-btn" title="Edit">
                                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                    </button>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            
                            <tr class="cate_hide display-n">
                                <td class="hidden-xs"><?= $no ?></td>
                                <td class="data_code"><input value="<?= $cate->cate_code ?>" id="cate_code" class="form-control"/></td>
                                <td class="data_name"><input value="<?= $cate->cate_name ?>" id="cate_name" class="form-control"/></td>
                                <td>
                                    <?php if($cate->is_delete == 0): ?>
                                    <span class="label label-success">Available</span>
                                    <?php elseif($cate->is_delete == 1): ?>
                                    <span class="label label-danger">Deny</span>
                                    <?php endif; ?>
                                </td>
                                <td>
                                    <?php if($cate->is_delete == 0): ?>
                                    <button class="btn btn-success confirm-cate-btn" id="" data="<?= $cate->id ?>" title="Save">
                                        <i class="fa fa-check" aria-hidden="true"></i>
                                    </button>
                                    <input class="modified" type="hidden" value="<?= date('Y-m-d H:i:s') ?>">
                                    <button class="btn btn-default cancel-btn" id="cancel-cate-btn" title="Cancel">
                                        <i class="fa fa-ban" aria-hidden="true"></i>
                                    </button>
                                    <button class="btn btn-danger del-btn" id="del-cate-btn" title="Delete" data="<?= $cate->id ?>">
                                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                                    </button>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <?php $no++; ?>
                            <?php endforeach; ?>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer text-center">
                <ul class="pagination pagination-sm no-margin pull-right">
                    <?php 
                        // Shows the next links
                        echo $this->Paginator->prev('« Previous');
                        //Shows the page numbers
                        echo $this->Paginator->numbers();
                        // Shows the previous links
                        echo $this->Paginator->next('Next »');
                    ?>
                </ul>
                
                <button class="btn btn-default fl" type="button">
                    <?php echo $this->Paginator->counter(['format' => '{{current}} of {{count}}']);?>
                </button>
                
            </div>
            <!-- /.box-footer -->
        </div>
        <!-- .box -->
        
        <?= $this->Form->create('search', ['url'=>'/categories','class'=>'sidebar-form m0 custom-search-bar']) ?>
          <div class="input-group">
            <?php echo $this->Form->input('Search', ['class'=>'form-control','placeholder'=>'Search...','name'=>'search','div'=>false,'label'=>false]);?>
            <span class="input-group-btn">
              <?= $this->Form->button('<i class="fa fa-search"></i>',['class'=>'btn btn-flat','escape'=>false,'name'=>'searchbtn','id'=>'search-btn','div'=>false]) ?>
            </span>
          </div>
        <?= $this->Form->end() ?>
        
    </div>
    <!-- .col -->
</div>
<script type="text/javascript">
    $().ready(function() {
	    $("#add_cate_form").validate({
			rules: {
				cate_code: {
                    required: true,
                    requiredCaps: true,
                    maxlength: 4,
				},
				cate_name: "required",
			},
			messages: {
				cate_code: {
				    required: "This field is required",
                    requiredCaps: "Required UPPERCASE & Character only",
                    maxlength: "Category code is too long, 4 is perfect"
				},
				cate_name: "This field is required",
			},
			highlight: function(element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-group').removeClass('has-error');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function(error, element) {
                if(element.parents('.input-group').length) {
                    error.insertAfter(element.parents(".input-group"));
                } else {
                    error.insertAfter(element);
                }
            }
	    });
	    
	    $.validator.addMethod("requiredCaps", function(value, element) {
            return this.optional(element) || /[A-Z]/.test(value); 
        });
	    
	});//end ready
	
	//Handler show the input to edit the content
    $( ".edit-btn" ).each(function(index) {
        $(this).on("click", function(){
           $a = $(this).parents("tr");
           $a.addClass("display-n");
           $a.next("tr").removeClass("display-n");
        });
    });
    
	//Handler hide the input
    $( ".cancel-btn" ).each(function(index) {
        $(this).on("click", function(){
           $a = $(this).parents("tr");
           $a.addClass("display-n");
           $a.prev("tr").removeClass("display-n");
        });
    });
    
	//Delete button
    $( ".del-btn" ).each(function(index) {
        $(this).on("click", function(e){
            var id = $(this).attr('data');
            e.preventDefault();
            swal({
            title: "Are you sure?",
            text: "This category will be no longer available",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#008d4c",
            confirmButtonText: "Yes, do it!",
            closeOnConfirm: true
            }, function(isConfirm){
                if(isConfirm){
                    //Call ajax function to update the editing
                    $.ajax({
                    type: "POST",
                    url: "/ajax-cate",
                    async: false,
                    data: { 'del': {'id': id } },
                    	success: function(data){
                    		if(data != "")
                    		{
                    			//Parse data got from server as JSON
                    			obj = jQuery.parseJSON(data);
                                if(obj['data'] == true){
                                    swal("Delete", "Record has been deleted", "success");
                                    location.reload();
                                }else{
                    			    swal("Opps!", "Something wrong, please refresh page", "error");
                                }
                    		}//end if
                    	},
                    	error: function(){
                            swal("Opps!", "Something wrong, please refresh page", "error");
                    	}
                    });//end ajax
                }else{
                    return false;
                }
            });

        });
    });
	
	//Handler call AJAX to save the data
    $( ".confirm-cate-btn" ).each(function(index) {
        $(this).on("click", function(){
            
            //Get the data to edit
            var id = $(this).attr("data");
            var code = $(this).parents("tr").children(".data_code").children("#cate_code").val();
            var name = $(this).parents("tr").children(".data_name").children("#cate_name").val();
            var modified = $(this).next(".modified").val();
           
            //Call ajax function to update the editing
            $.ajax({
            type: "POST",
            url: "/ajax-cate",
            async: false,
            data: { 'edit': {'id':id,
                            'cate_code':code,
                            'cate_name':name,
                            'modified':modified,
                            }    
                    },
            	success: function(data){
            		if(data != "")
            		{
            			//Parse data got from server as JSON
            			obj = jQuery.parseJSON(data);
                        if(obj['data'] == true){
                            swal("OK", "Data has been save", "success");
                            location.reload();
                        }else{
            			    swal("Opps!", "Something wrong, please refresh page", "error");
                        }
            		}//end if
            	},
            	error: function(){
                    swal("Opps!", "Something wrong, please refresh page", "error");
            	}
            });//end ajax
            
            $a = $(this).parents("tr");
            $a.addClass("display-n");
            $a.prev("tr").removeClass("display-n");
        });
    });
	
	$('#add_cate_btn').on('click',function(e){
    e.preventDefault();
    var form = $('#add_cate_btn').parents('#add_cate_form');
        swal({
            title: "Are you sure?",
            text: "You will add new category !",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#008d4c",
            confirmButtonText: "Yes, do it!",
            closeOnConfirm: true
        }, function(isConfirm){
            if (isConfirm) form.submit();
        });
    });	
    
    $('#cate_code').on('change keyup',function(e){
        var a = $(this).val();
        a = a.toUpperCase();
        $(this).val(a);
    });
    
    $('#cate_code').on('blur change',function(e){
        $.ajax({
            type: "POST",
            url: "/ajax-cate",
            data: {check_code: $(this).val()},
            success: function(data){
                obj = jQuery.parseJSON(data);
                if(!obj["data"])
                {
                    swal("This category code was existed");
                    $("#cate_code").val("");
                }
            },
            complete: function(){
                $('#divLoading').removeClass('show');
            },
            error: function(){
                $('#divLoading').removeClass('show');
            }
        });//end ajax
    }); 
</script>