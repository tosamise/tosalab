<div class="auto_out callout callout-success" onclick="this.classList.add('hidden')">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-check"></i> Success!</h4>    
    <?= h($message) ?>
</div>
