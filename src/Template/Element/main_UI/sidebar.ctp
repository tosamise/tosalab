<?php $LoggedUser = $this->request->session()->read('Auth.User'); ?>

<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <div class="pull-left image">
        <?php if($LoggedUser['avatar'])
          {
              echo $this->Html->image($LoggedUser['avatar'], ['alt' => 'user-avatar' , 'class' => 'img-circle']);
          }
          else 
          {
              echo $this->Html->image('avatars/default-avatar.png', ['alt' => 'user-avatar','class' => 'img-circle']); 
          }
          ?>
      </div>
      <div class="pull-left info">
        <p>
          <a href="/update-info/<?= $LoggedUser['id'] ?>">
            <?= $LoggedUser['nice_name'] ?>
          </a>
        </p>
        <a href="#"><i class="fa fa-clock-o text-success"></i>Member Since: 
          <?= ( $LoggedUser['created'] )? $LoggedUser['created']->format('Y-m-d') : "Unknown";?>
          </a>
      </div>
    </div>
    <a href="/update-info/<?php $LoggedUser['id'] ?>"></a>

    <div class="user-panel text-c" style="color:#FFF;">
      <?php echo $this->Html->link('<i class="fa fa-home mr10"></i>',  '/',  ['class' => '', 'target' => '','escape' => false]); ?>
      <?php echo $this->Html->link('<i class="fa fa-reply mr10"></i>',  '/logout',  ['class' => '', 'target' => '','escape' => false]); ?>

      <?php echo $this->Html->link('<i class="fa fa-user mr10"></i>',  '/user-list',  ['class' => '', 'target' => '','escape' => false]); ?>

    </div>
    <!--search form -->
    <form action="#" method="get" class="sidebar-form">
      <div class="input-group">
        <input type="text" name="q" class="form-control" placeholder="Search...">
        <span class="input-group-btn">
          <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
        </button>
        </span>
      </div>
    </form>

    <!-- /.search form -->
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu">

      <li class="treeview">
        <a href="/">
          <i class="fa fa-dashboard"></i> <span>Dashboard</span>
        </a>
      </li>

      <?php if( $this->MyHtml->check_role(['admin','manager']) ): ?>
      <li class="treeview">
        <a href="#">
          <i class="fa fa-user"></i> <span>Users Manager</span> <i class="fa fa-angle-left pull-right"></i>
          <ul class="treeview-menu">
            <li><a href="/register"><i class="fa fa-circle-o"></i> Register</a></li>
            <li><a href="/user-list"><i class="fa fa-circle-o"></i> User list</a></li>
          </ul>
        </a>
      </li>
      <?php endif; ?>
      
      <?php if( $this->MyHtml->check_role(['admin','photographer','manager']) ): ?>
      <li class="header">FILM CAMERA LAB</li>
      <li class="treeview">
        <a href="#">
          <i class="fa fa-camera-retro"></i> <span>Cameras</span> <i class="fa fa-angle-left pull-right"></i>
          <ul class="treeview-menu">
            <?php if( $this->MyHtml->check_role(['admin','manager']) ): ?>
            <li><a href="/new-camera"><i class="fa fa-circle-o"></i> Create new record</a></li>
            <?php endif; ?>
            <li><a href="/camera"><i class="fa fa-circle-o"></i> Camera list</a></li>
          </ul>
        </a>
      </li>

      <li class="treeview">
        <a href="#">
          <i class="fa fa-flask"></i> <span>Labs</span> <i class="fa fa-angle-left pull-right"></i>
          <ul class="treeview-menu">
            <?php if( $this->MyHtml->check_role(['admin','manager']) ): ?>
            <li><a href="/new-lab"><i class="fa fa-circle-o"></i> Add new lab</a></li>
            <?php endif; ?>
            <li><a href="/lab"><i class="fa fa-circle-o"></i> List of labs</a></li>
          </ul>
        </a>
      </li>

      <li class="treeview">
        <a href="#">
          <i class="fa fa-film"></i> <span>Films</span> <i class="fa fa-angle-left pull-right"></i>
          <ul class="treeview-menu">
            <?php if( $this->MyHtml->check_role(['admin','manager']) ): ?>
            <li><a href="/new-film"><i class="fa fa-circle-o"></i> Add new film</a></li>
            <?php endif; ?>
            <li><a href="/film"><i class="fa fa-circle-o"></i> List of films</a></li>
          </ul>
        </a>
      </li>

      <li class="treeview">
        <a href="#">
          <i class="fa fa-photo"></i> <span>Photos</span> <i class="fa fa-angle-left pull-right"></i>
          <ul class="treeview-menu">
            <li><a href="/gallery"><i class="fa fa-circle-o"></i> Gallery</a></li>
            <li><a href="/album"><i class="fa fa-circle-o"></i> Albums</a></li>
            <li><a href="/category"><i class="fa fa-circle-o"></i> Categories</a></li>
          </ul>
        </a>
      </li>
      <?php endif; ?>
      
      
      <li class="header">TOSA L'AMOUR</li>
        
      <li class="treeview">
        <a href="#">
          <?= $this->Html->image("svg-icon/hanger.svg", ['alt' => '' , 'class' => 'w20 pr5']); ?>
          <span>Wedding Clothes</span> <i class="fa fa-angle-left pull-right"></i>
          <ul class="treeview-menu">
            <li><a href="/products?cate=A">
              <?= $this->Html->image("svg-icon/dress.svg", ['alt' => '' , 'class' => 'w20 pr5']); ?> Dresses Collection</a></li>
            <li><a href="/products?cate=D">
              <?= $this->Html->image("svg-icon/coat.svg", ['alt' => '' , 'class' => 'w20 pr5']); ?> Áo dài Collection</a></li>
            <li><a href="/products?cate=V">
              <?= $this->Html->image("svg-icon/suit.svg", ['alt' => '' , 'class' => 'w20 pr5']); ?> Vest Collection</a></li>
            <li><a href="/products?cate=P">
              <?= $this->Html->image("svg-icon/king.svg", ['alt' => '' , 'class' => 'w20 pr5']); ?> Accessaries</a></li>
          </ul>
        </a>
      </li>
      
      <?php if( $this->MyHtml->check_role(['admin','manager']) ): ?>
      <li class="treeview">
        <a href="#">
          <?= $this->Html->image("svg-icon/resume.svg", ['alt' => '' , 'class' => 'w20 pr5']); ?>
          <span>Manage information</span> <i class="fa fa-angle-left pull-right"></i>
          <ul class="treeview-menu">
            <li><a href="/categories">
              <?= $this->Html->image("svg-icon/category.svg", ['alt' => '' , 'class' => 'w20 pr5']); ?> Categories</a></li>
            <li><a href="/suppliers">
              <?= $this->Html->image("svg-icon/hotel-supplier.svg", ['alt' => '' , 'class' => 'w20 pr5']); ?> Suppliers</a></li>
          </ul>
        </a>
      </li>
      
      <li class="treeview">
        <a href="#">
          <?= $this->Html->image("svg-icon/growth.svg", ['alt' => '' , 'class' => 'w20 pr5']); ?>
          <span>Manage products</span> <i class="fa fa-angle-left pull-right"></i>
          <ul class="treeview-menu">
            <li><a href="/add-product">
              <i class="fa fa-plus" aria-hidden="true"></i> Add new</a></li>
            <li><a href="/product-images">
              <i class="fa fa-photo"></i> Product images</a></li>
          </ul>
        </a>
      </li>
      <?php endif; ?>

      <?php if( $this->MyHtml->check_role(['admin']) ): ?>
      <li class="header">TAGS</li>
      <li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Important</span></a></li>
      <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Warning</span></a></li>
      <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Information</span></a></li>

      <li class="header">DEMO HTML</li>
      <li><a href="/AdminLTE/index.html" target="_blank"><i class="fa fa-circle-o text-red"></i> <span>INDEX 1</span></a></li>
      <li><a href="/AdminLTE/index2.html" target="_blank"><i class="fa fa-circle-o text-yellow"></i> <span>INDEX 2</span></a></li>
      <?php endif; ?>
    </ul>
  </section>
  <!-- /.sidebar -->
</aside>