<?php $LoggedUser = $this->request->session()->read('Auth.User'); ?>
<header class="main-header">
    <!-- Logo -->
    <a href="/" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>TS</b></span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>ToSa</b>lab</span>
    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <?php if($LoggedUser['avatar'])
                        {
                            echo $this->Html->image($LoggedUser['avatar'], ['alt' => 'user-avatar','class' => 'user-image']); 
                        }
                        else 
                        {
                            echo $this->Html->image('avatars/default-avatar.png', ['alt' => 'user-avatar','class' => 'user-image']); 
                        }
                        ?>
                        <span class="hidden-xs"><?= $LoggedUser['nice_name'] ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <?php if($LoggedUser['avatar'])
                            {
                                echo $this->Html->image($LoggedUser['avatar'], ['alt' => 'user-avatar','class' => 'img-circle']);
                            }
                            else 
                            {
                                echo $this->Html->image('avatars/default-avatar.png', ['alt' => 'user-avatar','class' => 'img-circle']); 
                            }
                            ?>
                            <p>
                                <?php if($LoggedUser['nice_name']): ?>
                                <?= $LoggedUser['nice_name'] ?> - <?= $roles[$LoggedUser['role']] ?>
                                <small>Last login <?= $LoggedUser['last_login']->format('Y-m-d'); ?></small>
                                <?php endif;?>
                            </p>
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <?=
                                    $this->Html->link(
                                        'Profile',
                                        '/update-info/'.$LoggedUser['id'],
                                        ['class' => 'btn btn-default btn-flat', 'target' => '','escape' => false]
                                    );
                                ?>
                            </div>
                            <div class="pull-right">
                                <?=
                                    $this->Html->link(
                                        'Log out',
                                        '/logout',
                                        ['class' => 'btn btn-default btn-flat', 'target' => '','escape' => false]
                                    );
                                ?>
                            </div>
                        </li>
                    </ul>
                </li>

            </ul>
        </div>
    </nav>
</header>