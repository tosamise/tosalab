<!-- src/Template/MCameras/add.ctp -->
<?= $this->Flash->render('flash') ?>

<div class="row">
    <!-- left column -->
    <div class="col-md-8">
        <!-- general form elements -->
        <div class="box box-warning">
            <div class="box-body">
                <div class="users form">
                <?= $this->Form->create($camera, ['enctype' => 'multipart/form-data','id'=>'add_camera_form']) ?>
                  
                    <div class="form-group">
                        <label>Camera name:</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-camera-retro"></i>
                            </div>
                            <?= $this->Form->input('cam_name', ['class'=>'form-control','id'=>'cam-name','placeholder'=>'Ex: Canon FT','div'=>false,'label'=>'','error'=>false] ); ?>
                        </div>
                        <?= $this->Form->error('cam_name', null, array('class' => 'error-message','div'=>false)); ?>
                    </div>
                    
                    <div class="form-group">
                        <?= $this->Form->input('cam_type', [
                            'options' => [
                                        'slr'=>'SLR - Single Lens Reflex camera',
                                        'rf'=>'Rangefinder',
                                        'med'=>'Medium Format'
                                        ],
                            'class'=>'form-control',
                            'error'=>false,
                            'label'=>'Camera type:'
                        ]) ?>
                        <?= $this->Form->error('cam_type', null, array('class' => 'error-message','div'=>false)); ?>
                    </div>
                    
                    <div class="form-group">
                        <label>Lens:</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-camera-retro"></i>
                            </div>
                            <?= $this->Form->input('lens', ['class'=>'form-control','id'=>'lens-name','placeholder'=>'Ex: 50mm f1.8','div'=>false,'label'=>'','error'=>false] ); ?>
                        </div>
                        <?= $this->Form->error('lens', null, array('class' => 'error-message','div'=>false)); ?>
                    </div>
                    
                    <div class="form-group">
                        <label>Bought Price:</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-money"></i>
                            </div>
                            <?= $this->Form->input('tmp_bought_price', ['class'=>'form-control','id'=>'bought_price','placeholder'=>'Ex: 3.000.000','div'=>false,'label'=>'','error'=>false,'autocomplete'=>'off'] ); ?>
                        </div>
                        <?= $this->Form->error('bought_price', null, array('class' => 'error-message','div'=>false)); ?>
                        <?= $this->Form->hidden('bought_price', ['id'=> 'hidden_bought_price'] );?>
                    </div>
                    
                    <div class="form-group">
                        <label>Series:</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-barcode"></i>
                            </div>
                            <?= $this->Form->input('cam_series', ['class'=>'form-control','id'=>'cam_series','placeholder'=>'Ex: Camera series','div'=>false,'label'=>'','error'=>false,'autocomplete'=>'off'] ); ?>
                        </div>
                        <?= $this->Form->error('cam_series', null, array('class' => 'error-message','div'=>false)); ?>
                    </div>
                    
                    <div class="form-group">
                        <?= $this->Form->input('belong_to', [
                                                    'options' => $owners,
                                                    'class'=>'form-control',
                                                    'error'=>false,
                                                    'label' => 'Owner:'
                                                ]) ?>
                        <?= $this->Form->error('belong_to', null, array('class' => 'error-message','div'=>false)); ?>
                    </div>
                    
                    <div class="form-group">
                        <label>Camera image:</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-photo"></i>
                            </div>
                            <?= $this->Form->file('cam_image_file', ['class'=>'form-control','id'=>'cam_image','div'=>false,'label'=>'','error'=>false] ); ?>
                        </div>
                        <?= $this->Form->error('cam_image', null, array('class' => 'error-message','div'=>false)); ?>
                    </div>
                    
                    <?= $this->Form->hidden('created', ['value'=> date('Y-m-d H:i:s')] );?>
                
                <?= $this->Form->button(__('<i class="fa fa-repeat"></i> Clear All'), ['type'=>'button','id'=>'clearAllbtn','class'=>'btn btn-app fr','escape'=>false]); ?>    
                <?= $this->Form->button(__('<i class="fa fa-save"></i> Save'), ['class'=>'btn btn-app fr','id'=>'add_camera_btn','escape'=>false]); ?>
                <?= $this->Form->end() ?>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <!--/.col (right) -->
    
</div>
<!-- /.row -->
<script type="text/javascript">
	$().ready(function() {
	    $("#add_camera_form").validate({
			rules: {
				username: "required",
				nice_name: "required",
				password: "required",
				role: "required",
				username: {
					required: true,
					minlength: 6
				},
				password: {
					required: true,
					minlength: 6
				},
			},
			messages: {
				username: "Please enter your username",
				nice_name: "Please enter your nice name",
				username: {
					required: "Please enter a username",
					minlength: "The username must consist of at least 6 characters"
				},
				password: {
					required: "Please enter a password",
					minlength: "The password must consist of at least 6 characters"
				},
			},
			highlight: function(element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-group').removeClass('has-error');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function(error, element) {
                if(element.parents('.input-group').length) {
                    error.insertAfter(element.parents(".input-group"));
                } else {
                    error.insertAfter(element);
                }
            }
	    }); 
	    
	});
	
	$('#add_camera_btn').on('click',function(e){
    e.preventDefault();
    var form = $('#add_camera_btn').parents('#add_camera_form');
        swal({
            title: "Are you sure?",
            text: "You will add new camera!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#008d4c",
            confirmButtonText: "Yes, do it!",
            closeOnConfirm: true
        }, function(isConfirm){
            if (isConfirm) form.submit();
        });
    });
</script>