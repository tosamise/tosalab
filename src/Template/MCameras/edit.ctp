<!-- src/Template/MCameras/edit.ctp -->
<?= $this->Flash->render('flash') ?>

    <div class="row">

        <!-- left column -->
        <div class="col-md-8">
            <!-- general form elements -->
            <div class="box box-warning">
                <div class="box-body">
                    <div class="users form">
                        <?= $this->Form->create($camera, ['enctype' => 'multipart/form-data']) ?>
                  
                        <div class="form-group">
                            <label>Camera name:</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-camera-retro"></i>
                                </div>
                                <?= $this->Form->input('cam_name', ['class'=>'form-control','id'=>'cam-name','placeholder'=>'Ex: Canon FT','div'=>false,'label'=>'','error'=>false] ); ?>
                            </div>
                            <?= $this->Form->error('cam_name', null, array('class' => 'error-message','div'=>false)); ?>
                        </div>
                        
                        <div class="form-group">
                            <?= $this->Form->input('cam_type', [
                                'options' => [
                                            'slr'=>'SLR - Single Lens Reflex camera',
                                            'rf'=>'Rangefinder',
                                            'med'=>'Medium Format'
                                            ],
                                'class'=>'form-control',
                                'error'=>false,
                                'label'=>'Camera type:'
                            ]) ?>
                            <?= $this->Form->error('cam_type', null, array('class' => 'error-message','div'=>false)); ?>
                        </div>
                        
                        <div class="form-group">
                            <label>Lens:</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-camera-retro"></i>
                                </div>
                                <?= $this->Form->input('lens', ['class'=>'form-control','id'=>'lens-name','placeholder'=>'Ex: 50mm f1.8','div'=>false,'label'=>'','error'=>false] ); ?>
                            </div>
                            <?= $this->Form->error('lens', null, array('class' => 'error-message','div'=>false)); ?>
                        </div>
                        
                        <div class="form-group">
                            <label>Bought Price:</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-money"></i>
                                </div>
                                <?= $this->Form->input('tmp_bought_price', ['class'=>'form-control','id'=>'bought_price','placeholder'=>'Ex: 3.000.000','div'=>false,'label'=>'','error'=>false,'autocomplete'=>'off'] ); ?>
                            </div>
                            <?= $this->Form->error('bought_price', null, array('class' => 'error-message','div'=>false)); ?>
                            <?= $this->Form->hidden('bought_price', ['id'=> 'hidden_bought_price'] );?>
                        </div>
                        
                        <div class="form-group">
                            <label>Series:</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-barcode"></i>
                                </div>
                                <?= $this->Form->input('cam_series', ['class'=>'form-control','id'=>'cam_series','placeholder'=>'Ex: Camera series','div'=>false,'label'=>'','error'=>false,'autocomplete'=>'off'] ); ?>
                            </div>
                            <?= $this->Form->error('cam_series', null, array('class' => 'error-message','div'=>false)); ?>
                        </div>
                        
                        <div class="form-group">
                            <?= $this->Form->input('belong_to', [
                                                        'options' => $owners,
                                                        'class'=>'form-control',
                                                        'error'=>false,
                                                        'label' => 'Owner:'
                                                    ]) ?>
                            <?= $this->Form->error('belong_to', null, array('class' => 'error-message','div'=>false)); ?>
                        </div>
                        
                        <div class="form-group">
                            <label>Camera image:</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-photo"></i>
                                </div>
                                <?= $this->Form->file('cam_image_file', ['class'=>'form-control','id'=>'cam_image','div'=>false,'label'=>'','error'=>false] ); ?>
                            </div>
                            <?= $this->Form->error('cam_image', null, array('class' => 'error-message','div'=>false)); ?>
                        </div>
                        
                        <?= $this->Form->hidden('modified', ['value'=> date('Y-m-d H:i:s')] );?>
                    
                    <?= $this->Html->link('<i class="fa fa-camera-retro"></i> List', '/camera',  ['class' => 'btn btn-app fr','id'=>'list_btn','target' => '','escape' => false]); ?>  
                    <?= $this->Form->button(__('<i class="fa fa-save"></i> Save'), ['class'=>'btn btn-app fr','escape'=>false]); ?>
                    <?= $this->Form->end() ?>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!--/.col (left) -->

        <!-- right column -->
        <div class="col-md-4">
            <!-- general form elements -->
            <div class="box box-warning">
                <div class="box-body text-c">
                    <?= $this->Html->image($camera->cam_image, ['class'=>'w100p']) ?>
                    <caption><i><?= $camera['cam_name']?> camera</i></caption>
                    <?php if( $camera['is_sold'] == 1 ): ?>
                        <br>
                        <span class='red-text'>This camera has been sold</span>
                    <?php endif ?>
                </div>
            </div>
            <!--/.box -->
            
            <?php if( $camera['is_sold'] != 1 ): ?>
                <?= $this->Form->create($camera ,['url'=>'/sold-camera','class'=>'text-c','onsubmit' => "return confirm('Are you sure to sold this camera?');"]); ?>
                <?= $this->Form->hidden('id');?>
                <?= $this->Form->hidden('modified', ['value'=> date('Y-m-d H:i:s')] );?>
                <?= $this->Form->button(__('<i class="fa fa-ban"></i> Mark camera as sold'), ['class'=>'btn btn-danger','escape'=>false]); ?>
                <?= $this->Form->end() ?>
            <?php endif ?>
            
        </div>
        <!--/.col (left) -->
    </div>
    <!-- /.row -->