<!-- src/Template/MCameras/view.ctp -->
<?= $this->Flash->render('flash') ?>

    <div class="row">
        
        <!-- left column -->
        <div class="col-md-4">
            <!-- general form elements -->
            <div class="box box-warning">
                <div class="box-body text-c">
                    <?= $this->Html->image($camera->cam_image, ['class'=>'w100p']) ?>
                    <caption><i><?= $camera['cam_name']?></i></caption>
                </div>
            </div>
            <!--/.box -->
        </div>
        <!--/.col (left) -->

        <!-- right column -->
        <div class="col-md-8">
            <!-- general form elements -->
            <div class="box box-warning">
                <div class="box-header with-border ml20">
                    <h3><?= $camera['cam_name'] ?></h3>
                </div>

                <div class="box-body fsize16">
                    <table class="ml20">
                        <tr>
                            <td class="w100"><b>Type:</b></td>
                            <td class="uc"><?= $camera['cam_type'] ?></td>
                        </tr>
                        <tr>
                            <td><b>Lens:</b></td>
                            <td><?= $camera['lens'] ?></td>
                        </tr>
                        <tr>
                            <td><b>Price:</b></td>
                            <?php setlocale(LC_MONETARY, 'en_US');?>
                            <td><?= number_format($camera['bought_price'], 0, ',', '.')."đ"; ?></td>
                        </tr>
                        <tr>
                            <td><b>Series:</b></td>
                            <td><?= $camera['cam_series'] ?></td>
                        </tr>
                        <tr>
                            <td><b>Owner:</b></td>
                            <td><?= $data[0]['nice_name'] ?></td>
                        </tr>
                        
                        <?php if( $camera['is_sold'] == 1 ): ?>
                            <tr>
                                <td><b>Status:</b></td>
                                <td><span class='red-text'>This camera has been sold</span></td>
                            </tr>
                        <?php endif;?>
                        
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
            <?= $this->Html->link('<i class="fa fa-film"></i> Back to list', '/camera',  ['class' => 'btn btn-app fr','id'=>'list_btn','target' => '','escape' => false]); ?>  
        </div>
        <!--/.col (right) -->
        
        <!-- below column -->
        <div class="col-md-12">
            <div class="box box-warning">
                <div class="box-header with-border">
                    <h4>Sample photos of this camera type</h4>
                </div>
                <div class="box-body fsize18">
                    <?= $this->Html->image('login-bg.jpg', ['class'=>'w300 border ml10 mr20']) ?>
                    <?= $this->Html->image($camera->cam_image, ['class'=>'w300 border mr20']) ?>
                    <?= $this->Html->image($camera->cam_image, ['class'=>'w300 border mr20']) ?>
                    <?= $this->Html->image('login-bg.jpg', ['class'=>'w300 border mr20']) ?>
                </div>
                <div class="box-footer">
                    <?= $this->Html->link('<i class="fa fa-hand-o-right"></i> See more...', '/photo',  ['class' => 'text-bold','id'=>'see_more','target' => '','escape' => false]); ?>  
                </div>
            </div>
            <!-- /.box --> 
        </div>
        <!--/.col (below) -->
        
    </div>
    <!-- /.row -->