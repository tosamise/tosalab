<!-- src/Template/MProimgs/add.ctp -->
<?= $this->Flash->render('flash') ?>
<?php $LoggedUser = $this->request->session()->read('Auth.User'); ?>
<button type="button" class="btn btn-default mt20 mb20" id="open_dropzone">Open upload</button>
<div class="row">
    <div class="col-md-12 display-n" id="wrap-dropzone">
        <!-- general form elements -->
        <div class="box box-warning">
            <div class="box-header with-border">
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="users form">
                <?= $this->Form->create(null, ['enctype' => 'multipart/form-data', 'class'=>'dropzone drop-style','id'=>'my-awesome-dropzone']) ?>
                
                   <?php $this->Form->file('file_name', ['class'=>'form-control','id'=>'file_name','multiple'=>true,'div'=>false,'label'=>'','error'=>false] ); ?>
                    
                    <?= $this->Form->hidden('created', ['value'=> date('Y-m-d H:i:s')] );?>
                
                <?php //$this->Form->button(__('<i class="fa fa-repeat"></i> Clear All'), ['type'=>'button','id'=>'upload-image','class'=>'btn btn-app fr','escape'=>false]); ?>    
                <?= $this->Form->end() ?>
                </div>
                <?= $this->Form->button(__('<i class="fa fa-upload"></i> Upload'), ['id'=>'upload-image','class'=>'btn btn-app fr mt10','escape'=>false]); ?>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    
    <div class="col-md-12 ">
        <div class="box box-warning">
            <div class="box-header with-border">
                <h3 class="box-title">All product images</h3>
        
                <div class="box-tools pull-right">
                    <span class="label label-danger">
                        <?php echo $this->Paginator->counter(['format' => 'Page {{page}} of {{pages}}' ]); ?>
                    </span>
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
            </button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body" id="selectable">
                
                <?php foreach($photos as $photo): ?>
                <div class="text-c col-md-3 minh120 minw120 maxw120 maxh120 ui-widget-content">
                    <?php echo $this->Html->image("/../".$imglinks['pro_images'].$photo->img_name, ['class'=>'pro_img w100p','id'=>$photo['id'] ]) ?>
                    <caption><i><?= $photo['img_name']?></i></caption>
                </div>
                <?php endforeach; ?>
                
            </div>
            <!-- /.box-body -->
            <div class="box-footer text-center">
                <ul class="pagination pagination-sm no-margin pull-right">
                    <?php 
                        // Shows the next links
                        echo $this->Paginator->prev('« Previous');
                        //Shows the page numbers
                        echo $this->Paginator->numbers();
                        // Shows the previous links
                        echo $this->Paginator->next('Next »');
                    ?>
                </ul>
                
                <button class="btn btn-default fl" type="button">
                    <?php echo $this->Paginator->counter(['format' => '{{current}} of {{count}}']);?>
                </button>
                
            </div>
            <!-- /.box-footer -->
        </div>
        <!-- .box -->
        
        <?= $this->Form->create('search', ['url'=>'/product-images','class'=>'sidebar-form m0 custom-search-bar mb30']) ?>
          <div class="input-group">
            <?php echo $this->Form->input('Search', ['class'=>'form-control','placeholder'=>'Search image...','name'=>'search','div'=>false,'label'=>false]);?>
            <span class="input-group-btn">
              <?= $this->Form->button('<i class="fa fa-search"></i>',['class'=>'btn btn-flat','escape'=>false,'name'=>'searchbtn','id'=>'search-btn','div'=>false]) ?>
            </span>
          </div>
        <?= $this->Form->end() ?>
    </div>
    <!-- .col -->
    
    <div class="col-md-4">
        <!-- general form elements -->
        <div class="box box-warning">
            <div class="box-body">
                <div class="users form">
                <?= $this->Form->create(null, ['enctype' => 'multipart/form-data','id'=>'add_img_to_pro']) ?>

                    <div class="form-group">
                        <label>You've selected:</label> <span id="select-result">none</span>
                    </div>

                    <div class="form-group">
                        <?= $this->Form->input('pro_id', [
                                                    'empty' => ['0'=>'Select product'],
                                                    'options' => $procodes,
                                                    'class'=>'form-control',
                                                    'id' => 'pro_id',
                                                    'error'=>false,
                                                    'label' => 'Product code:'
                                                ]) ?>
                        <?= $this->Form->error('pro_id', null, array('class' => 'error-message','div'=>false)); ?>
                    </div>
                    
                    <?= $this->Form->hidden('img_id', ['value'=> '','id'=>'img_id'] );?>
                    <?= $this->Form->hidden('created', ['value'=> date('Y-m-d H:i:s')] );?>
                    
                <?= $this->Form->end() ?>
                <?= $this->Form->button(__('<i class="fa fa-save"></i> Add to product'), ['class'=>'btn btn-app fr','id'=>'add_img','escape'=>false]); ?>
                
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <!--/.col (left) -->
    
    <div class="col-md-8">
        <!-- general form elements -->
        <div class="box box-warning">
            <div class="box-body" id="img_preview">
                
            </div>
        </div>
    </div>
     <!--/.col (right) -->
    
</div>
<!-- /.row -->
<?php echo $this->Html->script('customDropzone.js'); ?>
<script type="text/javascript">
    $(document).ready(function()
    {
        $(function() {
            $("#selectable").bind("mousedown", function(e) {
                e.metaKey = true;
            }).selectable({
                stop: function() {
                    var result = $( "#select-result" ).empty();
                    $("#img_id").val("");
                    var selectedID = [];
                    $( ".ui-selected .pro_img", this ).each(function() {
                        result.append( $(this).attr("id")+"," );
                        selectedID.push( $(this).attr("id") );
                        $("#img_id").val( selectedID );
                    });
                }
            });
        });
        
    	$('#upload-image').click(function(){
    	     swal({
                title: "Are you sure?",
                text: "Your photos will be uploaded to server",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#F39814",
                confirmButtonText: "Yes,do it!",
                closeOnConfirm: true
            }, function() {
                myDropzone.processQueue();
            });
    	});
    	
    	$('#add_img').click(function(){
    	    if( $( "#img_id" ).val() != "" && $( "#pro_id" ).val() != 0)
            {
        	    swal({
                    title: "Are you sure?",
                    text: "The image will be add to product id",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#F39814",
                    confirmButtonText: "Yes,do it!",
                    closeOnConfirm: true
                }, function() {
                    $("#add_img_to_pro").submit();
                });
            }
            else
            {
                swal("Error", "Please choose images and product", "error");
                $( "#pro_id" ).val(0);
            }
    	});
    	
    	$('#img_preview').magnificPopup({
        	delegate: 'a',
        	type: 'image',
        	tLoading: 'Loading image #%curr%...',
        	mainClass: 'mfp-img-mobile',
        	gallery: {
        		enabled: true,
        		navigateByImgClick: true,
        		preload: [0,1] // Will preload 0 - before current, and 1 after the current image
        	},
        	image: {
        		tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
        		titleSrc: function(item) {
        			return item.el.attr('title') + '<small>by ToSa L\'amour</small>';
        		}
        	}
        });
    	
    });//end ready

    $( "#pro_id" ).on('change', function(event) {
        var id = $(this).val();
        if( id != 0 )
        {
            $('#divLoading').addClass('show');
            $.ajax({
            type: "POST",
            url: "/ajax-proimg",
            async: false,
            data: { 'pro_id': id },
            	success: function(data){
            		if(data != "")
            		{
            			//Parse data got from server as JSON
            			var obj = jQuery.parseJSON(data);
            			var data = obj['data'];
            			var html_img = "";
                  var imglinks = "/../<?= ($imglinks['pro_images']?$imglinks['pro_images']:"") ?>";
                        if( data != "")
                        {
                            for( var i = 0; i < data.length; i++ )
                            {
                                html_img += "<a href='"+imglinks+data[i]['img_name']+"' title='"+data[i]['img_name']+"'> \
                                            <img src='"+imglinks+data[i]['img_name']+"' alt='"+data[i]['img_name']+"' class='img_preview w130 mr5'> \
                                            </a>";
                            }
                            
                        }else{
            			    html_img += "<img src='/img/No-image-found.jpg' class='img_preview w130 mr5'> \
            			                 <img src='/img/No-image-found.jpg' class='img_preview w130 mr5'> \
            			                 <img src='/img/No-image-found.jpg' class='img_preview w130 mr5'> \
            			                 <img src='/img/No-image-found.jpg' class='img_preview w130 mr5'> \
            			                 <img src='/img/No-image-found.jpg' class='img_preview w130 mr5'> \
            			                ";
                        }
                        
                        $("#img_preview").html(html_img);
                        $('#divLoading').removeClass('show');
            		}//end if
            	},
            	error: function(){
                    swal("Opps!", "Something wrong, please refresh page", "error");
            	}
            });//end ajax
        }
    });
    
    $('#open_dropzone').click(function(){
        var $t = $('#wrap-dropzone');

        if ($t.is(':visible')) {
            $t.slideUp();
            $(this).text("Open upload");
        } else {
            $t.slideDown();
            $(this).text("Close upload");
        }
    });
    
    
</script>

