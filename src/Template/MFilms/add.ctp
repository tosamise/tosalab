<!-- src/Template/MFilms/add.ctp -->
<?= $this->Flash->render('flash') ?>

<div class="row">
    <!-- left column -->
    <div class="col-md-8">
        <!-- general form elements -->
        <div class="box box-warning">
            <div class="box-body">
                <div class="users form">
                <?= $this->Form->create($film, ['enctype' => 'multipart/form-data', 'id' => 'add_film_form']) ?>
                    
                    <div class="form-group">
                        <label>Model:</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-truck"></i>
                            </div>
                            <?= $this->Form->input('model', ['class'=>'form-control','id'=>'model','placeholder'=>'Ex: Fujifilm, Agfa...','div'=>false,'label'=>'','error'=>false] ); ?>
                        </div>
                        <?= $this->Form->error('model', null, array('class' => 'error-message','div'=>false)); ?>
                    </div>
                    
                    <div class="form-group">
                        <label>Film name:</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-film"></i>
                            </div>
                            <?= $this->Form->input('film_name', ['class'=>'form-control','id'=>'film_name','placeholder'=>'Ex: Fuji200','div'=>false,'label'=>'','error'=>false] ); ?>
                        </div>
                        <?= $this->Form->error('film_name', null, array('class' => 'error-message','div'=>false)); ?>
                    </div>
                    
                    <div class="form-group">
                        <label>Film type:</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-film"></i>
                            </div>
                            <?= $this->Form->input('film_type', ['class'=>'form-control','id'=>'film_type','placeholder'=>'Ex: Color 35mm','div'=>false,'label'=>'','error'=>false] ); ?>
                        </div>
                        <?= $this->Form->error('film_type', null, array('class' => 'error-message','div'=>false)); ?>
                    </div>
                    
                    <div class="form-group">
                        <?= $this->Form->input('exp', [
                            'options' => [
                                        '12'=>'12',
                                        '24'=>'24',
                                        '36'=>'36',
                                        ],
                            'class'=>'form-control',
                            'error'=>false,
                            'label'=>'Exp:'
                        ]) ?>
                        <?= $this->Form->error('iso', null, array('class' => 'error-message','div'=>false)); ?>
                    </div>
                    
                    <div class="form-group">
                        <?= $this->Form->input('iso', [
                            'options' => [
                                        '100'=>'100',
                                        '200'=>'200',
                                        '400'=>'400',
                                        '600'=>'600',
                                        '800'=>'800',
                                        '1200'=>'1200',
                                        '1600'=>'1600',
                                        ],
                            'class'=>'form-control',
                            'error'=>false,
                            'label'=>'ISO:'
                        ]) ?>
                        <?= $this->Form->error('iso', null, array('class' => 'error-message','div'=>false)); ?>
                    </div>
                    
                    <div class="form-group">
                        <label>Film image:</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-photo"></i>
                            </div>
                            <?= $this->Form->file('film_image_file', ['class'=>'form-control','id'=>'film_image','div'=>false,'label'=>'','error'=>false] ); ?>
                        </div>
                        <?= $this->Form->error('film_image', null, array('class' => 'error-message','div'=>false)); ?>
                    </div>
                    
                    <?= $this->Form->hidden('created', ['value'=> date('Y-m-d H:i:s')] );?>
                
                <?= $this->Form->button(__('<i class="fa fa-repeat"></i> Clear All'), ['type'=>'button','id'=>'clearAllbtn','class'=>'btn btn-app fr','escape'=>false]); ?>    
                <?= $this->Form->button(__('<i class="fa fa-save"></i> Save'), ['class'=>'btn btn-app fr','id'=>'add_film_btn','escape'=>false]); ?>
                <?= $this->Form->end() ?>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <!--/.col (left) -->
    
</div>
<!-- /.row -->
<script type="text/javascript">
    $().ready(function() {
        
        //Get the Model list for suggestion
        $.ajax({
		type: "POST",
		url: "/ajax-film",
		async: false,
		data: { 'model': 'model' },
    		success: function(data){
    			if(data != "")
    			{
    				//Parse data got from server as JSON
    				obj = jQuery.parseJSON(data);
                    //Convert data from obj to array
    				var ACdata = [];
    				for( var i = 0; i < obj['data'].length; i++ )
    				{
    				    ACdata.push(obj['data'][i]['model']);
    				}
    				
    				$('#model').autocomplete({
                        source: ACdata
                    });
    			}//end if
    		},
        	error: function(){
                swal("Opps!", "Something wrong, please refresh page", "error");
        	}
       });//end ajax
       
       //Get the film type list for suggestion
        $.ajax({
		type: "POST",
		url: "/ajax-film",
		async: false,
		data: { 'type': 'type' },
    		success: function(data){
    			if(data != "")
    			{
    				//Parse data got from server as JSON
    				obj = jQuery.parseJSON(data);
                    //Convert data from obj to array
    				var ACdata = [];
    				for( var i = 0; i < obj['data'].length; i++ )
    				{
    				    ACdata.push(obj['data'][i]['film_type']);
    				}
    				
    				$('#film_type').autocomplete({
                        source: ACdata
                    });
    			}//end if
    		},
        	error: function(){
                swal("Opps!", "Something wrong, please refresh page", "error");
        	}
        });//end ajax
        
        //Get list of film name for check existances.
        var film_names = [];
        $.ajax({
            type: "POST",
            url: "/ajax-film",
            async: false, 
            data: {film_name : "filmname"},
            success: function(data)
            {
                if(data != "")
                {
                    //Parse data got from server as JSON
        			obj = jQuery.parseJSON(data);
                    film_names = obj['data'];
                }
            }
        })
        
        $.validator.addMethod("uniqueFilmName", function(value) {
            for( var i = 0; i <= film_names.length; i++ )
            {
                str = lcrsp(film_names[i]['film_name'])
	            str_1 = lcrsp(value);
	            
	            if( str === str_1 ) 
                    return false;
                else
                    return true;   
	            
            }
        }, "This film name has already existed");
        
        function lcrsp(str)
        {
            str = str.toLowerCase();
            str = str.replace(/\s+/g, '');
            return str;
        }
        
        $("#add_film_form").validate({
            onkeyup: false,
    		rules: {
    			model: "required",
    			film_name: {
    			    required: true,
    			    uniqueFilmName: true
    			},
    			film_type: "required"
    		},    
    		messages: {
    			model: "Please enter film model",
    			film_name: {
    			    required: "Please enter film name",
    			    uniqueFilmName : "This film name has already existed"
    			},
    			film_type: "Please enter film type",
    		},
    		highlight: function(element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-group').removeClass('has-error');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function(error, element) {
                if(element.parents('.input-group').length) {
                    error.insertAfter(element.parents(".input-group"));
                } else {
                    error.insertAfter(element);
                }
            }
        });
	});
	
	$('#add_film_btn').on('click',function(e){
    e.preventDefault();
    var form = $('#add_film_btn').parents('#add_film_form');
        swal({
            title: "Are you sure?",
            text: "You will add new film record!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#008d4c",
            confirmButtonText: "Yes, do it!",
            closeOnConfirm: true
        }, function(isConfirm){
            if (isConfirm) form.submit();
        });
    });
</script>