<!-- src/Template/MFilms/edit.ctp -->
<?= $this->Flash->render('flash') ?>

    <div class="row">

        <!-- left column -->
        <div class="col-md-8">
            <!-- general form elements -->
            <div class="box box-warning">
                <div class="box-body">
                    <div class="users form">
                        <?= $this->Form->create($film, ['enctype' => 'multipart/form-data','id' => 'edit_film_form']) ?>
                  
                        <div class="form-group">
                        <label>Model:</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-truck"></i>
                            </div>
                            <?= $this->Form->input('model', ['class'=>'form-control','id'=>'model','placeholder'=>'Ex: Fujifilm, Agfa...','div'=>false,'label'=>'','error'=>false] ); ?>
                        </div>
                        <?= $this->Form->error('model', null, array('class' => 'error-message','div'=>false)); ?>
                        </div>
                  
                        <div class="form-group">
                        <label>Film name:</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-film"></i>
                            </div>
                            <?= $this->Form->input('film_name', ['class'=>'form-control','id'=>'film_name','placeholder'=>'Ex: Fuji200','div'=>false,'label'=>'','error'=>false] ); ?>
                        </div>
                        <?= $this->Form->error('film_name', null, array('class' => 'error-message','div'=>false)); ?>
                        </div>
                        
                        <div class="form-group">
                            <label>Film type:</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-film"></i>
                                </div>
                                <?= $this->Form->input('film_type', ['class'=>'form-control','id'=>'film_type','placeholder'=>'Ex: Color 35mm','div'=>false,'label'=>'','error'=>false] ); ?>
                            </div>
                            <?= $this->Form->error('film_type', null, array('class' => 'error-message','div'=>false)); ?>
                        </div>
                        
                        <div class="form-group">
                            <?= $this->Form->input('exp', [
                                'options' => [
                                            '12'=>'12',
                                            '24'=>'24',
                                            '36'=>'36',
                                            ],
                                'class'=>'form-control',
                                'error'=>false,
                                'label'=>'Exp:'
                            ]) ?>
                            <?= $this->Form->error('iso', null, array('class' => 'error-message','div'=>false)); ?>
                        </div>
                        
                        <div class="form-group">
                            <?= $this->Form->input('iso', [
                                'options' => [
                                            '100'=>'100',
                                            '200'=>'200',
                                            '400'=>'400',
                                            '600'=>'600',
                                            '800'=>'800',
                                            '1200'=>'1200',
                                            ],
                                'class'=>'form-control',
                                'error'=>false,
                                'label'=>'ISO:'
                            ]) ?>
                            <?= $this->Form->error('iso', null, array('class' => 'error-message','div'=>false)); ?>
                        </div>
                        
                        <div class="form-group">
                            <?= $this->Form->input('made_in', [
                                'options' => [
                                            'VN'=>'Vietnam',
                                            'JP'=>'Japan',
                                            'CN'=>'China',
                                            'DE'=>'Germany',
                                            'US'=>'USA',
                                            ],
                                'class'=>'form-control',
                                'error'=>false,
                                'label'=>'Made in:'
                            ]) ?>
                            <?= $this->Form->error('made_in', null, array('class' => 'error-message','div'=>false)); ?>
                        </div>
                        
                        <div class="form-group">
                            <label>Film image:</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-photo"></i>
                                </div>
                                <?= $this->Form->file('film_image_file', ['class'=>'form-control','id'=>'film_image','div'=>false,'label'=>'','error'=>false] ); ?>
                            </div>
                            <?= $this->Form->error('film_image', null, array('class' => 'error-message','div'=>false)); ?>
                            
                            <div class="input-group">
                                <img src="/upload/1600.900.jpg" class="" id="preview" />
                            </div>
                        </div>
                        
                        <?= $this->Form->hidden('modified', ['value'=> date('Y-m-d H:i:s')] );?>
                    
                    <?= $this->Html->link('<i class="fa fa-film"></i> List', '/film',  ['class' => 'btn btn-app fr','id'=>'list_btn','target' => '','escape' => false]); ?>  
                    <?= $this->Form->button(__('<i class="fa fa-save"></i> Save'), ['class'=>'btn btn-app fr','id'=>'edit_film_btn','escape'=>false]); ?>
                    <?= $this->Form->end() ?>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!--/.col (left) -->

        <!-- right column -->
        <div class="col-md-4">
            <!-- general form elements -->
            <div class="box box-warning">
                <div class="box-body text-c">
                    <?= $this->Html->image($film->film_image, ['class'=>'w100p']) ?>
                    <caption><i><?= $film['film_name']?></i></caption>
                    <?php if( $film['is_unproductive'] == 1 ): ?>
                        <br>
                        <span class='red-text'>This film have been discontinued</span>
                    <?php endif ?>
                </div>
            </div>
            <!--/.box -->
            
            <?php if( $film['is_unproductive'] != 1 ): ?>
                <?= $this->Form->create($film ,['url'=>'/deactivate-film','class'=>'text-c']); ?>
                <?= $this->Form->hidden('id');?>
                <?= $this->Form->hidden('modified', ['value'=> date('Y-m-d H:i:s')] );?>
                <?= $this->Form->button(__('<i class="fa fa-ban"></i> Discontinue Film'), ['class'=>'btn btn-danger','escape'=>false]); ?>
                <?= $this->Form->end() ?>
            <?php else: ?>
                <?= $this->Form->create($film ,['url'=>'/activate-film','class'=>'text-c']); ?>
                <?= $this->Form->hidden('id');?>
                <?= $this->Form->hidden('modified', ['value'=> date('Y-m-d H:i:s')] );?>
                <?= $this->Form->button(__('<i class="fa fa-check"></i> Reactivate Film'), ['class'=>'btn btn-success','escape'=>false]); ?>
                <?= $this->Form->end() ?>
            <?php endif ?>
            
        </div>
        <!--/.col (left) -->
    </div>
    <!-- /.row -->

<script type="text/javascript">
    $().ready(function() {
        
        
      
        //Get the Model list for suggestion
        $.ajax({
		type: "POST",
		url: "/ajax-film",
		async: false,
		data: { 'model': 'model' },
    		success: function(data){
    			if(data != "")
    			{
    				//Parse data got from server as JSON
    				obj = jQuery.parseJSON(data);
                    //Convert data from obj to array
    				var ACdata = [];
    				for( var i = 0; i < obj['data'].length; i++ )
    				{
    				    ACdata.push(obj['data'][i]['model']);
    				}
    				
    				$('#model').autocomplete({
                        source: ACdata
                    });
    			}//end if
    		},
        	error: function(){
                swal("Opps!", "Something wrong, please refresh page", "error");
        	}
       });//end ajax
       
       //Get the film type list for suggestion
        $.ajax({
		type: "POST",
		url: "/ajax-film",
		async: false,
		data: { 'type': 'type' },
    		success: function(data){
    			if(data != "")
    			{
    				//Parse data got from server as JSON
    				obj = jQuery.parseJSON(data);
                    //Convert data from obj to array
    				var ACdata = [];
    				for( var i = 0; i < obj['data'].length; i++ )
    				{
    				    ACdata.push(obj['data'][i]['film_type']);
    				}
    				
    				$('#film_type').autocomplete({
                        source: ACdata
                    });
    			}//end if
    		},
        	error: function(){
                swal("Opps!", "Something wrong, please refresh page", "error");
        	}
        });//end ajax
        
        //Get list of film name for check existances.
        var film_names = [];
        $.ajax({
            type: "POST",
            url: "/ajax-film",
            async: false, 
            data: {film_name : "filmname"},
            success: function(data)
            {
                if(data != "")
                {
                    //Parse data got from server as JSON
        			obj = jQuery.parseJSON(data);
                    film_names = obj['data'];
                }
            }
        })
        
        $.validator.addMethod('filesize', function(value, element, param) {
            return this.optional(element) || (element.files[0].size <= param) 
        });
        
        
        $("#edit_film_form").validate({
            onkeyup: false,
    		rules: {
    			model: "required",
    			film_name: {
    			    required: true,
    			},
    			film_type: "required",
    			film_image_file: { 
    			    required: false, 
    			    extension: "png|jpe?g|gif", 
    			    filesize: 300*1024,
    			   
    			}
    		},    
    		messages: {
    			model: "Please enter film model",
    			film_name: {
    			    required: "Please enter film name",
    			},
    			film_type: "Please enter film type",
    			film_image_file: {
    			    extension: "Please choose the image file", 
    			    filesize: "Image file size is too big",
    			    
    			}
    		},
    		highlight: function(element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-group').removeClass('has-error');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function(error, element) {
                if(element.parents('.input-group').length) {
                    error.insertAfter(element.parents(".input-group"));
                } else {
                    error.insertAfter(element);
                }
            }
        });
        
	});//ready function
	
	$('#edit_film_btn').on('click',function(e){
    e.preventDefault();
    var form = $('#edit_film_btn').parents('#edit_film_form');
        swal({
            title: "Are you sure?",
            text: "You will edit film record!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#008d4c",
            confirmButtonText: "Yes, do it!",
            closeOnConfirm: true
        }, function(isConfirm){
            if (isConfirm) form.submit();
        });
    });
    
    function lcrsp(str)
    {
        str = str.toLowerCase();
        str = str.replace(/\s+/g, '');
        return str;
    }
    
    //Make image preview when choose input file
    $('#film_image').on('change',function(e)
    {
        var input = document.getElementById("film_image");

        if (input.files && input.files[0]) 
        {
            if(input.files[0].size < 300*1024)
            {
                var reader = new FileReader();
                reader.onload = function (e) 
                {
                    console.log(e.target);
                    $('#preview').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
            else
            {
                alert("The image too big");
                $('#preview').attr('src', "");
            }
        }
    });
    

</script>