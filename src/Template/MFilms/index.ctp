<!-- All Films -->
<?php $LoggedUser = $this->request->session()->read('Auth.User'); ?>
<div class="row">
    <div class="col-md-12 ">
        <div class="box box-warning">
            <div class="box-header with-border">
                <h3 class="box-title">All films info</h3>
        
                <div class="box-tools pull-right">
                    <span class="label label-danger">
                        <?php echo $this->Paginator->counter(['format' => 'Page {{page}} of {{pages}}' ]); ?>
                    </span>
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
            </button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                
                <?php foreach($films as $film): ?>
                <div class="col-md-3 minh150 minw150">
                    <div class="box">
                        <div class="box-body text-c">
                            <?php $img = $this->Html->image($film->film_image, ['class'=>'w100p']) ?>
                            <?php 
                                if( isset($LoggedUser) )
                                {
                                    if( $LoggedUser['role'] === '1' ){
                                        echo $this->Html->link( $img ,'edit-film/'.$film->id, ['class' => '', 'target' => '','escape' => false]); 
                                    }
                                    else {
                                        echo $this->Html->link( $img ,'view-film/'.$film->id, ['class' => '', 'target' => '','escape' => false]); 
                                    }  
                                }
                            ?>
                            <caption><i><?= $film['film_name']?></i></caption>
                            <?php 
                            //Be appeared when films are not continued to product.
                            if($film['is_unproductive'] == 1)
                            {
                                echo "<br/>";
                                echo "<span class='red-text'>This film have been discontinued</span>";   
                            }
                            ?>
                            <div class="cover-box"></div>
                        </div>
                    </div>
                </div>
                <?php endforeach; ?>
                
            </div>
            <!-- /.box-body -->
            <div class="box-footer text-center">
                <ul class="pagination pagination-sm no-margin pull-right">
                    <?php 
                        // Shows the next links
                        echo $this->Paginator->prev('« Previous');
                        //Shows the page numbers
                        echo $this->Paginator->numbers();
                        // Shows the previous links
                        echo $this->Paginator->next('Next »');
                    ?>
                </ul>
                
                <button class="btn btn-default fl" type="button">
                    <?php echo $this->Paginator->counter(['format' => '{{current}} of {{count}}']);?>
                </button>
                
            </div>
            <!-- /.box-footer -->
        </div>
        <!-- .box -->
        
        <?= $this->Form->create('search', ['url'=>'/film','class'=>'sidebar-form m0 custom-search-bar']) ?>
          <div class="input-group">
            <?php echo $this->Form->input('Search', ['class'=>'form-control','placeholder'=>'Search films...','name'=>'search','div'=>false,'label'=>false]);?>
            <span class="input-group-btn">
              <?= $this->Form->button('<i class="fa fa-search"></i>',['class'=>'btn btn-flat','escape'=>false,'name'=>'searchbtn','id'=>'search-btn','div'=>false]) ?>
            </span>
          </div>
        <?= $this->Form->end() ?>
        
        
    </div>
    <!-- .col -->
</div>    