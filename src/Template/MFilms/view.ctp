<!-- src/Template/MFilms/view.ctp -->
<?= $this->Flash->render('flash') ?>

    <div class="row">
        
        <!-- left column -->
        <div class="col-md-4">
            <!-- general form elements -->
            <div class="box box-warning">
                <div class="box-body text-c">
                    <?= $this->Html->image($film->film_image, ['class'=>'w100p']) ?>
                    <caption><i><?= $film['film_name']?></i></caption>
                </div>
            </div>
            <!--/.box -->
        </div>
        <!--/.col (left) -->

        <!-- right column -->
        <div class="col-md-8">
            <!-- general form elements -->
            <div class="box box-warning">
                <div class="box-header with-border ml20">
                    <h3><?= $film['film_name'] ?></h3>
                </div>

                <div class="box-body fsize16">
                    <table class="ml20">
                        <tr>
                            <td class="w100"><b>Model:</b></td>
                            <td><?= $film['model'] ?></td>
                        </tr>
                        <tr>
                            <td><b>Type:</b></td>
                            <td><?= $film['film_type'] ?></td>
                        </tr>
                        <tr>
                            <td><b>Size:</b></td>
                            <td><?= $film['exp'] ?></td>
                        </tr>
                        <tr>
                            <td><b>ISO:</b></td>
                            <td><?= $film['iso'] ?></td>
                        </tr>
                        <?php 
                            $made_in = "";
                            switch ($film['made_in']) {
                            case 'VN':
                                $made_in = "Vietnam";
                                break;
                            case 'JP':
                                $made_in = "Japan";
                                break;
                            case 'CN':
                                $made_in = "China";
                                break;
                            case 'DE':
                                $made_in = "Germany";
                                break;
                            case 'US':
                                $made_in = "USA";
                                break;
                            default:
                                $made_in = "Unknown";
                                break;
                        } ?>
                        <tr>
                            <td><b>Made in:</b></td>
                            <td><?= $made_in ?></td>
                        </tr>
                        
                        <?php if( $film['is_unproductive'] == 1 ): ?>
                        <tr>
                            <td><b>Status:</b></td>
                            <td>
                                <span class='red-text'>This film have been discontinued</span>
                            </td>
                        </tr>
                        <?php endif; ?>
                        
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
            <?= $this->Html->link('<i class="fa fa-film"></i> Back to list', '/film',  ['class' => 'btn btn-app fr','id'=>'list_btn','target' => '','escape' => false]); ?>  
        </div>
        <!--/.col (right) -->
        
        <!-- below column -->
        <div class="col-md-12">
            <div class="box box-warning">
                <div class="box-header with-border">
                    <h4>Sample photos of this film type</h4>
                </div>
                <div class="box-body fsize18">
                    <?= $this->Html->image('login-bg.jpg', ['class'=>'w300 border ml10 mr20']) ?>
                    <?= $this->Html->image($film->film_image, ['class'=>'w300 border mr20']) ?>
                    <?= $this->Html->image($film->film_image, ['class'=>'w300 border mr20']) ?>
                    <?= $this->Html->image('login-bg.jpg', ['class'=>'w300 border mr20']) ?>
                </div>
                <div class="box-footer">
                    <?= $this->Html->link('<i class="fa fa-hand-o-right"></i> See more...', '/film',  ['class' => 'text-bold','id'=>'see_more','target' => '','escape' => false]); ?>  
                </div>
            </div>
            <!-- /.box --> 
        </div>
        <!--/.col (below) -->
        
    </div>
    <!-- /.row -->