<!-- src/Template/MUsers/edit.ctp -->

<?= $this->Flash->render('flash') ?>
	<?php $LoggedUser = $this->request->session()->read('Auth.User');?>
	<div class="row">
		<!-- left column -->
		<div class="col-md-4">
			<div class="box box-widget widget-user">
				<!-- Add the bg color to the header using any of the bg-* classes -->
				<div class="widget-user-header bg-yellow-active">
					<h3 class="widget-user-username"><?= $user['nice_name'] ?></h3>
					<h5 class="widget-user-desc"><?= $roles[$user['role']] ?></h5>
				</div>
				<div class="widget-user-image">
					<?= $this->Html->image($user->avatar, ['class'=>'img-circle']) ?>
				</div>
				<div class="box-footer">
					<div class="row">
						<div class="col-sm-4 border-right">
							<div class="description-block">
								<h5 class="description-header">10</h5>
								<span class="description-text">Roll(s)</span>
							</div>
							<!-- /.description-block -->
						</div>
						<!-- /.col -->
						<div class="col-sm-4 border-right">
							<div class="description-block">
								<h5 class="description-header">10</h5>
								<span class="description-text">Camera(s)</span>
							</div>
							<!-- /.description-block -->
						</div>
						<!-- /.col -->
						<div class="col-sm-4">
							<div class="description-block">
								<h5 class="description-header">350</h5>
								<span class="description-text">Photo(s)</span>
							</div>
							<!-- /.description-block -->
						</div>
						<!-- /.col -->
					</div>
					<!-- /.row -->
				</div>
			</div>
			<?php if( $user['is_delete'] != 1 ): ?>
			<?= $this->Form->create($user,['url'=>'/ban-user','class'=>'text-c','id'=>'ban_form']); ?>
				<?= $this->Form->hidden('id', ['id'=> 'user_id'] );?>
					<?= $this->Form->button(__('<i class="fa fa-ban"></i> Ban this user'), ['class'=>'btn btn-danger','id'=>'ban_btn','escape'=>false]); ?>
						<?= $this->Form->end() ?>
							<?php else: ?>
							<div class="btn btn-danger w100p mb10">
								<span>This user was banned</span>
							</div>
							<?= $this->Form->create($user,['url'=>'/active-user','class'=>'text-c','id'=>'active_form']); ?>
								<?= $this->Form->hidden('id', ['id'=> 'user_id'] );?>
									<?= $this->Form->button(__('<i class="fa fa-power-off"></i> Re-active this user'), ['class'=>'btn btn-success','id'=>'active_btn','escape'=>false]); ?>
										<?= $this->Form->end() ?>
											<?php endif; ?>

		</div>
		<!--/.col (left) -->

		<!-- right column -->
		<div class="col-md-8">
			<!-- general form elements -->
			<div class="box box-warning">
				<div class="box-body">
					<div class="users form">
						<?= $this->Form->create($user, ['enctype' => 'multipart/form-data','id' => 'edit_user_form']) ?>

							<div class="form-group">
								<label>* Username:</label>
								<div class="input-group">
									<div class="input-group-addon">
										<i class="fa fa-user-secret"></i>
									</div>
									<?= $this->Form->input('username', ['class'=>'form-control','id'=>'username','placeholder'=>'admin','div'=>false,'label'=>'','error'=>false,'disabled'=>true] ); ?>
								</div>
							</div>

							<div class="form-group">
								<label>* Nice name:</label>
								<div class="input-group">
									<div class="input-group-addon">
										<i class="fa fa-user"></i>
									</div>
									<?= $this->Form->input('nice_name', ['class'=>'form-control','id'=>'nice_name','placeholder'=>'Administrator','div'=>false,'label'=>'','error'=>false] ); ?>
								</div>
								<?= $this->Form->error('nice_name', null, array('class' => 'error-message','div'=>false)); ?>
							</div>

							<div class="form-group">
								<label>Email:</label>
								<div class="input-group">
									<div class="input-group-addon">
										<i class="fa fa-envelope-o"></i>
									</div>
									<?= $this->Form->input('email', ['class'=>'form-control',
                                                            'id'=>'email',
                                                            'placeholder'=>'Your email',
                                                            'div'=>false,'label'=>'',
                                                            'error'=>false
                                                        ] 
                                                ); ?>
								</div>
								<?= $this->Form->error('email', null, array('class' => 'error-message','div'=>false)); ?>
							</div>

							<div class="form-group">
								<?= $this->Form->input('role', [
                                                            'options' => $roles,
                                                            'class'=>'form-control',
                                                            'error'=>false
                                                        ]) ?>
									<?= $this->Form->error('role', null, array('class' => 'error-message','div'=>false)); ?>
							</div>

							<div class="form-group">
								<label>Avatar:</label>
								<div class="input-group">
									<div class="input-group-addon">
										<i class="fa fa-photo"></i>
									</div>
									<?= $this->Form->file('avatar_file', ['class'=>'form-control','id'=>'avatar','div'=>false,'label'=>'','error'=>false] ); ?>
								</div>
								<?= $this->Form->error('avatar', null, array('class' => 'error-message','div'=>false)); ?>
							</div>

							<?= $this->Form->hidden('modified', ['value'=> date('Y-m-d H:i:s')] );?>

								<?= $this->Html->link('<i class="fa fa-stop"></i> Cancel', '/user-list',  ['class' => 'btn btn-app fr','id'=>'cancelbtn','target' => '','escape' => false]); ?>
									<?= $this->Form->button(__('<i class="fa fa-save"></i> Save'), ['class'=>'btn btn-app fr','id'=>'edit_user_btn','escape'=>false]); ?>
										<?= $this->Form->end() ?>

											<?= $this->Html->link('<i class="fa fa-key"></i> Edit this user password', '/edit-password/'.$user['id'],  ['class' => 'btn btn-app btn-danger fl m0','id'=>'changepassbtn','target' => '','escape' => false]); ?>
					</div>
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>
		<!--/.col (right) -->

	</div>
	<!-- /.row -->

	<script type="text/javascript">
		$().ready(function() {

			$("#edit_user_form").validate({
				rules: {
					nice_name: "required",
					role: "required",
					email: {
						email: true
					},
					avatar_file: {
						accept: "image/jpeg, image/pjpeg",
					}
				},
				messages: {
					nice_name: "Please enter your nice name",
					email: "Please enter a valid email address",
					avatar_file: {
						accept: "Please select only image file",
					}
				},
				highlight: function(element) {
					$(element).closest('.form-group').addClass('has-error');
				},
				unhighlight: function(element) {
					$(element).closest('.form-group').removeClass('has-error');
				},
				errorElement: 'span',
				errorClass: 'help-block',
				errorPlacement: function(error, element) {
					if (element.parents('.input-group').length) {
						error.insertAfter(element.parents(".input-group"));
					} else {
						error.insertAfter(element);
					}
				}
			});


		}); //end ready

		$('#edit_user_btn').on('click', function(e) {
			e.preventDefault();
			var form = $('#edit_user_btn').parents('#edit_user_form');
			swal({
				title: "Are you sure?",
				text: "You will update the user informtion!",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#008d4c",
				confirmButtonText: "Yes, do it!",
				closeOnConfirm: true
			}, function(isConfirm) {
				if (isConfirm) form.submit();
			});
		});

		$('#active_btn').on('click', function(e) {
			e.preventDefault();
			var form = $('#active_btn').parents('#active_form');
			swal({
				title: "Are you sure?",
				text: "You will set the user active!",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#008d4c",
				confirmButtonText: "Yes, do it!",
				closeOnConfirm: true
			}, function(isConfirm) {
				if (isConfirm) form.submit();
				swal({
					title: "The user has been actived!",
					text: "It will close in 2 seconds.",
					timer: 2000,
					showConfirmButton: false
				});
			});
		});

		$('#ban_btn').on('click', function(e) {
			e.preventDefault();
			var form = $('#ban_btn').parents('#ban_form');
			swal({
				title: "Are you sure?",
				text: "You will block this user",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, do it!",
				closeOnConfirm: true
			}, function(isConfirm) {
				if (isConfirm) form.submit();
				swal({
					title: "The user has been blocked!",
					text: "It will close in 2 seconds.",
					timer: 2000,
					showConfirmButton: false
				});
			});
		});
	</script>