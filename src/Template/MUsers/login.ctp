<!-- File: src/Template/Users/login.ctp -->
<!DOCTYPE html>
<html>
<head>
    <?php echo $this->Html->charset(); ?>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <title>ToSa Lab | Log in</title>
    
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    
    <!-- Bootstrap 3.3.6 -->
    <?php echo $this->Html->css('bootstrap/css/bootstrap.min.css'); ?>
    <!-- Font Awesome -->
    <?php echo $this->Html->css('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css'); ?>
    <!-- Ionicons -->
    <?php //echo $this->Html->css('https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css'); ?>
    <!-- Theme style -->
    <?php echo $this->Html->css('login.css'); ?>
    <!-- My common CSS -->
    <?php echo $this->Html->css('common.css'); ?>
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a style="color:#FFF!important;"><b>ToSa</b>lab</a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Sign in to start your jobs</p>

    <div class="users form">
    <?= $this->Form->create() ?>
    
        <div class="form-group has-feedback">
            <input type="text" class="form-control" placeholder="Username" name="username" autocomplete="off">
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
            <input type="password" class="form-control" placeholder="Password" name="password">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <!--<button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>-->
                <?= $this->Form->button(__('Log in') , array('class'=>'btn btn-warning btn-block btn-flat') ); ?>
            </div>
            <!-- /.col -->
        </div>
    <?= $this->Form->end() ?>
        
    </div>

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<?= $this->Flash->render('flash') ?>


<!-- jQuery -->
<?php echo $this->Html->script('jquery/jquery.min.js'); ?>
<!-- Bootstrap 3.3.6 -->
<?php echo $this->Html->script('bootstrap/bootstrap.min.js'); ?>
<!-- iCheck -->
<?php echo $this->Html->script('plugins/iCheck/icheck.min.js'); ?>

<script>
	/*** Message timeout for flash() of CakePHP ***/
	setTimeout(function(){
	$("div.message").fadeOut("slow", function () {
		$(this).remove()
		;})
	;}, 
	4000);
</script>
</body>
</html>