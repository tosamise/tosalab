<!-- src/Template/Users/updatepass.ctp -->
<?= $this->Flash->render('flash');?>

<div class="row">
    <!-- left column -->
    <div class="col-md-5">
        <!-- general form elements -->
        <div class="box box-warning">
            <div class="box-header with-border">
            <h3 class="box-title">Please fill all fields below with <strong class="red-text">* is required one</strong></h3>
            </div>

            <div class="box-body">
                <div class="users form">
                <?= $this->Form->create($user, ['id' => 'update_pass_form']) ?>
                  
                    <div class="form-group">
                        <label>Username:</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-user-secret"></i>
                            </div>
                            <?= $this->Form->input('username', ['class'=>'form-control','id'=>'username','placeholder'=>'admin','div'=>false,'label'=>'','disabled'=>'disabled'] ); ?>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label>* Current password:</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-key"></i>
                            </div>
                            <?= $this->Form->password('old_pass', ['class'=>'form-control','id'=>'old_pass','placeholder'=>'Current password','div'=>false,'label'=>'','error' => false] ); ?>
                        </div>
                        <?= $this->Form->error('old_pass', null, array('class' => 'error-message','div'=>false)); ?>
                    </div>
                    
                    <div class="form-group">
                        <label>* New password</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-key"></i>
                            </div>
                            <?= $this->Form->password('new_pass', ['class'=>'form-control','id'=>'new_pass','placeholder'=>'New password','div'=>false,'label'=>'','error' => false] ); ?>
                        </div>
                        <?= $this->Form->error('new_pass', null, array('class' => 'error-message','div'=>false)); ?>
                    </div>
                    
                    <div class="form-group">
                        <label>* Retype new password</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-key"></i>
                            </div>
                            <?= $this->Form->password('confirm_pass', ['class'=>'form-control','id'=>'confirm_pass','placeholder'=>'Retype new password','div'=>false,'label'=>'','error' => false] ); ?>
                        </div>
                        <?= $this->Form->error('confirm_pass', null, array('class' => 'error-message','div'=>false)); ?>
                    </div>
                    
                    <?= $this->Form->hidden('modified', ['value'=> date('Y-m-d H:i:s')] );?>
                    
                <?= $this->Html->link('<i class="fa fa-stop"></i> Cancel', '/update-info/'.$user['id'],  ['class' => 'btn btn-app fr','id'=>'cancelbtn','target' => '','escape' => false]); ?>
                <?= $this->Form->button(__('<i class="fa fa-save"></i> Save'), ['class'=>'btn btn-app fr','id'=>'update_pass_btn','escape'=>false]); ?>
                <?= $this->Form->end() ?>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <!--/.col (left) -->
</div>
<!-- /.row -->

<script type="text/javascript">
    $().ready(function() {
	    $("#update_pass_form").validate({
			rules: {
				old_pass: {
				    required: true,
				    minlength: 6
				},
				new_pass: {
				    required: true,
				    minlength: 6
				},
				confirm_pass: {
				    required: true,
				    minlength: 6,
				    equalTo: "#new_pass"
				},
			},
			messages: {
				old_pass: {
				    required: "Please enter your current password",
				    minlength: "Password is at least 6 characters",
				},
				new_pass: {
				    required: "Please enter your current password",
				    minlength: "Password is at least 6 characters",
				},
				confirm_pass: {
				    required: "Please enter your current password",
				    minlength: "Password is at least 6 characters",
				    equalTo: "Please enter the same password as above"
				},
				
			},
			highlight: function(element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-group').removeClass('has-error');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function(error, element) {
                if(element.parents('.input-group').length) {
                    error.insertAfter(element.parents(".input-group"));
                } else {
                    error.insertAfter(element);
                }
            },
            submitHandler: function () {
                return false
            }
	    });
	    
	    $('#update_pass_btn').on('click',function(e){
        e.preventDefault();
        var form = $('#update_pass_btn').parents('#update_pass_form');
            swal({
                title: "Are you sure?",
                text: "You will change your password!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#008d4c",
                confirmButtonText: "Yes, do it!",
                closeOnConfirm: true
            }, function(isConfirm){
                if (isConfirm) form.submit();
            });
        });
	});
	
</script>