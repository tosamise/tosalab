<!-- All Users -->
<?= $this->Flash->render('flash') ?>
<?php $LoggedUser = $this->request->session()->read('Auth.User'); ?>
<div class="row">
    <div class="col-md-12 ">
        <div class="box box-warning">
            <div class="box-header with-border">
                <h3 class="box-title">All users</h3>
        
                <div class="box-tools pull-right">
                    <span class="label label-danger">
                        <?php echo $this->Paginator->counter(['format' => 'Page {{page}} of {{pages}}' ]); ?>
                    </span>
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
            </button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
                <ul class="users-list clearfix">
                    <?php foreach($users as $user): ?>
                    
                        <?php $img = $this->Html->image($user->avatar, ['alt' => $user['nice_name'],'class' => 'img-circle w150 minw150 minh150']) ?>
                        <?php
                        if( isset($LoggedUser) )
                        {
                            //Be as Admin, can see all users without any exceptions.
                            if( $LoggedUser['role'] == 1 ){
                                echo "<li>";
                                echo $this->Html->link( $img ,  
                                    'edit-user/'.$user->id,
                                    ['class' => '', 'target' => '','escape' => false]); 
                                
                                echo $this->Html->link( $user->nice_name ,  
                                    'edit-user/'.$user->id,
                                    ['class' => 'users-list-name hidden-xs hidden-sm', 'target' => '','escape' => false]);
                                //Case user was block by admin
                                if( $user->is_delete != 1 )
                                {
                                    echo "<span class='users-list-date hidden-xs hidden-sm'>".$roles[$user->role]."</span>";
                                }
                                else 
                                {
                                    echo "<span class='users-list-date hidden-xs hidden-sm red-text'>User was blocked</span>";
                                }
                                echo "</li>";
                            }
                            else 
                            {
                                //Be as Users, can only see actived users.
                                if( $user->is_active != 1 )
                                { 
                                    echo "<li>";
                                    echo $this->Html->link( $img ,  
                                        'view-user/'.$user->id,
                                        ['class' => '', 'target' => '','escape' => false]); 
                                    
                                    echo $this->Html->link( $user->nice_name ,  
                                        'view-user/'.$user->id,
                                        ['class' => 'users-list-name hidden-xs hidden-sm', 'target' => '','escape' => false]);
                                    //Case user was block by admin
                                    if( $user->is_delete != 1 )
                                    {
                                        echo "<span class='users-list-date hidden-xs hidden-sm'>".$roles[$user->role]."</span>";
                                    }
                                    else 
                                    {
                                        echo "<span class='users-list-date hidden-xs hidden-sm red-text'>User was blocked</span>";
                                    }
                                    echo "</li>";
                                }    
                            }  
                        }
                        else 
                        {   
                            //if the logged in session not found, required to login
                            $this->redirect('/login');
                        }
                        ?>
                        
                        
                        
                    </li>
                    <?php endforeach; ?>
                </ul>
                <!-- /.users-list -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer text-center">
                <ul class="pagination pagination-sm no-margin pull-right">
                    <?php 
                        // Shows the next links
                        echo $this->Paginator->prev('« Previous');
                        //Shows the page numbers
                        echo $this->Paginator->numbers();
                        // Shows the previous links
                        echo $this->Paginator->next('Next »');
                    ?>
                </ul>
                
                <button class="btn btn-default fl" type="button">
                    <?php echo $this->Paginator->counter(['format' => '{{current}} of {{count}}']);?>
                </button>
                
            </div>
            <!-- /.box-footer -->
        </div>
        <!-- .box -->
        
        <?= $this->Form->create('search', ['url'=>'/user-list','class'=>'sidebar-form m0 custom-search-bar']) ?>
          <div class="input-group">
            <?php echo $this->Form->input('Search', ['class'=>'form-control','placeholder'=>'Search users...','name'=>'search','div'=>false,'label'=>false]);?>
            <span class="input-group-btn">
              <?= $this->Form->button('<i class="fa fa-search"></i>',['class'=>'btn btn-flat','escape'=>false,'name'=>'searchbtn','id'=>'search-btn','div'=>false]) ?>
            </span>
          </div>
        <?= $this->Form->end() ?>
        
        
    </div>
    <!-- .col -->
</div>    