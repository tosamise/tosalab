<!-- src/Template/Users/update.ctp -->
<?= $this->Flash->render('flash') ?>
<?php $LoggedUser = $this->request->session()->read('Auth.User');?>
<div class="row">
    <!-- left column -->
    <div class="col-md-4">
        <div class="box box-widget widget-user">
        <!-- Add the bg color to the header using any of the bg-* classes -->
        <div class="widget-user-header bg-yellow-active">
            <h3 class="widget-user-username"><?= $user['nice_name'] ?></h3>
            <h5 class="widget-user-desc"><?= $roles[$LoggedUser['role']] ?></h5>
        </div>
        <div class="widget-user-image">
            <?= $this->Html->image($user->avatar, ['class'=>'img-circle']) ?>
                <!--<img class="img-circle" src="/img/avatars/default-avatar.png" alt="User Avatar">-->
        </div>
        <div class="box-footer">
            <div class="row">
                <div class="col-sm-4 border-right">
                    <div class="description-block">
                        <h5 class="description-header">10</h5>
                        <span class="description-text">Roll(s)</span>
                    </div>
                    <!-- /.description-block -->
                </div>
                <!-- /.col -->
                <div class="col-sm-4 border-right">
                    <div class="description-block">
                        <h5 class="description-header">10</h5>
                        <span class="description-text">Camera(s)</span>
                    </div>
                    <!-- /.description-block -->
                </div>
                <!-- /.col -->
                <div class="col-sm-4">
                    <div class="description-block">
                        <h5 class="description-header">350</h5>
                        <span class="description-text">Photo(s)</span>
                    </div>
                    <!-- /.description-block -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
    </div>

    <div class="box box-warning">
        <div class="box-header with-border">
            <h3 class="box-title">About Me</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            
            <strong><i class="fa fa-user margin-r-5"></i>
			<?= ($user['nice_name']?$user['nice_name']:"Unknown") ?>
            </strong>
            <p class="text-muted">
			<?= ($user['first_name'] && $user['last_name']?$user['first_name']." ".$user['last_name']:"Unknown") ?>
            </p>

            <hr>
            
            <strong><i class="fa fa-birthday-cake margin-r-5"></i> Date of birth:</strong> 
            <?= ($user['dob']?date_format($user['dob'], 'F jS Y'):"Unknown") ?>
            
            <hr>
            
            <strong><i class="fa fa-map-marker margin-r-5"></i> Your current location</strong>
            <p class="text-muted"><?= ($user->city?$cities[$user->city]:"Unknown") ?></p>

            <hr>

            <strong><i class="fa fa-envelope-o margin-r-5"></i> Email: </strong>
            <?= ($user['email']?$user['email']:"Unknown") ?>
            <hr>

            <strong><i class="fa fa-quote-right margin-r-5"></i></strong>

            <p>Let's live as you will die tomorrow</p>
        </div>
        <!-- /.box-body -->
        <?= $this->Html->link('<i class="fa fa-key"></i> Deactivate account', '/deactivate-user/'.$user['id'],  ['class' => 'btn btn-danger fl m0 mt20','id'=>'deactivate_btn','target' => '','escape' => false]); ?>
    </div>

</div>
<!--/.col (left) -->

    <!-- right column -->
    <div class="col-md-8">
        <!-- general form elements -->
        <div class="box box-warning">
            <div class="box-header with-border">
            <h3 class="box-title">Please fill all fields below with <strong class="red-text">* is required one</strong></h3>
            </div>

            <div class="box-body">
                <div class="users form">
                <?= $this->Form->create($user, ['enctype' => 'multipart/form-data','id' => 'update_user_form']) ?>
                  
                    <div class="form-group">
                        <label>* Username:</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-user-secret"></i>
                            </div>
                            <?= $this->Form->input('username', ['class'=>'form-control','id'=>'username','placeholder'=>'admin','div'=>false,'label'=>'','disabled'=>'disabled','title'=>'Cannot edit username','error'=>false] ); ?>
                        </div>
                        <?= $this->Form->error('username', null, array('class' => 'error-message','div'=>false)); ?>
                    </div>
                    
                    <div class="form-group">
                        <label>* Nice name:</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-user"></i>
                            </div>
                            <?= $this->Form->input('nice_name', ['class'=>'form-control','id'=>'nice_name','placeholder'=>'Administrator','div'=>false,'label'=>'','error'=>false] ); ?>
                        </div>
                        <?= $this->Form->error('nice_name', null, array('class' => 'error-message','div'=>false)); ?>
                    </div>
                    
                    <div class="form-group">
                        <label>First name:</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-user"></i>
                            </div>
                            <?= $this->Form->input('first_name', ['class'=>'form-control','id'=>'first_name','placeholder'=>'Tommy','div'=>false,'label'=>'','error'=>false] ); ?>
                        </div>
                        <?= $this->Form->error('first_name', null, array('class' => 'error-message','div'=>false)); ?>
                    </div>
                    
                    <div class="form-group">
                        <label>Last name:</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-users"></i>
                            </div>
                            <?= $this->Form->input('last_name', ['class'=>'form-control','id'=>'last_name','placeholder'=>'Do','div'=>false,'label'=>'','error'=>false] ); ?>
                        </div>
                        <?= $this->Form->error('last_name', null, array('class' => 'error-message','div'=>false)); ?>
                    </div>
                    
                    <div class="form-group">
                        <label>Date of Birth:</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-birthday-cake"></i>
                            </div>
                            <?php if($user['dob']){$dob = $user['dob']->format('Y-m-d');}else{$dob = "1900-01-01";} ?>
                            <?= $this->Form->input('dob', ['type'=>'text','value'=> $dob,'class'=>'form-control','id'=>'dob','placeholder'=>'1990/04/07','div'=>false,'label'=>'','error'=>false,'autocomplete'=>'off'] ); ?>
                        </div>
                        <?= $this->Form->error('dob', null, array('class' => 'error-message','div'=>false)); ?>
                    </div>
                    
                    <div class="form-group">
                        <?= $this->Form->input('gender', [
                            'options' => ['male' => 'Male','female' => 'Female','others' => 'Others'],
                            'class'=>'form-control',
                            'error'=>false
                        ]) ?>
                        <?= $this->Form->error('gender', null, array('class' => 'error-message','div'=>false)); ?>
                    </div>
                    
                    <div class="form-group">
                        <label>ID number:</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-credit-card-alt"></i>
                            </div>
                            <?= $this->Form->input('id_card', ['class'=>'form-control',
                                                                'id'=>'id_card',
                                                                'placeholder'=>'024758360',
                                                                'div'=>false,'label'=>'',
                                                                'error'=>false,
                                                                'data-inputmask'=>'"mask": "999999999"',
                                                                'data-mask'
                                                            ]
                                                        ); ?>
                        </div>
                        <?= $this->Form->error('id_card', null, array('class' => 'error-message','div'=>false)); ?>
                    </div>                    
                    
                    <div class="form-group">
                        <label>Passport:</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-credit-card-alt"></i>
                            </div>
                            <?= $this->Form->input('passport', ['class'=>'form-control',
                                                                'id'=>'passport',
                                                                'placeholder'=>'B1234567',
                                                                'div'=>false,'label'=>'',
                                                                'error'=>false,
                                                            ]
                                                        ); ?>
                        </div>
                        <?= $this->Form->error('passport', null, array('class' => 'error-message','div'=>false)); ?>
                    </div>
                    
                    <div class="form-group">
                        <label>Phone:</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-phone"></i>
                            </div>
                            <?= $this->Form->input('phone', ['class'=>'form-control',
                                                            'id'=>'phone',
                                                            'placeholder'=>'Your phone',
                                                            'div'=>false,'label'=>'',
                                                            'data-inputmask'=>'"mask": "9999999999"',
                                                            'data-mask',
                                                            'error'=>false
                                                        ] 
                                                ); ?>
                        </div>
                        <?= $this->Form->error('phone', null, array('class' => 'error-message','div'=>false)); ?>
                    </div>
                    
                    <div class="form-group">
                        <label>Email:</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-envelope-o"></i>
                            </div>
                            <?= $this->Form->input('email', ['class'=>'form-control',
                                                    'id'=>'email',
                                                    'placeholder'=>'Your email',
                                                    'div'=>false,'label'=>'',
                                                    'error'=>false
                                                ] 
                                        ); ?>
                        </div>
                        <?= $this->Form->error('email', null, array('class' => 'error-message','div'=>false)); ?>
                    </div>
                    
                    <div class="form-group">
                        <label>Address:</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-map-marker"></i>
                            </div>
                            <?= $this->Form->input('address', ['class'=>'form-control','id'=>'address','placeholder'=>'133 Dương Bá Trạc, P.1, Q.8','div'=>false,'label'=>'','error'=>false] ); ?>
                        </div>
                        <?= $this->Form->error('address', null, array('class' => 'error-message','div'=>false)); ?>
                    </div>
                    
                    <div class="form-group">
                        <?= $this->Form->input('city', [
                            'options' => $cities,
                            'id' => 'select_city',
                            'class'=>'form-control',
                            'error'=>false
                        ]) ?>
                        <?= $this->Form->error('city', null, array('class' => 'error-message','div'=>false)); ?>
                    </div>
                    
                    <?= $this->Form->hidden('dist_tmp', ['value'=> $user['district'],'id'=>'dist_tmp' ] );?>
                    <div class="form-group">
                        <?= $this->Form->input('district', [
                            'id' => 'select_dist',
                            'options' => [],
                            'class'=>'form-control',
                            'error'=>false
                        ]) ?>
                        <?= $this->Form->error('district', null, array('class' => 'error-message','div'=>false)); ?>
                    </div>
                    
                    <?= $this->Form->hidden('ward_tmp', ['value'=> $user['ward'],'id'=>'ward_tmp' ] );?>
                    <div class="form-group">
                        <?= $this->Form->input('ward', [
                            'id' => 'select_ward',
                            'options' => [],
                            'class'=>'form-control',
                            'error'=>false
                        ]) ?>
                        <?= $this->Form->error('ward', null, array('class' => 'error-message','div'=>false)); ?>
                    </div>
                    
                    <div class="form-group">
                        <label>Avatar:</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-photo"></i>
                            </div>
                            <?= $this->Form->file('avatar_file', ['class'=>'form-control','id'=>'avatar','div'=>false,'label'=>'','error'=>false] ); ?>
                        </div>
                        <?= $this->Form->error('avatar', null, array('class' => 'error-message','div'=>false)); ?>
                    </div>
                    
                    <?= $this->Form->hidden('modified', ['value'=> date('Y-m-d H:i:s')] );?>
                    <?php $LoggedUser = $this->request->session()->read('Auth.User'); ?>
                    
                <?= $this->Html->link('<i class="fa fa-key"></i> Change password', '/change-password',  ['class' => 'btn btn-app btn-danger fl m0','id'=>'changepassbtn','target' => '','escape' => false]); ?>
                    
                <?= $this->Form->button(__('<i class="fa fa-save"></i> Save'), ['class'=>'btn btn-app fr','id'=>'update_user_btn','escape'=>false]); ?>
                <?= $this->Form->end() ?>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <!--/.col (right) -->
</div>
<!-- /.row -->
<script type="text/javascript">
	$().ready(function() {

	});
	
	$('#update_user_btn').on('click',function(e){
    e.preventDefault();
    var form = $('#update_user_btn').parents('#update_user_form');
        swal({
            title: "Are you sure?",
            text: "You will update your informtion!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#008d4c",
            confirmButtonText: "Yes, do it!",
            closeOnConfirm: true
        }, function(isConfirm){
            if (isConfirm) form.submit();
        });
    });

</script>