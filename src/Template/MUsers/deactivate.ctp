<!-- src/Template/Users/deactivate.ctp -->
<?= $this->Flash->render('flash') ?>

<div class="row">
    <!-- left column -->
    <div class="col-md-6">
        <!-- general form elements -->
        <div class="box box-warning">
            <div class="box-header with-border">
            <h3 class="box-title"><strong class="red-text">Are you sure to deactivate your account?</strong></h3>
            </div>

            <div class="box-body">
                <div class="users form">
                <?= $this->Form->create($user) ?>
                
                    <?= $this->Form->hidden('id', ['value'=> $this->request->params['pass'][0]] );?>
                    <?= $this->Form->hidden('modified', ['value'=> date('Y-m-d H:i:s')] );?>
                    
                <?= $this->Html->link('<i class="fa fa-stop"></i> Cancel', '/update-info/'.$user['id'],  ['class' => 'btn btn-default fr','id'=>'cancel_btn','target' => '','escape' => false]); ?>
                <?= $this->Form->button(__('<i class="fa fa-check"></i> Confirm'), ['class'=>'btn btn-success mr10 fr','escape'=>false]); ?>
                <?= $this->Form->end() ?>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <!--/.col (left) -->
    
</div>
<!-- /.row -->
<script type="text/javascript">

    
</script>