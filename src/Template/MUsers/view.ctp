<!-- src/Template/MUsers/view.ctp -->
<?= $this->Flash->render('flash') ?>

    <div class="row">
        
        <!-- left column -->
        <div class="col-md-4">
            <!-- general form elements -->
            <div class="box box-warning">
                <div class="box-body text-c">
                    <?= $this->Html->image($user->avatar, ['class'=>'w100p']) ?>
                </div>
            </div>
            <!--/.box -->
        </div>
        <!--/.col (left) -->

        <!-- right column -->
        <div class="col-md-8">
            <!-- general form elements -->
            <div class="box box-warning">
                <div class="box-header with-border ml20">
                    <h3><?= ($user['nice_name'])?$user['nice_name']:"Unknown";?></h3>
                </div>

                <div class="box-body fsize16">
                    <table class="ml20">
                        <tr>
                            <td class="w100"><b>Name:</b></td>
                            <td><?= ($user['first_name'] && $user['last_name'])?$user['first_name']." ".$user['last_name']:"Unknown"; ?></td>
                        </tr>
                        <tr>
                            <td><b>DOB:</b></td>
                            <td><?= ($user['dob']) ? date_format( $user['dob'] , "Y/m/d" ) : "Unknown"; ?></td>
                        </tr>
                        <tr>
                            <td><b>Gender:</b></td>
                            <td><?= ($user['gender']?$user['gender']:"Unknown") ?></td>
                        </tr>
                        
                        <?php if( isset( $user['is_delete'] ) && $user['is_delete'] == 1 ): ?>
                        <tr>
                            <td><b>Status:</b></td>
                            <td>
                                <span class='red-text'>This user has been blocked</span>
                            </td>
                        </tr>
                        <?php endif; ?>
                        
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
            <?= $this->Html->link('<i class="fa fa-user"></i> Back to list', '/user-list',  ['class' => 'btn btn-app fr','id'=>'list_btn','target' => '','escape' => false]); ?>  
        </div>
        <!--/.col (right) -->
        
        <!-- below column -->
        <div class="col-md-12">
            <div class="box box-warning">
                <div class="box-header with-border">
                    <h4>This user's photos</h4>
                </div>
                <div class="box-body fsize18">
                    <?= $this->Html->image('login-bg.jpg', ['class'=>'w300 border mr20 mt20']) ?>
                    <?= $this->Html->image('login-bg.jpg', ['class'=>'w300 border mr20 mt20']) ?>
                    <?= $this->Html->image('login-bg.jpg', ['class'=>'w300 border mr20 mt20']) ?>
                    <?= $this->Html->image('login-bg.jpg', ['class'=>'w300 border mr20 mt20']) ?>
                    <?= $this->Html->image('login-bg.jpg', ['class'=>'w300 border mr20 mt20']) ?>
                    <?= $this->Html->image('login-bg.jpg', ['class'=>'w300 border mr20 mt20']) ?>
                </div>
                <div class="box-footer">
                    <?= $this->Html->link('<i class="fa fa-hand-o-right"></i> See others...', '/gallery',  ['class' => 'text-bold','id'=>'see_more','target' => '','escape' => false]); ?>  
                </div>
            </div>
            <!-- /.box --> 
        </div>
        <!--/.col (below) -->
        
    </div>
    <!-- /.row -->