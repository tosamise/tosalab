<!-- src/Template/Users/add.ctp -->
<?= $this->Flash->render('flash') ?>

<div class="row">
    <!-- left column -->
    <div class="col-md-6">
        <!-- general form elements -->
        <div class="box box-warning">
            <div class="box-body">
                <div class="users form">
                <?= $this->Form->create($user,['id'=>'add_user_form']) ?>
                  
                    <div class="form-group">
                        <label>* Username:</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-user-secret"></i>
                            </div>
                            <?= $this->Form->input('username', ['class'=>'form-control','id'=>'username','placeholder'=>'Your login name','div'=>false,'label'=>'','error'=>false] ); ?>
                        </div>
                        <?= $this->Form->error('username', null, array('class' => 'error-message','div'=>false)); ?>
                    </div>
                    
                    <div class="form-group">
                        <label>* Nice name:</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-user"></i>
                            </div>
                            <?= $this->Form->input('nice_name', ['class'=>'form-control','id'=>'nice_name','placeholder'=>'Your display name','div'=>false,'label'=>'','error'=>false] ); ?>
                        </div>
                        <?= $this->Form->error('nice_name', null, array('class' => 'error-message','div'=>false)); ?>
                    </div>
                    
                    <div class="form-group">
                        <label>* Password:</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-key"></i>
                            </div>
                            <?= $this->Form->input('password', ['class'=>'form-control','id'=>'password','div'=>false,'label'=>'','error'=>false] ); ?>
                        </div>
                        <?= $this->Form->error('password', null, array('class' => 'error-message','div'=>false)); ?>
                    </div>
                    
                    <div class="form-group">
                        <?= $this->Form->input('role', [
                            'options' => $roles,
                            'class'=>'form-control',
                            'error'=>false
                        ]) ?>
                        <?= $this->Form->error('role', null, array('class' => 'error-message','div'=>false)); ?>
                    </div>
                    
                    <?= $this->Form->hidden('created', ['value'=> date('Y-m-d H:i:s')] );?>
                    <?= $this->Form->hidden('avatar', ['value'=> 'avatars/default-avatar.png'] );?>
                
                <?= $this->Form->button(__('<i class="fa fa-repeat"></i> Clear All'), ['type'=>'button','id'=>'clearAllbtn','class'=>'btn btn-app fr','escape'=>false]); ?>    
                <?= $this->Form->button(__('<i class="fa fa-save"></i> Save'), ['class'=>'btn btn-app fr','id'=>'add_user_btn','escape'=>false]); ?>
                <?= $this->Form->end() ?>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <!--/.col (right) -->
    
    <div class="col-md-6">
        <!-- general form elements -->
        <div class="box box-warning">
            <div class="box-header with-border">
                <h3 class="box-title">Latest Members</h3>
            </div>
            
            <div class="box-body no-padding">
                <ul class="users-list clearfix ">
                    <?php foreach($newusers as $user):?>    
                        <li>
                            <?php $img = $this->Html->image($user['avatar'], ['alt' => $user['nice_name'],'class' => 'img-circle w200']) ?>
                            <?php echo $this->Html->link( $img ,  
                                'edit-user/'.$user['id'],
                                ['class' => '', 'target' => '','escape' => false]); 
                            ?>
                            <a class="users-list-name" href="edit-user/<?= $user['id'] ?>"><?= $user['nice_name'] ?></a>
                            <span class="users-list-date">Joined: <?= $user['created']->format('Y-m')?></span>
                        </li>
                    <?php endforeach;?> 
                </ul>
                <!-- /.users-list -->
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <!--/.col (right) -->
    
</div>
<!-- /.row -->
<script type="text/javascript">
	$().ready(function() {
	    $("#add_user_form").validate({
			rules: {
				username: "required",
				nice_name: "required",
				password: "required",
				role: "required",
				username: {
					required: true,
					minlength: 6
				},
				password: {
					required: true,
					minlength: 6
				},
			},
			messages: {
				username: "Please enter your username",
				nice_name: "Please enter your nice name",
				username: {
					required: "Please enter a username",
					minlength: "The username must consist of at least 6 characters"
				},
				password: {
					required: "Please enter a password",
					minlength: "The password must consist of at least 6 characters"
				},
			},
			highlight: function(element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-group').removeClass('has-error');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function(error, element) {
                if(element.parents('.input-group').length) {
                    error.insertAfter(element.parents(".input-group"));
                } else {
                    error.insertAfter(element);
                }
            }
	    }); 
	});
	
	//set input username to small and remove space.
	$('#username').on('change',function(){
	    var str = $('#username').val();
	    str = str.toLowerCase();
	    str = str.replace(/\s+/g, '_');
	    $('#username').val(str);
	});
	
	$('#add_user_btn').on('click',function(e){
        e.preventDefault();
        var form = $('#add_user_btn').parents('#add_user_form');
        swal({
            title: "Are you sure?",
            text: "You will add the new lab address!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#008d4c",
            confirmButtonText: "Yes, do it!",
            closeOnConfirm: true
        }, function(isConfirm){
            if (isConfirm) form.submit();
        });
    });
</script>