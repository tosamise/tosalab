<!--  -->
<?php $LoggedUser = $this->request->session()->read('Auth.User'); ?>
<?php $product_url = '../upload/pro_images/'; ?>
<div class="row">
    <div class="col-md-12 ">
        <div class="box box-warning">
            <div class="box-header with-border">
                <h4><?= ($this->request->query('cate')?$cates[$this->request->query('cate')]:"") ?></h4>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                
                <?php
                foreach($products as $product): ?>
                <div class="col-md-3 minh150 minw150">
                    <div class="box text-c">
                        <div class="box-body text-c" id="">
                        <?php 
                            if( $product->img_name != null ):
                                $img = $this->Html->image($product_url.$product->img_name, ['class'=>'pro_img w100p','id'=>$product['pro_id'] ]);
                            else: 
                                $img = $this->Html->image('no-image.png', ['class'=>'pro_img w100p','id'=>'' ]);
                            endif; 
                        ?>
                        <?php
                            $tag_array = explode("," , $product['tags_id'] );
                            $tag_string = [];
                            $new_tags = $tags->toArray();
                            for ($i=0; $i < count($tag_array) ; $i++) { 
                                //echo $tag_array[$i];
                                if( $new_tags[$tag_array[$i]] )
                                {
                                    array_push( $tag_string , $new_tags[$tag_array[$i]]);
                                }
                            }
                            $tag_string = implode(", ", $tag_string)
                        ?>
                        <?php echo $this->Html->link( $img , '#view_detail',
                                                    ['class' => 'image-zoom popup-modal',
                                                     'target' => '',
                                                     'escape' => false,
                                                     'data_pro_id' => $product['pro_id'],
                                                     'data_pro_code' => $product['pro_code'],
                                                     'data_cate_code' => $product['cate_code'],
                                                     'data_color' => $product['color'],
                                                     'data_img_name' => $product['img_name'],
                                                     'data_quality' => $product['quality'],
                                                     'data_quantity' => $product['quantity'],
                                                     'data_price' => $product['price'],
                                                     'data_price_up' => $product['price_up'],
                                                     'data_tags' => $product['tags_id'],
                                                     'data_tags_name' => $tag_string,
                                                     'data_date' => date_format($product['import_date'],"Y-m-d"),
                                                     'data_sold' => $product['is_sold'],
                                                    ]); 
                        ?>
                        </div>
                        <caption><i>
                        <?php if($product['is_sold'] != 1): ?>
                        <?= $product['cate_code'].$product['pro_code']?>
                        <?php else: ?>    
                        <b class="red-text">Sold</b>
                        <?php endif; ?>
                        </i></caption>
                    </div>
                </div>
                <?php endforeach; ?>
                
            </div>
            <!-- /.box-body -->
            
            <div class="box-footer text-center"> 
                <ul class="pagination pagination-sm no-margin pull-right">
                    <?php 
                        // Shows the next links
                        echo $this->Paginator->prev('« Previous');
                        //Shows the page numbers
                        echo $this->Paginator->numbers();
                        // Shows the previous links
                        echo $this->Paginator->next('Next »');
                    ?>
                </ul>
            </div>
            <!-- /.box-footer -->
        </div>
        <!-- .box -->
        
        <?= $this->Form->create('search', ['url'=>'/products','class'=>'sidebar-form m0 custom-search-bar']) ?>
          <div class="input-group">
            <?php echo $this->Form->input('Search', ['class'=>'form-control','placeholder'=>'Search products...','name'=>'search','div'=>false,'label'=>false]);?>
            <span class="input-group-btn">
              <?= $this->Form->button('<i class="fa fa-search"></i>',['class'=>'btn btn-flat','escape'=>false,'name'=>'searchbtn','id'=>'search-btn','div'=>false]) ?>
            </span>
          </div>
        <?= $this->Form->end() ?>
        
    </div>
    <!-- .col -->
</div>    

<!-- Popup edit -->
<div id="view_detail" class="mfp-hide white-popup-block uploadPopup" style="border: 3px solid #f39c12;">
	<h3></h3>
	<div class="box-body">
        <div class="row">
            <div class="col-md-5 minh150 minw150 text-c">
                <div class="row">
                    <img src="" class="pro_img popup-img col-md-10 col-xs-12" id="preview_url" alt="">
                </div>
            </div>
            <div class="col-md-7">
                <div class="row">
                    <div class="col-md-12" style="font-size:50px"><b>#<span id="preview_pro_code"></span></b></div>
                </div>
                <div class="row">
                    <div id="show_form" class="box box-default mb20" style="font-size:18px">
                        <div class="col-md-4 mt20"><b>Color:</b></div>
                        <div class="col-md-8 mt20"><span id="preview_color"></span></div>
                        <div class="col-md-4 mt20"><b>Quality:</b></div>
                        <div class="col-md-5 mt20"><span id="preview_quality"></span></div>
                        <div class="col-md-4 mt20"><b>Quantity:</b></div>
                        <div class="col-md-5 mt20"><span id="preview_quantity"></span></div>
                        <div class="col-md-4 mt20"><b>Rent price:</b></div>
                        <div class="col-md-5 mt20 price-font"><span id="preview_rent_price"></span></div>
                        <div class="col-md-4 mt20"><b>Sell price:</b></div>
                        <div class="col-md-5 mt20 price-font"><span id="preview_sell_price"></span></div>
                        <div class="col-md-4 mt20"><b>Tags:</b></div>
                        <div class="col-md-5 mt20"><span class="tags" id="preview_tags"></span></div>
                    </div>
                    <form id="edit_form" class="edit-form" method="post" action="/edit-product">
                    <div class="box box-default mb20" style="font-size:18px">
                        <div class="col-md-4 mt20"><b>Category:</b></div>
                        <div class="input-wrap col-md-8 mt20">
                        <?= $this->Form->input('cate_code', [
                            'options' => $cates,
                            'empty' => ["0"=>"Select category"],
                            'id' => 'edit_cate_code',
                            'class' => 'form-control',
                            'error' => false,
                            'label' => false,
                            'placeholder' => 'Select category'
                        ]) ?>
                        </div>

                        <div class="col-md-4 mt20"><b>Product ID:</b></div>
                        <div class="input-wrap col-md-8 mt20">
                            <input value="" id="edit_pro_code" name="pro_code" class="form-control" />
                        </div>

                        <div class="col-md-4 mt20"><b>Color:</b></div>
                        <div class="input-wrap col-md-8 mt20">
                            <input value="" id="edit_color" class="form-control" />
                        </div>

                        <?php 
                            $quality = [];
                            for($i = 60; $i<=100; $i = $i+5 )
                            {
                                $quality += [$i => $i];
                            } 
                        ?>
                        <div class="col-md-4 mt20"><b>Quality: (%)</b></div>
                        <div class="input-wrap col-md-8 mt20">
                            <?= $this->Form->input('quality_stt', [
                                'options' => $quality,
                                'empty' => ["0"=>"Select quality"],
                                'id' => 'edit_quality',
                                'class'=>'form-control',
                                'error'=>false,
                                'label' => false,
                                'div' => false
                            ]) ?>
                        </div>
                        
                        <div class="col-md-4 mt20 red-text"><b>Quantity: </b></div>
                        <div class="input-wrap col-md-8 mt20">
                            <input value="" id="edit_quantity" name="edit_quantity" class="form-control" />
                        </div>

                        <div class="col-md-4 mt20 red-text"><b>Fatory price: (VNĐ)</b></div>
                        <div class="input-wrap col-md-8 mt20">
                            <input value="" id="edit_price" name="edit_price" class="form-control" />
                        </div>
                        
                        <div class="col-md-4 mt20 red-text"><b>Origin price: (VNĐ)</b></div>
                        <div class="input-wrap col-md-8 mt20">
                            <input value="" id="edit_price_up" name="edit_price_up" class="form-control" />
                        </div>
                        
                        <div class="col-md-4 mt20"><b>Import date:</b></div>
                        <div class="input-wrap col-md-8 mt20">
                            <input value="" id="edit_date" class="form-control" />
                        </div>
                        
                        <div class="col-md-4 mt20"><b>Tags:</b></div>
                        <div class="input-wrap col-md-8 mt20">
                            <?= $this->Form->input('tags_id', [
                                'options' => $tags,
                                'id' => 'edit_tags',
                                'class'=>'',
                                'multiple' => true,
                                'error'=>false,
                                'label'=>false,
                                'div'=>false
                            ]) ?>
                        </div>
                        
                        <!--<div class="col-md-4 mt20"><b>Import date:</b></div>-->
                        <!--<div class="input-wrap col-md-8 mt20">-->
                        <!--    <input value="" id="edit_date" class="form-control" />-->
                        <!--</div>-->
                        <input type="hidden" value="" id="pro_code" class="form-control" />
                        <input type="hidden" value="" id="pro_id" class="form-control" />
                        <input type="hidden" value="<?= date('Y-m-d h:i:s'); ?>" id="modified" class="form-control" />
                        <div class="col-md-12 mt20 mb20"></div>
                    </div>
                    </form>
                </div>
            </div>  
        </div>
        <?= $this->Form->button(__('<i class="fa fa-times"></i> Close'), ['class'=>'btn btn-app fr','id'=>'close_btn','escape'=>false,'type'=>'button']); ?>

        <?php if($LoggedUser['role'] == '1'): ?>
        <?= $this->Form->button(__('<i class="fa fa-pencil"></i> Edit'), ['class'=>'btn btn-app fr','id'=>'edit_btn','escape'=>false,'type'=>'button']); ?>
        <?= $this->Form->button(__('<i class="fa fa-save"></i> Save'), ['class'=>'btn btn-app fr','id'=>'save_btn','escape'=>false,'type'=>'button']); ?>
        <?= $this->Form->button(__('<i class="fa fa-shopping-cart"></i> Sold'), ['class'=>'btn btn-app fr','id'=>'sold_btn','escape'=>false,'type'=>'button']); ?>
        <?php endif; ?>

    </div>
    <!-- /.box-body -->
</div>
<?php echo $this->Html->script('price.js'); ?>
<?php echo $this->Html->script('plugins/numberformat/jquery.number.js'); ?>
<script type="text/javascript">
$().ready(function() 
{
    
    var $select = $('#edit_tags').selectize({
        maxOptions : 5,
        maxItems: 5,
        persist: false,
        createOnBlur: true,
        create: true
    });
    var selectize = $select[0].selectize;
    
    $(function () {
    	$('.popup-modal').magnificPopup({
    		type: 'inline',
    		preloader: false,
    		focus: '#view_detail',
    		modal: true
    	});
    	$(document).on('click', '#close_btn', function (e) {
    		e.preventDefault();
    		$.magnificPopup.close();
    	});
    });

    $("#edit_form").validate({
        rules: {
            cate_code: {
                checkSelect: true,
            },
            
            quality_stt: {checkSelect: true,},
            edit_quantity: {
			    required: true,
			    number: true,
			    maxlength: 2,
			},
            pro_code: {
                required: true,
                minlength: 4,
                maxlength: 4,
                checkProCode: true
            },
            edit_price: {checkPrice: true,},
            edit_price_up: {checkPrice: true,},
        },
            tags_id: {noNumber: true,},
        messages: {
            cate_code: {
                checkSelect: "Please select the item",
            },
            quality_stt: {checkSelect: "Please select the item",},
            edit_quantity: {
			    required: "This field is required",
			    number: "Only number is allowed",
			    maxlength: "Only 02 digits are allowed",
			},
            pro_code: {
                required: "This field is required",
                minlength: "At least 4 digits",
                maxlength: "Maximum 4 digits",
                checkProCode: "Invalid product code"
            },
            edit_price: {checkPrice: "This field is required",},
            edit_price_up: {checkPrice: "This field is required",},
            tags_id: {noNumber: "No number accepted",},
        },
        highlight: function(element) {
            $(element).parents('.input-wrap').addClass('has-error');
            $('#save_btn').prop('disabled',true);
        },
        unhighlight: function(element) {
            $(element).parents('.input-wrap').removeClass('has-error');
            $('#save_btn').prop('disabled',false);
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function(error, element) {
            if(element.parents('.input-group').length) {
                error.insertAfter(element.parents(".input-group"));
            } else {
                error.insertAfter(element);
            }
            
        },
    });
    
    $.validator.addMethod("checkProCode", function(value, element) {
        return this.optional(element) || /[0-9]{4}\b/.test(value); 
    });

    $.validator.addMethod("checkSelect", function(value, element) {
        return this.optional(element) || value != 0; 
    });

    $.validator.addMethod("checkPrice", function(value, element) {
        return this.optional(element) || value != 0; 
    });
    
    $.validator.addMethod("noNumber", function(value, element) {
        return this.optional(element) || value.isNumeric();
    });
    
    $( ".popup-modal" ).each(function(index) 
    {
        $(this).on("click", function()
        {
            $('#edit_btn').html("<i class='fa fa-pencil'></i> Edit");
            $("#save_btn").hide();
            $('#show_form').show();
            $('#edit_form').hide();
            $("#sold_btn").show();

            $("#preview_url").attr( 'src' , $(this).find('.pro_img').attr('src') );
            $("#preview_pro_code").text(""+$(this).attr('data_cate_code')+$(this).attr('data_pro_code')+"");
            $("#is_sold").remove();
            if($(this).attr('data_sold') == 1)
            {
                $("<b id='is_sold' class='red-text'> SOLD</b>").insertAfter("#preview_pro_code");
                $("#sold_btn").hide();
            }
            $("#preview_color").text( ($(this).attr('data_color') != "" ? $(this).attr('data_color') : "Unknown") );
            
            var quality = Number( $(this).attr('data_quality') );
            $("#preview_quality").text(""+(quality?quality:"100")+"%");
            $("#preview_quantity").text($(this).attr('data_quantity'));
            
            var price_up = Number( $(this).attr('data_price_up') );
            var ObjPrice = new Price();
            ObjPrice.setInfo(price_up,quality);
            //Set the RENT price
            $("#preview_rent_price").text(""+$.number( ObjPrice.calRentPrice(), 0, ',' )+"đ");
            $("#preview_sell_price").text(""+$.number( ObjPrice.calSellPrice(), 0, ',' )+"đ");
            
            //Get&Set tags to element
            $("#preview_tags").html("");
            var tag_name = $(this).attr('data_tags_name');
            tag_name = tag_name.split(',');
            for (var i = 0; i < tag_name.length; i++) {
                $("#preview_tags").append("<i class='tag'>"+tag_name[i]+"</i>");
            }
            
            //Set the edit form
            if($('#edit_form'))
            {
                $('#edit_form, #save_btn, #cancel_btn').hide();
                $("#edit_price").val( $.number( $(this).attr('data_price') , 0, ',' ) );
                $("#edit_price_up").val( $.number( $(this).attr('data_price_up') , 0, ',' ) );
                $("#edit_pro_code").val( $(this).attr('data_pro_code') );
                $("#edit_quality").val( quality );
                $("#edit_quantity").val( $(this).attr('data_quantity') );
                $("#edit_cate_code").val( $(this).attr('data_cate_code') );
                $("#edit_color").val( $(this).attr('data_color') );
                $("#edit_date").val( $(this).attr('data_date') );
                $("#pro_id").val( $(this).attr('data_pro_id') );
                $("#pro_code").val( $(this).attr('data_pro_code') );
                selectize.setValue( $(this).attr('data_tags').split(',') , false);
                
            }
        });
    });
    
    $('#save_btn').on('click',function(e){
        e.preventDefault();
        price = $("#edit_price").val().replace(/[^\d\.]/g, '');
        price_up = $("#edit_price_up").val().replace(/[^\d\.]/g, '');
        var data = {
            'id' : $("#pro_id").val(),
            'price' : price,
            'price_up' : price_up,
            'pro_code' : $("#edit_pro_code").val(),
            'cate_code' : $("#edit_cate_code").val(),
            'quality_stt' : $("#edit_quality").val(),
            'quantity' : $("#edit_quantity").val(),
            'color' : $("#edit_color").val(),
            'import_date' : $("#edit_date").val(),
            'tags_id' : $("#edit_tags").val().toString(),
            'modified' : $("#modified").val(),
            };
        //console.log(data);
        swal({
            title: "Are you sure?",
            text: "You will update product info !",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#008d4c",
            confirmButtonText: "Yes, do it!",
            closeOnConfirm: true
        }, function(isConfirm){
            if(isConfirm)
            {
                $.ajax({
                    type: "POST",
                    url: "/ajax-pro",
                    data: {update_data: data},
                    success: function(data){
                        location.reload();
                    },
                    complete: function(){
                        $('#divLoading').removeClass('show');
                    },
                    error: function(){
                        $('#divLoading').removeClass('show');
                    }
                });//end ajax
            }
        });
    });

    $("#edit_btn").on("click", function()
    {
        if( $("#show_form").is(":visible") )
        {
            $('#edit_btn').html("<i class='fa fa-stop'></i> Cancel");
            $("#save_btn").show();
            $('#show_form').hide(200);
            $('#edit_form').show(200);
        }
        else
        {
            $('#edit_btn').html("<i class='fa fa-pencil'></i> Edit");
            $("#save_btn").hide();
            $('#show_form').show(200);
            $('#edit_form').hide(200);
        }
    });

    $("#edit_pro_code, #edit_cate_code").on("keyup change", function()
    {
        if( $("#edit_cate_code").val() != "" )
        {
            $("#preview_pro_code").text(""+$("#edit_cate_code").val()+$("#edit_pro_code").val() );
        }
        else
        {
            swal("Select category first");
            $("#edit_cate_code").focusin();
        }
    });
        
    $("#edit_pro_code").on("blur", function()
    {
        $('#save_btn').prop('disabled',true);
        var pro_code = $(this).val();
        var cate_code = $("#edit_cate_code").val();
        if( pro_code != $('#pro_code').val() )
        {
            $.ajax({
                type: "POST",
                url: "/ajax-pro",
                data: {check_code: pro_code, cate_code: cate_code},
                success: function(data){
                    obj = jQuery.parseJSON(data);
                    if(!obj["data"])
                    {
                        swal("This product code was existed");
                        $("#edit_pro_code").val("");
                    }
                    else
                    {
                        $('#save_btn').prop('disabled',false);
                    }
                },
                complete: function(){
                    $('#divLoading').removeClass('show');
                },
                error: function(){
                    $('#divLoading').removeClass('show');
                }
            });//end ajax
        }
    });

    $('#edit_price, #edit_price_up').on('change keyup',function(e){
        $(this).val( $.number( $(this).val() , 0, ',' ) );
    });
    $('#edit_price, #edit_price_up').on('click',function(e){
        $(this).select();
    });
    
    //Call datepicker for input
    $( "#edit_date" ).datepicker({
		dateFormat: "yy-mm-dd",
		changeMonth: true,
		changeYear: true,
		yearRange: ((new Date).getFullYear()-1) + ':' + ((new Date).getFullYear()),
		maxDate: new Date
	});

    $("#sold_btn").on("click", function(e)
    {
        e.preventDefault();
        swal({
            title: "Have already sell?",
            text: "This product will mark as sold",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#008d4c",
            confirmButtonText: "Yes, alreay sold!",
            closeOnConfirm: true
        }, function(isConfirm){
            if(isConfirm)
            {
                $.ajax({
                    type: "POST",
                    url: "/ajax-pro",
                    data: {sold: $('#pro_id').val()},
                    success: function(data){
                        location.reload();
                    },
                    complete: function(){
                        $('#divLoading').removeClass('show');
                    },
                    error: function(){
                        $('#divLoading').removeClass('show');
                    }
                });//end ajax
            }
        });
    });
    
});//Ready
</script>