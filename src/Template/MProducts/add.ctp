<!-- Categories index -->
<?php $LoggedUser = $this->request->session()->read('Auth.User'); ?>
<?= $this->Flash->render('flash') ?>
<div class="row">
    <div class="col-md-12 ">
        <div class="box box-warning">
            
            <div class="box-header with-border">
                <h3 class="box-title">Add new</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="users form">
                <?= $this->Form->create(null, ['url' => 'add-product','id'=>'add_pro_form']) ?>
                    <div class="form-group">
                        <label>Category:</label>
                        <?= $this->Form->input('cate_code', [
                            'options' => $cates,
                            'empty' => "Select category",
                            'id' => 'cate_code',
                            'class' => 'form-control',
                            'error' => false,
                            'label' => false,
                            'placeholder' => 'Select category'
                        ]) ?>
                        <?= $this->Form->error('cate_code', null, array('class' => 'error-message','div'=>false)); ?>
                    </div>
                  
                    <div class="form-group">
                        <label>Code:</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-code"></i>
                            </div>
                            <?= $this->Form->input('pro_code', ['type'=>'text','class'=>'form-control','id'=>'pro_code','placeholder'=> 'Ex: 0001, 0010, 0100...','div'=>false,'label'=>'','error'=>false] ); ?>
                        </div>
                        <?= $this->Form->error('pro_code', null, array('class' => 'error-message','div'=>false)); ?>
                    </div>
                    
                    <hr>
                    
                    <div class="form-group">
                        <label>Product Code: </label><span class="fsize18" id="preview_pro_code"></span>
                    </div>

                    <div class="form-group">
                        <label>Supplier:</label>
                        <?= $this->Form->input('sup_id', [
                            'options' => $sups,
                            'empty' => "Select supplier",
                            'id' => 'sup_id',
                            'class'=>'form-control',
                            'error'=>false,
                            'label' => false,
                        ]) ?>
                        <?= $this->Form->error('sup_id', null, array('class' => 'error-message','div'=>false)); ?>
                    </div>
                    
                    <div class="form-group">
                        <label>Color:</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-paint-brush"></i>
                            </div>
                            <?= $this->Form->input('color', ['class'=>'form-control','id'=>'color','placeholder'=>'Product color','div'=>false,'label'=>'','error'=>false] ); ?>
                        </div>
                        <?= $this->Form->error('color', null, array('class' => 'error-message','div'=>false)); ?>
                    </div>
                    
                    <div class="form-group">
                        <label>Import date:</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <?= $this->Form->input('import_date', ['class'=>'form-control','id'=>'import_date','placeholder'=> 'Date of import','div'=>false,'label'=>'','error'=>false] ); ?>
                        </div>
                        <?= $this->Form->error('import_date', null, array('class' => 'error-message','div'=>false)); ?>
                    </div>
                    <?php 
                    $quality = [];
                    for($i = 60; $i<=100; $i = $i+5 )
                    {
                        $quality += [$i => $i];
                    } 
                    ?>
                
                    <div class="form-group">
                        <label>Quality (%):</label>
                        <?= $this->Form->input('quality_stt', [
                            'options' => $quality,
                            'empty' => "Select quality",
                            'id' => 'quality_stt',
                            'class'=>'form-control',
                            'error'=>false,
                            'label' => false,
                        ]) ?>
                        <?= $this->Form->error('quality_stt', null, array('class' => 'error-message','div'=>false)); ?>
                    </div>
                    
                    <div class="form-group">
                        <label>Quantity:</label>
                        <?= $this->Form->input('quantity', [
                            'value' => '1',
                            'id' => 'quantity',
                            'class'=>'form-control',
                            'error'=>false,
                            'label' => false,
                        ]) ?>
                        <?= $this->Form->error('quantity', null, array('class' => 'error-message','div'=>false)); ?>
                    </div>
                    
                    <div class="form-group">
                        <label>Price:</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-money"></i>
                            </div>
                            <?= $this->Form->input('price', ['class'=>'form-control','id'=>'price','placeholder'=> 'Price of product','div'=>false,'label'=>'','error'=>false] ); ?>
                        </div>
                        <?= $this->Form->error('price', null, array('class' => 'error-message','div'=>false)); ?>
                    </div
                    
                    <div class="form-group">
                        <label>Price Up:</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-money"></i>
                            </div>
                            <?= $this->Form->input('price_up', ['class'=>'form-control','id'=>'price_up','placeholder'=> 'Price of product','div'=>false,'label'=>'','error'=>false] ); ?>
                        </div>
                        <?= $this->Form->error('price_up', null, array('class' => 'error-message','div'=>false)); ?>
                    </div>
                    
                    <div class="form-group">
                        <?= $this->Form->input('tags_id', [
                            'options' => $tags,
                            'id' => 'tags',
                            'class'=>'',
                            'multiple' => true,
                            'error'=>false
                        ]) ?>
                        <?= $this->Form->error('tags_id', null, array('class' => 'error-message','div'=>false)); ?>
                    </div>

                    <?= $this->Form->hidden('created', ['value'=> date('Y-m-d H:i:s')] );?>
                    
                <?= $this->Form->button(__('<i class="fa fa-save"></i> Add'), ['class'=>'btn btn-app','id'=>'add_pro_btn','escape'=>false]); ?>
                <?= $this->Form->end() ?>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- .col -->
    </div>
</div>
<?php echo $this->Html->script('plugins/numberformat/jquery.number.js'); ?>
<script type="text/javascript">
    $().ready(function() {
	    $("#add_pro_form").validate({
			rules: {
				pro_code: {
                    required: true,
                    minlength: 4,
                    maxlength: 4,
                    checkProCode: true
				},
				quantity: {
				    required: true,
				    number: true,
				    maxlength: 2,
				},
				cate_code: {checkSelect:true},
				sup_id: {checkSelect:true},
				import_date: {
				    required: false,
				    date: true,
				},
			},
			messages: {
				pro_code: {
				    required: "This field is required",
				    minlength: "At least 4 digits",
                    maxlength: "Maximum 4 digits",
                    checkProCode: "Invalid product code"
				},
				quantity: {
				    required: "This field is required",
				    number: "Only number is allowed",
				    maxlength: "Only 02 digits are allowed",
				},
				cate_code: {checkSelect:"This field is required"},
				sup_id: {checkSelect: "This field is required"},
				import_date: {
				    date: "Invalid date format",
				},
			},
			highlight: function(element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-group').removeClass('has-error');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function(error, element) {
                if(element.parents('.input-group').length) {
                    error.insertAfter(element.parents(".input-group"));
                } else {
                    error.insertAfter(element);
                }
            }
	    });
	    
	    $.validator.addMethod("checkProCode", function(value, element) {
            return this.optional(element) || /[0-9]{4}\b/.test(value); 
        });
        
	    $.validator.addMethod("checkSelect", function(value, element) {
            if( value == '' || value == null){return false}else{return true}; 
        });
        
        $(function() {
            $('#tags').selectize({
                maxOptions : 5,
                maxItems: 5,
                persist: false,
                createOnBlur: true,
                create: true
            });
        });
        
        //Call datepicker for input
        $( "#import_date" ).datepicker({
    		dateFormat: "yy-mm-dd",
    		changeMonth: true,
    		changeYear: true,
    		yearRange: ((new Date).getFullYear()-1) + ':' + ((new Date).getFullYear()),
    		maxDate: new Date
    	});
	    
	});//end ready
	
	$('#add_pro_btn').on('click',function(e){
    e.preventDefault();
    var form = $('#add_pro_form');
        swal({
            title: "Are you sure?",
            text: "You will add new product !",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#008d4c",
            confirmButtonText: "Yes, do it!",
            closeOnConfirm: true
        }, function(isConfirm){
            if (isConfirm) form.submit();
        });
    });	
    
    $('#pro_code').on('change keyup',function(e){
        if( $("#cate_code").val() != "" )
        {
            $("#preview_pro_code").text(" "+$("#cate_code").val()+$(this).val() );
        }
        else
        {
            swal("Select category first");
            $(this).val("");
            $("#cate_code").focusin();
        }
    }); 
    
    $('#pro_code').on('blur',function(e){
        $.ajax({
            type: "POST",
            url: "/ajax-pro",
            data: {check_code: $(this).val(), cate_code: $("#cate_code").val()},
            success: function(data){
                obj = jQuery.parseJSON(data);
                if(!obj["data"])
                {
                    swal("This product code was existed");
                    $("#pro_code").val("");
                }
            },
            complete: function(){
                $('#divLoading').removeClass('show');
            },
            error: function(){
                $('#divLoading').removeClass('show');
            }
        });//end ajax
    }); 
    
    $('#cate_code').on('change',function(e){
        $("#pro_code").val("");
        $("#preview_pro_code").text("");
    });
    
    $('#price, #price_up').on('change keyup',function(e){
        $(this).val( $.number( $(this).val() , 0, ',' ) );
    });
</script>