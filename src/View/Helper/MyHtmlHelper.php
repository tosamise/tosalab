<?php
namespace App\View\Helper;
use Cake\View\Helper;
use Cake\View\View;
use Cake\View\Helper\HtmlHelper;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\ORM\Query;
use App\Controller\AppController;
/**
 *
 * Description: This class supports methods/functions
 * Class: MyHtmlHelper
 * @author: HuyDo
 * Path: src\View\Helper\MyHtmlHelper.php
 *
 */
class MyHtmlHelper extends Helper {

    /**
    * Description: Check the logged in user to view HTML
    * Function: check_role()
    * @author: HuyDo
    * @params: $role - The request role for returning true
    * @return: boolean
    * @version: 1.0
    */
    public function check_role($rq_role = "") 
    {
        $flag = false;
        if($rq_role != "")
        {   
            //Get user role id
            $user_role_id = $this->__get_logged_role_id($this->request->session()->read('Auth.User.id'))->role;
            //Get all roles
            $roles = $this->__getrole();
            $role_name = $roles[$user_role_id];
            //Check current user role match with request roles.
            if( in_array( $role_name , $rq_role ) )
            {
                $flag = true; 
            }
        }
        return $flag;
    }
    
    /**
	 * Description: Get the list of roles
	 * Function: getrole
	 * @author: HuyDo
	 * @params: none
	 * @return: object
	 * @version: 1.0
	 */
    private function __getrole()
    {
        //Get model MRoles
        $MRoles = TableRegistry::get('MRoles');
        $query = $MRoles->find('list', [
            'keyField' => 'id',
            'valueField' => 'role'
        ]);
        $data_init = ['0'=>'Select role'];
        $data = $data_init + $query->toArray();
        return $data;
    }//getrole
    
    /**
	 * Description: Get the logged in user role id
	 * Function: __get_logged_role_id
	 * @author: HuyDo
	 * @params: none
	 * @return: object
	 * @version: 1.0
	 */
    private function __get_logged_role_id($user_id)
    {
        //Get model MUsers
        $MUsers = TableRegistry::get('MUsers');
        $query = $MUsers->query();
        return $query->select("role")->where(['id'=>$user_id])->first();
    }
    
}
